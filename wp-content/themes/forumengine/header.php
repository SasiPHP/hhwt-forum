<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<!--[if lt IE 7]> <html class="ie ie6 oldie" lang="en"> <![endif]-->
	<!--[if IE 7]>    <html class="ie ie7 oldie" lang="en"> <![endif]-->
	<!--[if IE 8]>    <html class="ie ie8 oldie" lang="en"> <![endif]-->
	<!--[if gt IE 8]> <html class="ie ie9 newest" lang="en"> <![endif]-->
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta charset="utf-8">
	    <title>
            <?php wp_title( '|', true, 'right' ); ?>
	    </title>
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
		<!-- <meta name="thumbnail" content="<?php echo TEMPLATEURL . '/screenshot.png' ?>" /> -->
		<meta name="description" content="<?php echo et_get_option("blogdescription"); ?>" />
		<!-- Facebook Metas -->
		<meta property="og:image" content="<?php echo TEMPLATEURL . '/screenshot.png' ?>" />
		<!-- Facebook Metas / END -->

		<?php
			$favicon = et_get_option("et_mobile_icon") ? et_get_option("et_mobile_icon") : (TEMPLATEURL . '/img/fe-favicon.png');
		?>
		<link rel="shortcut icon" href="<?php echo TEMPLATEURL;?>/images/favicon.ico"/>
		<?php wp_head() ?>		
		<link href="<?php echo TEMPLATEURL ?>/css/hhwt.css" type='text/css' rel="stylesheet" >

		<!--[if lt IE 9]>
	      <script src="<?php echo TEMPLATEURL . '/js/libs/respond.min.js' ?>"></script>
	    <![endif]-->
	</head>
	<body <?php echo body_class() ?> onLoad="fireGoogleAnalytcis();">
	<?php 
    	global $hhwt_frontend,$custom_settings,$hhwt_session;     	

    	$bag_icon = "<i class='fa fa-shopping-cart' aria-hidden='true'></i>Bag";

    	//print_r($hhwt_session->get_user_session());
    	$user_email = isset( $_COOKIE['user_email'] ) ? $_COOKIE['user_email'] : 'not set';
    ?>


	<div class="site-container" <?php echo $user_email;?>>
		<div class="cnt-container">

			<div class="container-fluid hhwt-navbar-fluid">
				<div class="container hhwt-navbar-container">				
				
					<!-- Desktop Menu -->
					<nav class="navbar hhwt-navbar hidden-sm hidden-xs">
						<div class="navbar-header hhwt-navbar-header">
							<a class="navbar-brand" href="http://havehalalwilltravel.com/">
								<img src="<?php echo fe_get_logo() ?>">
							</a>
						</div>
						<?php
						$search='<li class="hhwt-nav-menu-li"><form role="search" method="get" id="hhwt-searchform" class="hhwt-searchform" action="'.home_url().'">
					       			<p class="search input-group">
					       				<span class="input-group-addon trg-submit"><i class="fa fa-search"></i></span>
					        			<input type="text" autocomplete="off" class="form-control" name="s" placeholder="Search..." value="'.get_search_query().'" >
					        			<span class="input-close">x</span>
					       			</p>
					      		</form></li>';

						wp_nav_menu( array(
							'menu'              => 'hhwt-nav-menu',
							'theme_location'    => 'header_menu',
							'depth'             => 0,
							'container'         => 'div',
							'container_class'   => 'navbar-collapse collapse in',
							'menu_class'        => 'nav navbar-nav hhwt-nav-menu',
							'items_wrap' 		=> '<ul id="%1$s" class="%2$s">'.$search.'%3$s</ul>',
							'walker'            => new HHWTNavWalker()
						));

						?>
						<?php global $current_user,$et_query; ?>

						<div class="hhwt-nav-right-container">
							<ul class="nav navbar-nav navbar-right hhwt-nav-menu-right">
								<li class="hhwt-trg-login hhwtmainmenu login <?php if($current_user->ID != ""){ echo 'collapse';} ?>">
									<a id="open_login" data-toggle="modal" href="#modal_login">
										<?php _e('Login', ET_DOMAIN) ?>
									</a>
								</li>
								<li class="hhwt-trg-login hhwtmainmenu profile-account <?php if(!$current_user->ID){ echo 'collapse';} ?>">
									<span class="name"><a href="javascript:void(0);">Hi <?php echo $current_user->display_name; ?> <span class="arrow"></span></a></span>								
									<span class="img"><a href="javascript:void(0);"><?php echo  et_get_avatar($current_user->ID) ?></a></span>

									<ul class=" menucontainer dropdown-menu" role="menu">
										<li class="menu-item menu-item-type-custom menu-item-object-custom hhwtmainsubmenu">
											<a href="<?php echo get_author_posts_url($current_user->ID); ?>"><?php _e('Profile',ET_DOMAIN); ?>
											</a>
										</li>
										<?php 
											/*wp_nav_menu( array(
												'menu'              => 'hhwt-nav-menu',
												'theme_location'    => 'marketplace_traveler',
												'depth'             => 0,
												'container'         => '',
												'container_class'   => '',
												'items_wrap' 		=> '%3$s'
											));*/
										?>
										<li class="menu-item menu-item-type-custom menu-item-object-custom hhwtmainsubmenu"><a href="<?php echo wp_logout_url( $_SERVER['REQUEST_URI'] ); ?>">Logout</a></li>
									</ul>
									<!-- <span class="number">8</span>   -->
									<div class="clearfix"></div>
								</li>
							</ul>
						</div>

						<?php /*<div class="dropdown-profile">
							<span class="arrow-up"></span>
							<div class="content-profile">
								<div class="head"><span class="text">@<?php echo $current_user->user_login; ?></span></div>
								<ul class="list-profile">
									<li>
										<a href="<?php echo get_author_posts_url($current_user->ID); ?>">
											<span class="icon" data-icon="U"></span>
											<br />
											<span class="text"><?php _e('Profile',ET_DOMAIN); ?></span>
										</a>
									</li>
									<!-- <li>
										<a href="#">
											<span class="icon" data-icon="M"></span>
											<br />
											<span class="text">Inbox (8)</span>
										</a>
									</li> -->
									<li>
										<a href="<?php echo wp_logout_url( $_SERVER['REQUEST_URI'] ); ?>">
											<span class="icon" data-icon="Q"></span>
											<br />
											<span class="text"><?php _e('Logout',ET_DOMAIN); ?></span>
										</a>
									</li>
								</ul>
							</div>
						</div>*/?>

					</nav>
				</div>
			</div>
			<!-- End of Navigation -->