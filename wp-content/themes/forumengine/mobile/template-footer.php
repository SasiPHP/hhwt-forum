<?php
/*if (is_active_sidebar( 'fe-mobile-sidebar' )){
	?>
	<div data-role="content" class="sidebar-mobile-wrap">
		<?php
			dynamic_sidebar( 'fe-mobile-sidebar' );
		?>
	</div>
	<?php
}*/
?>
	<footer id="footer">
		<div class="container-fluid hhwt-footer-fluid">
	         <div class="container hhwt-footer-container">

	            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 hhwt-footer-categories">
	               <?php
	                  if ( is_active_sidebar( 'footer_column_1' ) ) {
	                     dynamic_sidebar( 'footer_column_1' );
	                  }

	                  if ( is_active_sidebar( 'footer_column_2' ) ) {
	                     dynamic_sidebar( 'footer_column_2' );
	                  }

	                  if ( is_active_sidebar( 'footer_column_3' ) ) {
	                     dynamic_sidebar( 'footer_column_3' );
	                  }
	               ?>
	            </div>

	            <?php
	               if ( is_active_sidebar( 'footer_column_4' ) ) {
	                  dynamic_sidebar( 'footer_column_4' );
	               }
	            ?>
	          
	            <div class="col-lg-offset-1 col-lg-3 col-md-4 col-sm-4 hhwt-footer-download">
	               <div class="col-lg-12 col-md-12 col-sm-12 hhwt-footer-download-title">
	                  <h4>Download Our Mobile App</h4>
	               </div>
	               <div class="col-lg-6 col-md-6 col-sm-6 hhwt-footer-download-btn">
	                  <a href="https://itunes.apple.com/sg/app/hhwt-travel-planner-for-muslim-travellers-recommended/id1088691334?mt=8">   <img target="_blank" src="<?php echo TEMPLATEURL;?>/images/ios_footer_btn.png" title="IOS APP">
	                  </a>
	               </div>
	               <div class="col-lg-6 col-md-6 col-sm-6 hhwt-footer-download-btn">
	                  <a href="https://play.google.com/store/apps/details?id=com.hhwt.travelplanner&hl=en">
	                     <img target="_blank" src="<?php echo TEMPLATEURL;?>/images/android_footer_btn.png" alt="Android APP">
	                  </a>
	               </div>
	            </div>

	            <div class="hhwt-footer-logo col-xs-12">
	               <img src="<?php echo TEMPLATEURL;?>/images/footer_logo.png">
	            </div>
	            <div class="hhwt-footer-copyright col-xs-12">
	               <p>&copy; <?php echo date('Y');?> Hello Travel Pte Ltd.All Rights Reserved.</p>
	            </div>
	            <div class="hhwt-footer-agree col-xs-12">
	               <a href="http://www.havehalalwilltravel.com/end-user-license-agreement/" target="_blank">End User License Agreement</a>
	               <span>|</span>
	               <a href="http://www.havehalalwilltravel.com/privacy-policy/" target="_blank">Privacy Policy</a>
	            </div>
	         </div>
	      </div>
	      <!-- <div id="go-top-button" class="fa fa-angle-up" title="Scroll To Top"></div> -->

	</footer><!-- End Footer -->

</div>