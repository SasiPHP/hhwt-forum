$(window).scroll(function() {
	var $_scrolltop = $(this).scrollTop();
	var top_px = '45px';
	// Go to top button
	if ($_scrolltop > 100) {
		$('#go-top-button').css({
			bottom: top_px
		});
	} else {
		$('#go-top-button').css({
			bottom: '-100px'
		});
	}
});

$('#go-top-button').on("click", function() {
	$('html, body').animate({
		scrollTop: 0
	}, 700);
	return false;
});