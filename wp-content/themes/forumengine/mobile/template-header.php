<div <?php echo body_class( '' ) ?> data-role="page" id="<?php echo apply_filters( 'mobile_page_id', 'main' ) ?>">
	   <?php 
	    	global $hhwt_frontend,$custom_settings; 
	    	$gtm_id = $hhwt_frontend->hhwtfe_gtm_id();
	    	$bag_icon = "<i class='fa fa-shopping-cart' aria-hidden='true'></i>Bag";
	    ?>

	    <div data-role="panel" id="fe_category">
			<div class="fe-cat-list">
				<ul>
					<li class="<?php if (is_home() || is_front_page()) echo 'fe-current' ?>">
						<a href="<?php echo home_url( ) ?>">
							<span class="arrow"><span class="fe-sprite"></span></span>
							<span class="name"><?php _e('All', ET_DOMAIN) ?></span>
							<span class="flags"></span>
						</a>
					</li>
					<?php et_mobile_categories(); ?>
				</ul>
			</div>
		</div>

		<?php if(apply_filters( 'fe_expand_category' , true )){ ?>
		<style type="text/css">
			.fe-cat-list ul li.fe-has-child > ul{
				max-height: initial;
			}
		</style>
		<script type="text/javascript">
			$(document).ready(function() {
				$('.fe-cat-list ul li.fe-has-child').addClass('fe-opened');
			});
		</script>
		<?php } ?>
    
	   <div class="container-fluid hhwt-navbar-fluid">
			<div class="container hhwt-navbar-container">
				<!-- Mobile Menu -->
				<nav class="navbar hhwt-navbar visible-sm visible-xs">
					<div class="hhwt-mob-menu">
						<span class="hhwt-mob-search fa fa-search" data-toggle="collapse" data-target="#hhwt-mob-nav-search"></span>
						<a href="http://havehalalwilltravel.com/">
							<img class="hhwt-mob-logo" src="<?php echo TEMPLATEURL;?>/images/header_logo.png">
						</a>

						<button type="button" class="navbar-toggle collapse-menu" data-toggle="collapse" data-target="#hhwt-mobile-mmenu">
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					    </button>

					</div>

					<div class="collapse collapse-search" id="hhwt-mob-nav-search">
							<form role="search" method="get" id="hhwt-searchform-mobile" class="hhwt-searchform" action="<?php echo home_url();?>">
							<p class="input-group">
								<span class="input-group-addon trg-submit"><i class="fa fa-search"></i></span>
								<input class="form-control" autocomplete="off" name="s" placeholder="Search..." type="text" value="<?php echo get_search_query();?>">
							</p>
						</form>
					</div>
					

					<?php
					wp_nav_menu( array(
						'menu'              => 'hhwt-nav-menu',
						'theme_location'    => 'header_menu',
						'depth'             => 0,
						'container'         => 'div',
						'container_class'   => 'navbar-collapse collapse in',
						'menu_class'        => 'nav navbar-nav hhwt-nav-menu',
						'items_wrap' 		=> '<ul id="%1$s" class="%2$s">%3$s</ul>',
						'walker'            => new HHWTNavWalker())
					);
					?>
					<?php global $current_user,$et_query; ?>
					<div id="hhwt-mobile-mmenu" class="collapse navbar-collapse">
						<div class="hhwt-nav-right-container hhwt-user-menu">
							<ul class="nav navbar-nav navbar-right hhwt-nav-menu-right">
								<?php if($current_user->ID == ""){ ?>
								<li class="hhwt-trg-login hhwtmainmenu"><a href="<?php echo home_url('login');?>">
									<?php _e('Login', ET_DOMAIN) ?>
								</a></li>
								<?php } else { ?>
								<li class="hhwt-trg-login hhwtmainmenu profile-account">
									<span class="name"><a href="javascript:void(0);" class="dropdown-toggle">Hi <?php echo $current_user->display_name; ?> <span class="arrow"></span></a></span>								
									<span class="img"><a href="javascript:void(0);" class="dropdown-toggle"><?php echo  et_get_avatar($current_user->ID) ?></a></span>

									<ul class=" menucontainer dropdown-menu loggedinMenu" role="menu">
										<li class="menu-item menu-item-type-custom menu-item-object-custom hhwtmainsubmenu">
											<a href="<?php echo get_author_posts_url($current_user->ID); ?>"><?php _e('Profile',ET_DOMAIN); ?>
											</a>
										</li>
									<?php 
										/*wp_nav_menu( array(
											'menu'              => 'hhwt-nav-menu',
											'theme_location'    => 'marketplace_traveler',
											'depth'             => 0,
											'container'         => '',
											'container_class'   => '',
											'items_wrap' 		=> '%3$s'
										));*/
									?>
									<li class="menu-item menu-item-type-custom menu-item-object-custom hhwtmainsubmenu hhwt-session-end"><a href="<?php echo wp_logout_url( $_SERVER['REQUEST_URI'] ); ?>">Logout</a></li>
									</ul>
									<!-- <span class="number">8</span>   -->
									<div class="clearfix"></div>
								</li>
								<?php } ?>
							</ul>
						</div>
					</div>

				</nav>

			</div>
		</div>
		<!-- End of Navigation -->