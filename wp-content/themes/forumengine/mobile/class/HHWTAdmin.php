<?php

/**
Halal wordpress admin functionalities
*/

Class HHWTAdmin{	

	public function __construct() {

		add_action('wp_ajax_wpmsp_admin_ajax_method', array($this, 'hhwt_admin_ajax_handler'));
		add_action('wp_ajax_nopriv_wpmsp_admin_ajax_method', array($this, 'hhwt_admin_ajax_handler'));

		//admin custom settings page
		add_action('admin_menu', array($this, 'hhwt_create_custom_settings_page') );

		//add featured post for slider
		add_action('admin_init', array($this, 'hhwt_register_meta_box'), 1);

		//save metabox data
		add_action('save_post', array($this, 'hhwt_save_featured_metabox'));

		//enqueue common scripts
		add_action('admin_enqueue_scripts', array($this, 'hhwt_enqueue_custom_scripts') );

		//create custom widget
		add_action( 'widgets_init', array($this, 'hhwt_custom_widgets') );

		add_action('after_setup_theme', array($this, 'hhwt_hide_admin_bar'));

		//export reports
		if( isset($_POST["export-latest"]) && !empty($_POST["export-latest"]) ) {
			$this->hhwt_export_latest_report();
		}

		//dropdown categories multiselect
		add_filter( 'wp_dropdown_cats', array($this, 'hhwt_dropdown_multiple_cats'), 10, 2 );
	}

	public function hhwt_admin_ajax_handler() {
		$gotomethod = trim($_POST['gotomethod']);
		if (!empty($gotomethod) && method_exists($this, $gotomethod)) {
			$rtnval = call_user_func(array($this,$gotomethod),$_POST); 
			die($rtnval);
		} else {
			die('no-method found');
		}
	}

	public function hhwt_create_custom_settings_page()
	{
		add_menu_page('Manage Reports','Manage Reports','manage_options','manage-hhwt-reports', array($this, 'hhwt_report_page_content'), 'dashicons-media-spreadsheet');
		add_menu_page('HHWT Settings','HHWT Settings','manage_options','hhwt-settings', array($this, 'hhwt_custom_settings_content'));
	}

	public function hhwt_custom_settings_content()
	{
		$custom_settings = get_option(HHWT_CUSTOM_STS);

		if( isset($_POST['submit']) ) {

			if (isset($_POST['hhwtcustomsts']['gads-top'])) {
				$_POST['hhwtcustomsts']['gads-top'] = addslashes($_POST['hhwtcustomsts']['gads-top']);
			}

			if (isset($_POST['hhwtcustomsts']['gads-bottom'])) {
				$_POST['hhwtcustomsts']['gads-bottom'] = addslashes($_POST['hhwtcustomsts']['gads-bottom']);
			}

			update_option( HHWT_CUSTOM_STS, $_POST['hhwtcustomsts'] );
			echo '<div class="notice notice-success is-dismissible" style="margin:20px;"><p>Settings updated</p></div>';
		}
		
		if ($custom_settings) {
			$mailchimp_key    = $custom_settings['mailchimp-key'];
			$mailchimp_list   = $custom_settings['mailchimp-list'];
			$slider_count     = $custom_settings['slide-count'];
			$floater_btn_text = $custom_settings['floater-btn'];
			$url              = $custom_settings['url'];
			$floater_desc     = $custom_settings['floater-desc'];
			$floater_logo     = $custom_settings['floater-logo'];

			$gads_top = stripslashes($custom_settings['gads-top']);
			$gads_bottom = stripslashes($custom_settings['gads-bottom']);

			$multi_grid_sel   = $custom_settings['home-multi-grid'];
			$dest_grid_sel    = $custom_settings['home-dest-grid'];

			$list_grid_sel    = $custom_settings['home-list-grid'];

			$multi_grid_args = array(
								'name'=> 'hhwtcustomsts[home-multi-grid]',
								'class'=> 'regular-text',
								'selected' => $multi_grid_sel
							);

			$dest_grid_args = array(
								'name'=> 'hhwtcustomsts[home-dest-grid]',
								'class'=> 'regular-text',
								'selected' =>  $dest_grid_sel
							);

			$list_grid_args = array(
								'name'=>'',
								'class'=>'regular-text',
								'multiple'=>'true',
								'id'=>'list-post-source'
							);
			$api_url                 = esc_url($custom_settings['api-url']);
			$latest_article_count    = $custom_settings['latest-article-count'];
			$fb_id                   = $custom_settings['fb-id'];

			/* Mongo Settings */
			$mongo_endpoint= esc_url($custom_settings['mongo-endpoint']);
			$mongo_public  = $custom_settings['mongo-public'];
			$mongo_private = $custom_settings['mongo-private'];
			$event_secret = $custom_settings['event-secret'];

			$newsletter_countries= $custom_settings['newsletter-countries'];

			if ($list_grid_sel) {
				$list_grid_args['exclude'] = implode(",", $list_grid_sel);
			}

			$gtm_id = $custom_settings['gtm-id'];
		}
		?>
		<head>
			<style>
				.title-filter-container .form-table th{
					font-weight: normal;
				}
				.select-list-container{
					width: 100%;
					overflow: hidden;
					clear: both;
				}
				.select-source{
					float: left;
					width: 350px;
				}
				.select-buttons{
					float: left;
					width: 50px;
					margin-left: 50px;
					margin-right: 50px;
				}
				.select-buttons .button{
					width: 50px;
    				margin-bottom: 10px;
				}
				.select-target{
					float: left;
					width: 350px;
				}
			</style>
		</head>
		<div class="title-filter-container">
			<form method="post">

				<div class="pagetitle">
					<h2>Homepage Settings</h2>
				</div>
			
				<table class="form-table">
					<tbody>
						<tr>
							<th>Multi Grid</th>
							<td><?php wp_dropdown_categories($multi_grid_args); ?></td>
						</tr>
						<tr>
							<th>Destination Grid</th>
							<td><?php wp_dropdown_categories($dest_grid_args); ?></td>
						</tr>
						<tr>
							<th>List Grid(Related)</th>
							<td>
								<div class="select-list-container">
									<div class="select-source">
										<?php wp_dropdown_categories($list_grid_args); ?>
									</div>
									<div class="select-buttons">
										<input type='button' id='btn-all-right' value='>>' class="button button-primary" /><br />
										<input type='button' id='btn-right' value='>' class="button button-primary" /><br />
										<input type='button' id='btn-left' value='<' class="button button-primary" /><br />
										<input type='button' id='btn-all-left' value='<<' class="button button-primary" />
									</div>
									<div class="select-target">
										<select size="8" multiple class="regular-text" name="hhwtcustomsts[home-list-grid][]" id="list-post-target">
											<?php 
											if ($list_grid_sel) {
												foreach ($list_grid_sel as $key => $cat_id) {
													printf("<option value=%d %s>%s</option>", $cat_id, 'selected', get_cat_name($cat_id));
												}
											} 
											?>
										</select>
									</div>
								</div>
							</td>
						</tr>
					</tbody>
				</table><br><br>

				<div class="pagetitle">
					<h2>Mailchimp Settings</h2>
				</div>
			
				<table class="form-table">
					<tbody>
						<tr>
							<th>Mailchimp Key</th>
							<td><input type="text" class="regular-text" name="hhwtcustomsts[mailchimp-key]" value="<?php echo($custom_settings['mailchimp-key']) ? $custom_settings['mailchimp-key'] : ''; ?>"></td>
						</tr>
						<tr>
							<th>List ID</th>
							<td><input type="text" class="regular-text" name="hhwtcustomsts[mailchimp-list]" value="<?php echo($custom_settings['mailchimp-list']) ? $custom_settings['mailchimp-list'] : ''; ?>"></td>
						</tr>
					</tbody>
				</table><br><br>
				<div class="pagetitle">
					<h2>Slider Settings</h2>
				</div>
				<table class="form-table">
					<tbody>
						<tr>
							<th>Homepage Slide Count</th>
							<td><input type="text" class="regular-text" name="hhwtcustomsts[home-slide-count]" value="<?php echo($custom_settings['home-slide-count']) ? $custom_settings['home-slide-count'] : ''; ?>"></td>
						</tr>
						<tr>
							<th>Otherpage Slide Count</th>
							<td><input type="text" class="regular-text" name="hhwtcustomsts[slide-count]" value="<?php echo($custom_settings['slide-count']) ? $custom_settings['slide-count'] : ''; ?>"></td>
						</tr>
					</tbody>
				</table><br><br>
				<div class="pagetitle">
					<h2>Floater Widget Settings</h2>
				</div>

				<table class="form-table">
					<tbody>
						<tr>
							<th>Widget Description</th>
							<td><textarea class="regular-text" name="hhwtcustomsts[floater-desc]"><?php echo $floater_desc; ?></textarea></td>
						</tr>
						<tr>
							<th>Widget Button Text</th>
							<td><input type="text" class="regular-text" name="hhwtcustomsts[floater-btn]" value="<?php echo $floater_btn_text; ?>"></td>
						</tr>
						<tr>
							<th>Widget Plan Url</th>
							<td><input type="text" class="regular-text" name="hhwtcustomsts[url]" value="<?php echo $url; ?>"></td>
						</tr>
						<tr>
							<th>Widget Image</th>
							<td>
								<input type="text" class="regular-text" name="hhwtcustomsts[floater-logo]" value="<?php echo $floater_logo; ?>" class="widefat"/>
								<input type="button" class="upload-button" value="Choose">
							</td>
						</tr>
					</tbody>
				</table>

				<div class="pagetitle">
					<h2>Google Ads</h2>
				</div>

				<table class="form-table">
					<tbody>
						<tr>
							<th>Top Ad</th>
							<td><textarea class="regular-text" name="hhwtcustomsts[gads-top]"><?php echo $gads_top; ?></textarea></td>
						</tr>
						<tr>
							<th>Bottom Ad</th>
							<td><textarea class="regular-text" name="hhwtcustomsts[gads-bottom]"><?php echo $gads_bottom; ?></textarea></td>
						</tr>
					</tbody>
				</table>

				<div class="pagetitle">
					<h2>GTM ID</h2>
				</div>

				<table class="form-table">
					<tbody>
						<tr>
							<th>GTM Body Script ID</th>
							<td><input type="text" name="hhwtcustomsts[gtm-id]" value="<?php echo $gtm_id;?>"></td>
						</tr>
					</tbody>
				</table>

				<div class="pagetitle">
					<h2>Other Settings</h2>
				</div>

				<table class="form-table">
					<tbody>
						<tr>
							<th>API Url</th>
							<td><input type="text" class="regular-text" name="hhwtcustomsts[api-url]" value="<?php echo $api_url; ?>"></td>
						</tr>
						<tr>
							<th>Latest Article Count</th>
							<td><input type="text" class="regular-text" name="hhwtcustomsts[latest-article-count]" value="<?php echo $latest_article_count; ?>"></td>
						</tr>
						<tr>
							<th>Facebook ID</th>
							<td><input type="text" class="regular-text" name="hhwtcustomsts[fb-id]" value="<?php echo $fb_id; ?>"></td>
						</tr>
					</tbody>
				</table>

				<div class="pagetitle">
					<h2>Mongo Settings</h2>
				</div>

				<table class="form-table">
					<tbody>
						<tr>
							<th>Public Key</th>
							<td><input type="text" class="regular-text" name="hhwtcustomsts[mongo-public]" value="<?php echo $mongo_public; ?>"></td>
						</tr>
						<tr>
							<th>Private Key</th>
							<td><input type="text" class="regular-text" name="hhwtcustomsts[mongo-private]" value="<?php echo $mongo_private; ?>"></td>
						</tr>
						<tr>
							<th>Achievement Secret Key</th>
							<td><input type="text" class="regular-text" name="hhwtcustomsts[event-secret]" value="<?php echo $event_secret; ?>"></td>
						</tr>
						<tr>
							<th>API Endpoint</th>
							<td><input type="text" class="regular-text" name="hhwtcustomsts[mongo-endpoint]" value="<?php echo $mongo_endpoint; ?>"></td>
						</tr>
					</tbody>
				</table>

				<div class="pagetitle">
					<h2>Newsletter Widget Countries</h2>
				</div>

				<table class="form-table">
					<tbody>
						<tr>
							<th>Newsletter Display Countries</th>
							<td><td><textarea class="regular-text" name="hhwtcustomsts[newsletter-countries]"><?php echo $newsletter_countries; ?></textarea></td></td>
						</tr>						
					</tbody>
				</table>

				<?php submit_button(); ?>
			</form>
		</div>
		<script type="text/javascript">
			jQuery(document).ready(function() {

				var uploadID = '';

				jQuery('.upload-button').click(function() {
					uploadID = jQuery(this).prev('input');
					tb_show('', 'media-upload.php?type=image&TB_iframe=true');
					set_send(uploadID);
					return false;
				});

				function set_send(uploadID) {
					window.send_to_editor = function(html) {
						if(typeof jQuery(html).attr('src') != 'undefined' )
							imgurl = jQuery(html).attr('src');
						else
							imgurl = jQuery(html).attr('href');

						uploadID.val(imgurl);
						tb_remove();
					};
				}

			});

			jQuery('#btn-right').click(function (e) {
	            jQuery('select').moveToListAndDelete('#list-post-source', '#list-post-target');
	            e.preventDefault();
	        });

	        jQuery('#btn-all-right').click(function (e) {
	            jQuery('select').moveAllToListAndDelete('#list-post-source', '#list-post-target');
	            e.preventDefault();
	        });

	        jQuery('#btn-left').click(function (e) {
	            jQuery('select').moveToListAndDelete('#list-post-target', '#list-post-source');
	            e.preventDefault();
	        });

	        jQuery('#btn-all-left').click(function (e) {
	            jQuery('select').moveAllToListAndDelete('#list-post-target', '#list-post-source');
	            e.preventDefault();
	        });

		</script>
		<?php
	}

	public function hhwt_register_meta_box(){
		add_meta_box('featured-post', __('Featured Post'), array($this, 'hhwt_featured_slider_metabox'), 'post', 'side', 'low');
	}

	public function hhwt_featured_slider_metabox()
	{
		global $post;
		wp_nonce_field( '_hhwt_nonce_val_featured_meta_', 'hhwt-nonce-featured-box' );
		$featured = get_post_meta($post->ID, META_FEATURED, true);
		?>  
		<input type='checkbox' name='hhwt-input-featured-post' value='1' <?php echo checked(1, $featured, false); ?> />
		<label for='hhwt-input-featured-post'>Set as Feature post</label>
		<?php
	}

	public function hhwt_save_featured_metabox($post_id)
	{

		if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;


		if ( ! isset( $_POST['hhwt-nonce-featured-box'] ) ) {
			return $post_id;
		}

		if ( ! wp_verify_nonce( $_POST['hhwt-nonce-featured-box'], '_hhwt_nonce_val_featured_meta_' ) ) {
			return $post_id;
		}

		// Check that the logged in user has permission to edit this post
		if ( ! current_user_can( 'edit_post' ) ) {
			return $post_id;
		}


		update_post_meta( esc_attr($post_id), META_FEATURED, esc_attr($_POST['hhwt-input-featured-post'])); 
	}

	public function hhwt_enqueue_custom_scripts()
	{
		wp_enqueue_script( 'hhwt-admin', TEMPLATEURL.'/assets/js/hhwt-admin.js', array('jquery'), null, true );
		$current_screen = get_current_screen();

		if ($current_screen->base == 'toplevel_page_manage-hhwt-reports') {
			wp_enqueue_script( 'datatables-js', TEMPLATEURL. '/admin/js/jquery.dataTables.js', array('jquery'), null, true);
			wp_enqueue_style( 'datatables-css', TEMPLATEURL. '/admin/css/jquery.dataTables.css' );
		}

		if ($current_screen->base == 'toplevel_page_hhwt-settings') {
			wp_enqueue_script( 'selectlist-js', TEMPLATEURL. '/admin/js/jquery.selectlistactions.js', array('jquery'), null, true);
		}
	}

	public function hhwt_custom_widgets()
	{

		register_sidebar( array(
			'name'          => esc_html__( 'Footer Column 1', 'hhwt' ),
			'id'            => 'footer-1',
			'description'   => '',
			'before_widget' => '<div id="%1$s" class="col-sm-4 col-xs-12 hhwt-footer-grid clearfix %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="titles">',
			'after_title'   => '</h4>',
			)
		);

		register_sidebar( array(
			'name'          => esc_html__( 'Footer Column 2', 'hhwt' ),
			'id'            => 'footer-2',
			'description'   => '',
			'before_widget' => '<div id="%1$s" class="col-sm-4 col-xs-12 hhwt-footer-grid clearfix %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="titles">',
			'after_title'   => '</h4>',
			) 
		);

		register_sidebar( array(
			'name'          => esc_html__( 'Footer Column 3', 'hhwt' ),
			'id'            => 'footer-3',
			'description'   => '',
			'before_widget' => '<div id="%1$s" class="col-sm-4 col-xs-12 hhwt-footer-grid clearfix %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="titles">',
			'after_title'   => '</h4>',
			) 
		);

		register_sidebar( array(
			'name'          => esc_html__( 'Footer Column 4', 'hhwt' ),
			'id'            => 'footer-4',
			'description'   => '',
			'before_widget' => '<div id="%1$s" class="col-lg-2 col-md-2 col-sm-2 col-xs-12 hhwt-footer-follow %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="titles">',
			'after_title'   => '</h4>',
			) 
		);

		register_sidebar( array(
			'name'          => esc_html__( 'Footer Column 5', 'hhwt' ),
			'id'            => 'footer-5',
			'description'   => '',
			'before_widget' => '<div id="%1$s" class="clearfix %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="titles">',
			'after_title'   => '</h4>',
			) 
		);

		register_sidebar( array(
			'name'          => esc_html__( 'Global Sidebar', 'hhwt' ),
			'id'            => 'global-sidebar',
			'description'   => '',
			'before_widget' => '<div id="%1$s" class="clearfix %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="titles">',
			'after_title'   => '</h4>',
			) 
		);
	}

	public function hhwt_report_table()
	{
		global $wpdb;
		$tblprefix  =   $wpdb->prefix;
		$tblname    =   $tblprefix.'hhwt_travel_report';

		return $tblname;
	}

	public function hhwt_hide_admin_bar() {

		global $wpdb;
		//create custom table
		$charset_collate = $wpdb->get_charset_collate();

		if (!current_user_can('administrator') && !is_admin()) {
			show_admin_bar(false);
		}

		$tblname = $this->hhwt_report_table();

		$report_sql = "CREATE TABLE IF NOT EXISTS $tblname (
			ID int(11) NOT NULL AUTO_INCREMENT,
			user_name VARCHAR(255) NOT NULL,
			job_title VARCHAR(255) NOT NULL,
			user_email VARCHAR(255) NOT NULL,
			user_company VARCHAR(255) NOT NULL,
			created_date datetime NOT NULL,
			modified_date datetime NOT NULL,
			UNIQUE KEY id (ID)) $charset_collate;";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		dbDelta($report_sql);

	}

	public function hhwt_report_page_content()
	{
		global $wpdb;
		//get active records
		$tblname = $this->hhwt_report_table();

		$active_records = $wpdb->get_results("SELECT * FROM {$tblname} ORDER BY ID DESC", OBJECT_K);

		?>
		<style type="text/css">
			.pagetitle h2{
				margin-top: 20px;
			}
			.btn-export{
				float: right;
				margin-bottom: 20px !important;
			}
			.dataTables_filter{
				display: none;
			}
		</style>

		<div class="title-filter-container">
			<div class="pagetitle">
				<h2>Reports</h2>
				<form method="post" class='user-export-form'>
					<?php  wp_nonce_field('hhwtnonce_export_reports'); ?>
					<input type="submit" name="export-latest" class="button button-primary btn-export" value="Export Reports">
				</form>
			</div>
		</div>

		<table width="100%" class="wp-list-table widefat fixed striped tbl-reports" id="hhwt_table_report">
			<thead>
				<tr>
					<th>S.No</th>
					<th class="search">Name</th>
					<th class="search">Job Title</th>
					<th class="search">Email</th>
					<th class="search">Company</th>
					<th class="search">Date</th>
				</tr>
			</thead>
			<tbody>
			<?php
				if ($active_records) {

					$si =1;
					foreach($active_records as $single_record) {

						$report_id  =   $single_record->ID;
						$user_name  =   $single_record->user_name;
						$job_title  =   $single_record->job_title;
						$user_email =   $single_record->user_email;
						$user_company =   $single_record->user_company;
						$created_date =   $single_record->created_date;

			?>
						<tr>
							<td><?php echo $si; ?></td>
							<td><?php echo $user_name?></td>
							<td><?php echo $job_title ?></td>
							<td><?php echo $user_email ?></td>
							<td><?php echo $user_company ?></td>
							<td><?php echo date("M d Y", strtotime($created_date)); ?></td>
						</tr>

			<?php
						$si++;
					}

				}
			?>
			</tbody>
		</table>
		<script type="text/javascript">
			/* datatables */
			jQuery(document).ready(function() {

				jQuery('#hhwt_table_report').dataTable( {
					"aLengthMenu": [ 50, 100, 200, 300, 400, 500 ], 
					"iDisplayLength": 50,
				});

				var post_table = jQuery('#hhwt_table_report').DataTable();

				jQuery('#hhwt_table_report .search').each( function () {
					var title = jQuery(this).text();
					jQuery(this).html( '<input type="text" placeholder="'+title+'" />' );
				});
				post_table.columns().every( function () {
					var that = this;

					jQuery( 'input', this.header() ).on( 'keyup change', function () {
						if ( that.search() !== this.value ) {
							that
								.search( this.value )
								.draw();
						}
					} );
				} );

			} );
		</script>
		<?php
	}

	public function hhwt_export_latest_report()
	{
		global $wpdb;

		$tbl_report  =  $this->hhwt_report_table();
		$report_list =  $wpdb->get_results("SELECT * from {$tbl_report}", ARRAY_A);

		if (wp_verify_nonce($_POST['_wpnonce'], 'hhwtnonce_export_reports') && count($report_list) > 0 ) {

			$column_header = array("S.No", "Name", "Job Title", "Email", "Company", "Created Date");

			header("Pragma: public");
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private", false);
			header("Content-Type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"HHWT_Reports_List.csv\";" );
			header("Content-Transfer-Encoding: binary");

			$i = 1;
			$output = fopen('php://output', 'w');
			fputcsv($output, $column_header);
			foreach($report_list as $report) {
				unset($report['ID']);
				unset($report['modified_date']);

				if($report['created_date'])
					$report['created_date'] = date("d-m-Y", strtotime($report['created_date']));

				array_unshift($report,$i);
				fputcsv($output, $report);
				$i++;
			}
			fclose($output);
			exit;
		}
	}

	public function hhwt_register_custompost_to_fb($post)
	{
		if (!in_array("my", $post)) {
			$post[] = "my";
		}
		return $post;
	}

	public function hhwt_dropdown_multiple_cats( $output, $r ) {

	    if( isset( $r['multiple'] ) && $r['multiple'] ) {

	        $output = preg_replace( '/^<select/i', '<select multiple size=8', $output );

	        $output = str_replace( "name='{$r['name']}'", "name=''", $output );
	    }

	    return $output;
	}


} //End of Class