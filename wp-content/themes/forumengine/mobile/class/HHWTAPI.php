<?php

/**
Halal wordpress admin functionalities
*/

Class HHWTAPI{

    private $user_travel_plan;
    private $user_achievements;

    /* user actions */
    private $apiurl_login;
    private $apiurl_register;
    private $apiurl_fb_register;
    private $apiurl_shared_login;
    private $apiurl_forgetpassword;

    /*bucket urls*/
    private $apiurl_getbuckets;
    private $apiurl_managebucket;
    private $apiurl_addtrip;
    private $apiurl_tripcheck;
    private $apiurl_bucketlistdelete;

    /*gamify url*/
    private $apiurl_gamify;

    /* cities */
    private $apiurl_getcities;

    public function __construct() {

        // ajax handler
        add_action('wp_ajax_hhwtapi_ajax_method', array($this, 'hhwtapi_ajax_handler'));
        add_action('wp_ajax_nopriv_hhwtapi_ajax_method', array($this, 'hhwtapi_ajax_handler'));

        //trigger admin login
        add_action('authenticate', array($this, 'hhwtapi_trigger_admin_login'), 30, 3 );

        $this->user_travel_plan = array(
                1=>"Within a week",
                2=>'Within 2 weeks',
                3=>'Within 1 month',
                4=>'Within 3 months',
                5=>'Within 6 months',
                6=>"I'm not sure",
                7=>"I'm not travelling",
            );

        $this->user_achievements = array(
            'save'=>'Save',
            'unsave'=>'Un Save',
            'addreview'=>'Add Review',
            'mediareview'=>'Add Review with Image',
            'likereview'=>'Like Review',
            'unlikereview'=>'Unlike Review',
            'deletereview'=>'Delete Review',
            'deletemediareview'=>'Delete Review with Image',
            'newuser'=>'User Registration'
        );

        /* API variables */
        $this->apiurl_login = BUCKET_API_HOST . 'login';
        $this->apiurl_register = BUCKET_API_HOST . 'register';
        $this->apiurl_fb_register = BUCKET_API_HOST . 'facebooksignupsignin';
        $this->apiurl_shared_login = BUCKET_API_HOST . 'getuser';
        $this->apiurl_forgetpassword = BUCKET_API_HOST . 'forgotpassword';

        /*bucket urls*/
        $this->apiurl_getbuckets = BUCKET_API_HOST . 'getbuckets';
        $this->apiurl_managebucket = BUCKET_API_HOST . 'managebucket';
        $this->apiurl_addtrip = BUCKET_API_HOST . 'addtripplan';
        $this->apiurl_tripcheck = BUCKET_API_HOST . 'tripplancheck';
        $this->apiurl_bucketlistdelete = BUCKET_API_HOST . 'bucketlistdelete';

        /*gamify url*/
        $this->apiurl_gamify = BUCKET_API_HOST . 'gamifystoreevent';

        /*get cities */
        $this->apiurl_getcities = BUCKET_API_HOST . 'get_cities';
    }

    public function hhwtapi_ajax_handler() {
        $gotomethod = trim($_POST['gotomethod']); 
        if (!empty($gotomethod) && method_exists($this, $gotomethod)) {
            $rtnval = call_user_func(array($this,$gotomethod),$_POST); 
            die($rtnval);
        } else {
            die('no-method found');
        }
    }

    public function hhwtapi_get_response($url, $post_params=array(), $method=1)
    {
        if (is_array($post_params)) {
            $post_params = http_build_query($post_params);
        }

        $headers =  array("Content-Type: application/x-www-form-urlencoded");
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, $method);
        
        if($post_params)
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 

        $response = curl_exec($ch);

        if (curl_error($ch)) {
            return false;
        } else {
            $response_data = json_decode($response, true);
        }

        curl_close($ch);

        return $response_data;
    }

    public function hhwtapi_decrypt_data($data)
    {
        return base64_decode($data);
    }

    public function hhwtapi_format_register_data($post_data)
    {
        $email = (isset($post_data['user_email'])) ? $post_data['user_email'] : '';
        $fname = (isset($post_data['first_name'])) ? $post_data['first_name'] : '';
        $lname  = (isset($post_data['last_name'])) ? $post_data['last_name'] : '';
        $uname  = (isset($post_data['user_login'])) ? $post_data['user_login'] : '';
        $name = $fname.' '.$lname;

        $user_data = array(
            'fname'=> $fname,
            'lname'=> $lname,
            'email'=> $email,
            'type'=>'web-blog',
            'subscribe'=> 'yes'
        );

        if ($post_data['fbid']) {
            $user_data['fb_id'] = (isset($post_data['fbid'])) ? $post_data['fbid'] : 0;
            $user_data['imgurl'] = (isset($post_data['fbimg'])) ? $post_data['fbimg'] : ''; 
        } else {
            $user_data['password'] = (isset($post_data['user_password'])) ? $post_data['user_password'] : ''; 
            $user_data['country'] = (isset($post_data['user_country'])) ? $post_data['user_country'] : '';
            $user_data['username'] = $uname;
            $user_data['name'] = $name;
        }

        return $user_data; 
    }

    /*
    Format user menu data
    */
    public function hhwtapi_format_menu_data()
    {
        global $hhwt_session;

        $user_session = $hhwt_session->get_user_session();

        if ($user_session) {
            $user_type = $user_session['user_type'];
            $user_name = $user_session['user_name'];
            $user_image = $user_session['user_image'];
        }

        $bucket_list_url = BUCKET_DASHBOARD;
        $message_img = TEMPLATEURL . '/images/menu-icons/message.png';

        $message_img = "<img src={$message_img}><span class='visible-xs visible-sm'>Messages</span>";

        $notification_icon = "<span class='fa fa-bell'></span><span class='fa fa-circle'></span><span class='visible-xs visible-sm'>Notifications</span>";
        if (empty($user_image))
            $user_image = "https://secure.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=32&d=mm&r=g&forcedefault=1";
        $user_avatar = "<img src='{$user_image}'>";
        $bag_icon = "<i class='fa fa-shopping-cart' aria-hidden='true'></i>Bag";

        if ($user_type == 0)
            $menu_name = 'marketplace_traveler';
        else 
            $menu_name = 'marketplace_host';

        $menu_data = wp_nav_menu( array(
                        'menu'              => 'hhwt-login-menu',
                        'theme_location'    => $menu_name,
                        'depth'             => 0,
                        'container'         => '',
                        'menu_class'        => 'nav navbar-nav navbar-right hhwt-nav-menu-right',
                        'items_wrap'        => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'walker'            => new HHWTNavWalker(),
                        'echo'              => false)
                    );

        $menu_data = str_replace('<span class="menu-logged-user">{username}</span>', $user_name, $menu_data);
        $menu_data = str_replace('{userimage}', $user_avatar, $menu_data);
        $menu_data = str_replace('{messageicon}', $message_img, $menu_data);
        $menu_data = str_replace('{notificationicon}', $notification_icon, $menu_data);
        $menu_data = str_replace('{shoppingcart}', $bag_icon, $menu_data);

        return $menu_data;
    }

    public function hhwtapi_user_success_action($user_data, $buckets=array())
    {
        global $hhwt_session;

        $user_response = array();

        /*user data*/
        if ($user_data) {
            $hhwt_session->set_user_session($user_data);
            $user_response['user'] = $user_data;
            $user_id = $user_response['user']['user_id'];

            /*user menu*/
            $menu_html = $this->hhwtapi_format_menu_data();
            $user_response['menu'] = $menu_html;
        }

        /* bucket articles */
        if ($buckets['status']==1) {

            /*js buckets*/
            $bucket_data = array_column($buckets['buckets'], 'listing_id', 'bucket_list_id');
            $user_response['bucket_article'] = $bucket_data;

            /*session buckets*/
            $buckets_list = ($buckets['buckets']);
            $session_bucket = array();
            foreach ($buckets_list as $bucket_list) {
                $temp = array(
                    'bucket_list_id' => $bucket_list['bucket_list_id'],
                    'bucket_id'      => $bucket_list['bucketId'],
                    'bucket_name'    => $bucket_list['name']
                );
                $session_bucket[$bucket_list['listing_id']] = $temp;
            }

            $hhwt_session->set_bucket_article($session_bucket);
        }

        $api_data   =   array('userId'=>$user_id);
        $buckets_list = $this->hhwtapi_get_response($this->apiurl_getbuckets, $api_data);
        if ($buckets_list['status'] == 1) {
            $bucket_data = array_column($buckets_list['buckets'], 'name', 'bucketId');
            $hhwt_session->set_user_bucket($bucket_data);
        }

        /* set user data in session */
        if ($user_response['user']) {
            $hhwt_session->set_session_data($user_response);
        }

        return $user_response;
    }

    public function hhwtapi_register_gamify($type, $api_data, $exist = false)
    {
        if($exist)
            return;

        if ($type == 'register' || $type == 'fbregister' || $type == 'blogadmin') {

            $reg_type = ($type == 'fbregister') ? 'fbregister' : 'website';

            $gamify_data = array('type' => $reg_type);

            if(isset($api_data['email']))
                $gamify_data['email'] = $api_data['email'];

            if(isset($api_data['country']))
                $gamify_data['country'] = $api_data['country'];

            if(isset($api_data['fb_id']))
                $gamify_data['fbid'] = $api_data['fb_id'];

            $gamify_res = $this->hhwtapi_gamify_response('newuser', $gamify_data);
        }
    }

    public function hhwtapi_manage_user($type, $api_data)
    {
        $user_data = array();
        $failure = 'failure';

        if (!empty($type)) {

            switch ($type) {

                case 'login':
                    $api_url = $this->apiurl_login;
                    break;

                case 'register':
                    $api_url = $this->apiurl_register;
                    $failure = 'exist';
                    break;

                case 'fbregister':
                    $api_url = $this->apiurl_fb_register;
                    break;

                case 'sharedlogin':
                    $api_url = $this->apiurl_shared_login;
                    break;

                case 'blogadmin':
                    $api_url = $this->apiurl_register;
                    $failure = 'exist';
                    break;

                case 'adminshared':
                    $api_url = $this->apiurl_shared_login;
                    $failure = 'notexist';
                    break;
                
                default:
                    # code...
                    break;
            }

            $api_result = $this->hhwtapi_get_response($api_url, $api_data);

            if ($api_result['status']) {

                $email = (isset($api_result['email']) && !empty($api_result['email'])) ? $api_result['email'] : $api_data['email'];
                $user_name    = $api_result['name'];
                $trim_name    = explode(" ",$user_name);
                $display_name = $trim_name[0];
                /*format session data*/
                $user_data = array(
                                    'user_id' => $api_result['user_id'],
                                    'user_email'=> $email,
                                    'user_name'=> $display_name,
                                    'user_type'=> $api_result['user_type'],
                                    'user_image'=> $api_result['image']
                                );

                $user_response = $this->hhwtapi_user_success_action($user_data, $api_result['bucket']);

                /*gamify register_data*/
                $reg_msg = (isset($api_result['msg']) && strtolower($api_result['msg']) == 'welcome back!') ? true : false;
                $this->hhwtapi_register_gamify($type, $api_data, $reg_msg);

                if($type =='blogadmin' || $type == 'adminshared')
                    return $user_response;

                if ($user_response)
                    die(json_encode($user_response));
                else
                    die('failure');

            } else {

                if($type =='blogadmin' || $type == 'adminshared')
                    return $failure;

                die($failure);
            }
        }

        die('failure');
    }

    /* User Registration
    1.format user data to api db
    2.call api
    3.set user session, buckets, bucket_articles
    4.format user menu
    5.format cookie data
    **/
    public function hhwtapi_register_user()
    {
        
        $return = array();

        if (isset($_POST['user_data'])) {

            $decrpted = $this->hhwtapi_decrypt_data($_POST['user_data']);

            if (is_string($decrpted)) {
                parse_str($decrpted, $parse_data);
                $post_data = $parse_data['register'];
            } 

            /* API Data */
            $api_data = $this->hhwtapi_format_register_data($post_data);

            /* API Call */
            $this->hhwtapi_manage_user('register', $api_data);

        }
        
        die('failure');
    }

    public function hhwtapi_register_fbuser()
    {
        global $hhwt_session;

        if (isset($_POST['user_data'])) {
            $post_data = ($_POST['user_data']);

            /* API Data */
            $api_data = $this->hhwtapi_format_register_data($post_data);

            /* API Call */
            $this->hhwtapi_manage_user('fbregister', $api_data);

        } 

        die('failure');
    }

    /* Login user */
    public function hhwtapi_user_login()
    {
        $return = array();
    
        if (isset($_POST['user_data'])) {

            $decrpted = $this->hhwtapi_decrypt_data($_POST['user_data']);

            parse_str($decrpted, $parse_data);

            $user_email = (isset($parse_data['user_email'])) ? $parse_data['user_email'] : '';
            $user_password = (isset($parse_data['user_password'])) ? $parse_data['user_password'] : '';

            /* API Data */
            $api_data = array(
                            'email'=> $user_email,
                            'password'=> $user_password
                        );

            /* API Call */
            $this->hhwtapi_manage_user('login', $api_data);

        }

        die('failure');
    }

    /* Trigger user login */
    public function hhwtfe_trigger_user_login()
    {
        global $hhwt_session;
        $session_data = $hhwt_session->get_session_data();

        if ($session_data['user']) {
            $session_article = $hhwt_session->get_bucket_article();
            if ($session_article){
                $bucket_article = array_keys($session_article);
                $session_data['bucket_article'] = $bucket_article;
            }
            die(json_encode($session_data));
        }

        $user_id = (isset($_POST['user_id'])) ? $_POST['user_id'] : '';

        $return = array();

        if (!empty($user_id)) {

            /* API Data */
            $api_data = array('user_id'=> $user_id);

            /* API Call */
            $this->hhwtapi_manage_user('sharedlogin', $api_data);
        }

        die('failure');
    }

    public function hhwtapi_destory_session()
    {
        session_destroy();
        $bag_icon = "<i class='fa fa-shopping-cart' aria-hidden='true'></i>Bag";
        $menu_data = wp_nav_menu( array(
            'menu'              => 'hhwt-login-menu',
            'theme_location'    => 'login_menu',
            'depth'             => 0,
            'container'         => '',
            'menu_class'        => 'nav navbar-nav navbar-right hhwt-nav-menu-right',
            'items_wrap'        => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'walker'            => new HHWTNavWalker(),
            'echo'              => false)
        );
        $menu_data = str_replace('{shoppingcart}', $bag_icon, $menu_data);
        die($menu_data);
    }


    public function hhwtapi_userid()
    {
        global $hhwt_session;

        $user_id = false;
        $user_session = $hhwt_session->get_user_session();

        if ($user_session)
            $user_id = $user_session['user_id'];
        
        return $user_id;
    }

    public function hhwtapi_username()
    {
        global $hhwt_session;

        $user_name = '';
        $user_session = $hhwt_session->get_user_session();

        if ($user_session)
            $user_name = $user_session['user_name'];
        
        return $user_name;
    }

    public function hhwtapi_existing_bucket($user_id)
    {
        global $hhwt_session;

        $session_buckets = $hhwt_session->get_user_bucket($bucket_data);

        if ($session_buckets) {
            return $session_buckets;
        }

        $bucket_data = false;
        $api_data   =   array('userId'=>$user_id);
        $buckets = $this->hhwtapi_get_response($this->apiurl_getbuckets, $api_data);

        if ($buckets['status'] == 1) {
            $bucket_data = array_column($buckets['buckets'], 'name', 'bucketId');
            $hhwt_session->set_user_bucket($bucket_data);
        }

        return $bucket_data;  
    }

    public function hhwtapi_bucket_content()
    {
        $user_id = $this->hhwtapi_userid();

        if(!$user_id)
            die('trg-login');

        $post_id = (isset($_REQUEST['post_id']) ) ? $_REQUEST['post_id'] : '';
        $exist_bucket = $this->hhwtapi_existing_bucket($user_id);

        ob_start();
        ?>
        <div class="save-to-bucket">
            <input class="hdn-bucket-name" type="hidden" name="hdn-bucket-name" value="">
            <input type="hidden" name="hdn-post-id" value="<?php echo $post_id; ?>">
            <p>Select Bucket List</p> 
            <div class="hhwt-select-list <?php echo (!$exist_bucket) ? 'without-bucket' : ''; ?>">
                <select class="form-control hhwt-select-bucketname <?php echo (!$exist_bucket) ? 'hidden' : ''; ?>" name="hhwt-bucket-id">
                    <option value="">Select Bucket</option>
                    <?php
                    if ($exist_bucket) {
                        foreach ($exist_bucket as $key => $value) {
                            printf(__('<option value="%d">%s</option>'), (int)$key, $value);
                        }
                    }
                    ?>
                </select>
                <button type="button" class="btn hhwt-create-new">Create New</button> </br>
            </div>
            <label id="hhwt-select-bucketname-error" class="error" for="hhwt-select-bucketname"></label>
            <div class="hhwt-text-input">
                <input type="text" class="form-control hhwt-input-bucketname" id="new-bucket-name" name="hhwt-input-bucketname" placeholder="New Bucket">
                <button type="button" class="btn hhwt-saved-btn btn-create-bucket">Save</button>
                <button type="button" class="btn hhwt-saved-btn btn-cancel-bucket">Cancel</button>
            </div>
            <button type="submit" class="btn hhwt-save-btn btn-save-article-to-bucket"><i class="fa fa-heart" aria-hidden="true"></i> Save to Bucket List</button><br>
            <img class="form-loading" src="<?php echo TEMPLATEURL . '/images/loading.gif'; ?>">
        </div>
        <script type="text/javascript">
            /* Save article to bucket */
            jQuery("#hhwt-form-create-bucket").validate({
                rules:{
                    'hhwt-select-bucketname':{
                        required:true
                    }
                },
                messages:{
                    'hhwt-select-bucketname':{
                        required:'please select bucket'
                    }
                },
                submitHandler: function(form) {
                    var bucket_data = hhwt_serialize(form);
                    var $_this = jQuery(form);

                    var user_trip = jQuery('.trip-plan-travel p.selected').text();
                    var user_id = $_this.find('[name="hdn-id"]').val();
                    var city_slug = $_this.find('[name="hdn-city-slug"]').val();

                    if (validate_value(city_slug)) {
                        city_name = city_slug.replace("-", " ");
                        city_name = hhwt_capitalize(city_name);
                    }

                    jQuery.ajax({
                        url: hhwt_ajaxurl,
                        type: "POST",
                        data: {
                            'action': 'hhwtapi_ajax_method',
                            'gotomethod': 'hhwtapi_save_article_to_bucket',
                            'bucket_data': bucket_data
                        },
                        beforeSend: function(){
                            jQuery(form).find(".form-loading").addClass('show');
                            jQuery(form).find("[type='submit']").prop('disabled', true);
                        },
                        success: function(response) {
                            if (response != 'undefined' && response!='failure') {
                                var response_data = JSON.parse(response);

                                jQuery('.modal').modal('hide');
                                jQuery("html, body").animate({
                                    scrollTop: 0
                                }, 1500);

                                var article_id = $_this.find('[name="hdn-post-id"]').val();

                                jQuery('.save-bucket[data-post-id="'+article_id+'"]')
                                .attr('data-in-bucket', true)
                                .find('i').removeClass('fa-heart-o').addClass('fa-heart');

                                if(jQuery('.save-bucket[data-post-id="'+article_id+'"]').hasClass('hhwt-save-button')) {
                                    jQuery('.save-bucket.hhwt-save-button[data-post-id="'+article_id+'"]').html('<i class="fa fa fa-heart" aria-hidden="true"></i> Saved');
                                }

                                jQuery("#hhwt-modal-saved-list").modal('show');
                                jQuery("#hhwt-modal-saved-list .modal-body").html(response_data.article);
                                jQuery("#hhwt-modal-notification .notification-modal-footer").css('display', 'block');
                                jQuery(".hhwt-nav-menu-right .hhwtmainmenu-notification span.fa-circle").css('display', 'block');
                                user_status = 'Success';
                            } else {
                                jQuery('.modal').modal('hide');
                                jQuery('#hhwt-modal-error').modal('show');
                                user_status = 'Failure';
                            }
                        },
                        error: function() {
                            jQuery('.modal').modal('hide');
                            jQuery('#hhwt-modal-error').modal('show');
                        },
                        complete: function(){
                            jQuery(form).find(".form-loading").removeClass('show');
                            article_title = jQuery(form).find('[name="hdn-title"]').attr( 'data-article');
                            bucket_name   = jQuery(form).find('[name="hhwt-select-bucketname"] option:selected').text();
                            eventLabel = {
                                'article' : article_title,
                                'Bucket' : bucket_name
                            };
                            dl_data = format_data_layer(eventLabel, user_status, 'save article');
                            hhwt_push_datalayer(dl_data);
                        }
                    }).done(function(response) {
                        var response_data = JSON.parse(response);
                        jQuery.ajax({
                            url: hhwt_ajaxurl,
                            type: "POST",
                            data: {
                                'action': 'hhwtapi_ajax_method',
                                'gotomethod': 'hhwtapi_save_gamify_data',
                                'gamify_data': response_data.gamify
                            },
                        });
                    });
                }
            });
        </script>
        <?php
        die(ob_get_clean());
    }

    public function hhwtapi_create_new_bucket()
    {
        global $hhwt_session;
        $user_id = $this->hhwtapi_userid();

        if(!$user_id)
            die('trg-login');

        $bucket_name = (isset($_REQUEST['bucket_name'])) ? $_REQUEST['bucket_name'] : '';

        if (!empty($bucket_name) ) {

            $api_data   =   array(
                            'user_id'=>$user_id,
                            'bucketname'=>$bucket_name,
                            'bucket_type'=>BUCKET_TYPE
                        );

            $bucket_res = $this->hhwtapi_get_response($this->apiurl_managebucket, $api_data);
            if ($bucket_res['status']=='1') {
                $bucket_id = $bucket_res['bucket_id'];
                $hhwt_session->push_user_bucket($bucket_id, $bucket_name);
                die("{$bucket_id}");
            }
        }

        die('failure');
    }

    public function hhwtapi_article_content($post_data)
    {
        global $hhwt_frontend;

        $user_id = $this->hhwtapi_userid();

        if (!$user_id)
            die('failure');

        $title = $category = $thumb = $article_data = '';
        $has_trip_plan = true;
        
        $post_id = (isset($post_data['post_id'])) ? $post_data['post_id'] : $_REQUEST['post_id'];
        $skip_trip = (isset($post_data['skip_trip'])) ? $post_data['skip_trip'] : 0;

        if($skip_trip)
            $has_trip_plan = false;

        if ($post_id) {
            $thumb.= get_the_post_thumbnail($post_id, 'simple-small', array("alt"=>"hhwt article", 'height'=>'50', 'width'=>'50'));
            $title.= get_the_title($post_id);
            $categories = get_the_category($post_id);

            if ($categories) {
                $category.= $hhwt_frontend->hhwtfe_print_category($categories[0]->term_id);

                if($has_trip_plan)
                    $trip_not_exist = $this->hhwtapi_trip_plan_exist($categories);
            }
        }
        ob_start();
        if($trip_not_exist) {
            $article_data.= $trip_not_exist;
        }
        ?>
        <div class="save-bucket-article">
            <?php echo $thumb; ?>
            <div>
                <p><?php echo $category; ?></p>
                <h3><?php echo $title; ?></h3>
            </div>
        </div>
        <?php
        $article_data.= ob_get_clean();
        if ($post_data['skip_trip'])
            return $article_data;
        die($article_data);
    }

    public function hhwtapi_planner_user_name($api_user_id)
    {
        global $wpdb;

        if (!empty($api_user_id)) {
            $name_query = sprintf(__("SELECT name FROM user WHERE id = %s "), $api_user_id);
            $name = $wpdb->get_var($name_query);
            $name = trim($name);
            if(empty($name) || is_null($name))
                return 'Halal User';
            else
                return $name;
        }

        return 'Halal User';
    }

    public function hhwtapi_gamify_response($type, $fields = array())
    {
        $user_id = $this->hhwtapi_userid();
        $user_name = $this->hhwtapi_username();

        $achievement = $this->user_achievements[$type];

        $achievement_data = array(
            'playerid' => $user_id,
            "section" => "achievements"
        );

        if ($type == 'stream') {
            $achievement_data['action'] = "stream";
        } else {
            $achievement_data['allowduplicates'] = true;
            $achievement_data['achievement'] = $achievement;
            $achievement_data['playername'] = $user_name;
            $achievement_data['action'] = "save";
            $achievement_data['fields'] = $fields;
        }
        $gami_data = json_encode($achievement_data);
        $encode_data = base64_encode($gami_data);
        $post_data = array('event_data'=>$encode_data);

        $gamify_res = $this->hhwtapi_get_response($this->apiurl_gamify, $post_data);
        
        return $gamify_res;
    }

    public function hhwtapi_travel_plan_date($option)
    {
        switch ($option) {
            case 1:
                $travel_date = date("Y-m-d", strtotime("+1 week"));
                break;

            case 2:
                $travel_date = date("Y-m-d", strtotime("+2 weeks"));
                break;

            case 3:
                $travel_date = date("Y-m-d", strtotime("+1 month"));
                break;

            case 4:
                $travel_date = date("Y-m-d", strtotime("+3 months"));
                break;

            case 5:
                $travel_date = date("Y-m-d", strtotime("+6 months"));
                break;

            default:
                $travel_date = date("Y-m-d");
                break;
        }

        return strtotime($travel_date);
    }

    public function hhwtapi_save_article_to_bucket()
    {
        global $hhwt_session;

        $user_id = $this->hhwtapi_userid();

        if(!$user_id)
            die('trg-login');

        if(!isset($_POST['bucket_data']))
            die('failure');

        $decrypted = $this->hhwtapi_decrypt_data($_POST['bucket_data']);
        parse_str($decrypted, $params);

        $bucket_id = (isset($params['hhwt-bucket-id'])) ? $params['hhwt-bucket-id'] : '';
        $post_id = (isset($params['hdn-post-id'])) ? $params['hdn-post-id'] : '';
        $bucket_name = (isset($params['hdn-bucket-name'])) ? $params['hdn-bucket-name'] : '';

        $article_content = '';
        $save_result = array();

        /*Trip Plan data*/
        $trip_plan = (isset($params['hdn-trip-plan'])) ? $params['hdn-trip-plan'] : '';
        $city_slug = (isset($params['hdn-city-slug'])) ? $params['hdn-city-slug'] : '';

        if (!empty($bucket_id) && !empty($post_id) ) {

            $data   =   array(
                            'user_id'=>$user_id,
                            'bucket_type'=>BUCKET_TYPE,
                            'listing_id'=>$post_id,
                            'bucket_id'=>$bucket_id
                        );

            $bucket_res = $this->hhwtapi_get_response($this->apiurl_managebucket, $data);

            if ($bucket_res['status']=='1') {

                $listing_id = $bucket_res['bucket_listing_id'];

                if ($listing_id) {

                    /* Insert Trip plan data */
                    if (!empty($trip_plan) && !empty($city_slug) ) {

                        $trip_plan_data =   array(
                                                'user_id'=>$hhwt_frontend->hhwt_api_userid(),
                                                'city_slug'=>$city_slug,
                                                'bucket_list_id'=>$listing_id,
                                                'trip_plan'=>$trip_plan
                                            );

                        $trip_plan_res = $this->hhwtapi_get_response($this->apiurl_addtrip, $trip_plan_data);
                    }

                    /*push article data to session*/
                    $session_bucket = array(
                                        'bucket_list_id'=>$listing_id,
                                        'bucket_id'=>$bucket_id,
                                        'bucket_name'=>$bucket_name,
                                    );

                    $hhwt_session->push_bucket_article($post_id, $session_bucket);

                    /* Trigger mongo track */
                    $gamifydata = array();
                    $gamifydata['id'] = $post_id;
                    $gamifydata['status']  = 'save';

                    if(!empty($city_slug))
                        $gamifydata['city_slug'] = $city_slug;

                    if (!empty($trip_plan)) {
                        $gamifydata['trip_id'] = $trip_plan;
                        $gamifydata['trip_plan'] = $this->user_travel_plan[$trip_plan];
                        $gamifydata['trip_date'] = $this->hhwtapi_travel_plan_date($trip_plan);
                    }
                    
                    $article_content.= $this->hhwtapi_article_content( array('post_id'=>$post_id, 'skip_trip'=> 1) );
                    $save_result['article'] = $article_content;
                    $save_result['gamify'] = $gamifydata;
                }
                die(json_encode($save_result));
            }
            die('failure');
        }
        die('failure');
    }


    public function hhwtapi_save_gamify_data($post_data)
    {
        global $hhwt_session;
        $gamifydata = $post_data['gamify_data'];
        $post_id = $gamifydata['id'];
        $bucket_data = $this->hhwtapi_bucketlist_data($post_id);

        /*gamify data*/
        $gamifydata['type'] = 'article';
        $gamifydata['bucket_list_id'] = $bucket_data['bucket_list_id'];
        $gamifydata['bucket_id'] = $bucket_data['bucket_id'];
        $gamifydata['bucket_name'] = $bucket_data['bucket_name'];
        $gamify_status = $gamifydata['status'];
        unset($gamifydata['status']);

        if ($gamify_status == 'save' || $gamify_status == 'unsave')
            $gamify_res = $this->hhwtapi_gamify_response($gamify_status, $gamifydata);

    }

    public function hhwtapi_bucketlist_data($post_id)
    {
        global $hhwt_session;

        $session_articles = $hhwt_session->get_bucket_article();
        $bucket_list_data = $session_articles[$post_id];

        return $bucket_list_data;
    }

    public function hhwtapi_remove_article_from_bucket()
    {
        global $hhwt_session;
        $user_id = $this->hhwtapi_userid();
        $delete_result = array();

        if(!$user_id)
            die('trg-login');

        if(!isset($_POST['article_data']))
            die('failure');

        $decrypted = $this->hhwtapi_decrypt_data($_POST['article_data']);

        parse_str($decrypted, $param);

        $post_id = $param['hdn-post-id'];

        if ($post_id) {
                /*get bucket data*/
            $bucket_list_data = $this->hhwtapi_bucketlist_data($post_id);

            if ($bucket_list_data) {

                /* Trigger mongo track */
                $gamifydata = array();
                $gamifydata['id'] = $post_id;
                $gamifydata['status']  = 'unsave';

                $bucket_list_id = $bucket_list_data['bucket_list_id'];
                $bucket_name = $bucket_list_data['bucket_name'];

                $api_data = array(
                                "user_id"=>$user_id,
                                "bucket_listing_id"=>$bucket_list_id
                            );

                $api_result = $this->hhwtapi_get_response($this->apiurl_bucketlistdelete, $api_data);

                if ($api_result['status']) {
                    $hhwt_session->pop_bucket_article($post_id);
                    $delete_result['bucket'] = $bucket_name;
                    $delete_result['gamify'] = $gamifydata;
                    die(json_encode($delete_result));
                }
            }
        }
        die('failure');
    }

    /* forget password */
    public function hhwtapi_forgot_password()
    {
        if (isset($_POST['user_data'])) {

            $decrpted = $this->hhwtapi_decrypt_data($_POST['user_data']);
            parse_str($decrpted, $param);

            $email = $param['forgetemail'];

            if (!empty($email)) {
                 
                $data   =   array( 'emailid'=> $email);
                $api_result = $this->hhwtapi_get_response($this->apiurl_forgetpassword, $data);

                if ($api_result['status'] == 1) {
                    die($api_result['msg']);
                }
            }
        }
        die('failure');
    }

    public function hhwtapi_get_cities()
    {
        global $hhwt_session;

        $session_cities = $hhwt_session->get_user_cities();

        if ($session_cities) {
           return $session_cities; 
        }

        $api_result = $this->hhwtapi_get_response($this->apiurl_getcities);

        if ($api_result['Status'] == 1) {
            $cities_list = $api_result['categorylist'];
            $cities = array_column($cities_list, 'city_slug');
            $hhwt_session->set_user_cities($cities);
        }

        return $hhwt_session->get_user_cities();
    }

    public function hhwtapi_trip_plan_exist($post_category)
    {
        $user_id = $this->hhwtapi_userid();

        if ($post_category) {

            /* get planner cities */
            $list_of_city = $this->hhwtapi_get_cities();

            if(count($list_of_city)<=0)
                return false;

            /* get article category */
            foreach ($post_category as $single_cat) {
                $cat_name = strtolower($single_cat->name);
                $cat_name = str_replace(" ", "-", $cat_name);
                if (in_array($cat_name, $list_of_city)) {
                    $city_slug = $cat_name;
                    break;
                }
            }

            if (!empty($city_slug) && $list_of_city) {
             
                //check trip exist
                $trip_data  =   array(
                                    "city_slug"=>$city_slug,
                                    'user_id'=>$user_id
                                );

                $trip_res = $this->hhwtapi_get_response($this->apiurl_tripcheck, $trip_data);

                if ($trip_res['status'] == 0) {
                   ob_start();
                   ?>
                   <input type="hidden" name="hdn-city-slug" value="<?php echo $city_slug; ?>">
                   <div class="trip-plan-travel planner-modal-body">
                        <h4>Are you planning to travel anytime soon?</h4>
                        <p data-value="1">Within a week</p>
                        <p data-value="2">Within 2 weeks</p>
                        <p data-value="3">Within 1 month</p>
                        <p data-value="4">Within 3 months</p>
                        <p data-value="5">Within 6 months</p>
                        <p data-value="6">I'm not sure</p>
                        <p data-value="7">I'm not travelling</p>
                        <div class="alert"></div>
                        <button type="button" name="next" class="btn hhwt-btn-trip-choose hhwt-next hhwt-submit-btn" value="Next">Proceed</button>
                    </div>
                   <?php
                   return ob_get_clean();
                }
            }
        }

        return false;
    }

    public function hhwtapi_notification_stream()
    {
        $notifications = $this->hhwtapi_gamify_response("stream");
        if ($notifications['status'] == 1) {
            return $notifications['result'];
        } else {
            return false;
        }
    }

    public function hhwtapi_user_notifications()
    {
        $return_string = '';
        $notifications = $this->hhwtapi_notification_stream();

        if ($notifications) {
            foreach ($notifications as $notification) {
                $city_name = (isset($notification['dataelement']['city']) ) ? $notification['dataelement']['city'] : '';
                $name = (isset($notification['dataelement']['name']) ) ? $notification['dataelement']['name'] : '';
                $data_img = (isset($notification['dataelement']['photos']['0']) ) ? $notification['dataelement']['photos']['0'] : '';
                $article_count = (isset($notification['achievement']['bucketlistCount']) ) ? $notification['achievement']['bucketlistCount'] : '';
                $bucket_name = (isset($notification['achievement']['bucket_name']) ) ? $notification['achievement']['bucket_name'] : '';
                $points = (isset($notification['achievement']['points']) ) ? $notification['achievement']['points'] : '';
                $notification_time = (isset($notification['achievement']['formated_date']) ) ? $notification['achievement']['formated_date'] : '';
                $achievement_type = (isset($notification['achievement']['name']) ) ? $notification['achievement']['name'] : '';
                $review_like_count = (isset($notification['achievement']['totalreviewlike']) ) ? $notification['achievement']['totalreviewlike'] : '';

                switch ($achievement_type) {
                    case 'Add Review':
                        $notification_class  = "notification-dataelement";
                        $notification_icon   = "<span class='notification-icon'>+{$points}</span>";
                        $notification_text   = "Sweet, You received {$points} points for writing a review with photos attached";
                        $notification_link   = "Write a Review";
                        break;
                    case 'Save':
                        $notification_class  = '';
                        $notification_icon   = "<span class='fa fa-heart notification-icon'></span>";
                        $notification_text   = "You saved {$article_count} items into {$bucket_name} Check them out again?";
                        $notification_link   = "View Bucket List";
                        break;
                    case 'Like Review':
                        if ($review_like_count <= 5)
                            $notification_icon   = "<img class='notification-img' src='{$data_img}'>";
                        else 
                            $notification_icon   = "<span class='fa fa-thumbs-up notification-icon'></span>";
                        $notification_class  = '';
                        $notification_text   = "{$review_like_count} people liked your review of {$name}, {$city_name}. Nice!";
                        $notification_link   = "View My Points";
                        break;
                    default:
                        $notification_icon   = '';
                        $notification_class  = '';
                        $notification_text   = '';
                        $notification_link   = '';
                        break;
                }
                if ($notification_icon != ''){
                    $return_string.="<div class='notification-list {$notification_class}'>
                                        {$notification_icon}
                                        <div class='notification-content'>
                                            <p class='hhwt-notification-text'>{$notification_text}</p>
                                            <a href=".BUCKET_DASHBOARD.">{$notification_link}</a>
                                            <p class='notification-time'>{$notification_time}</p>
                                        </div>
                                    </div>";
                }
            }
        } else { 
            $return_string.='<div class="no-notification"><span class="fa fa-bell-o notification-icon" aria-hidden="true"></span>
                <p class="hhwt-notification-text">You don\'t have any notifications</p></div>';
        }
        die($return_string);
    }

    public function hhwtapi_trigger_admin_login( $user, $username, $password ) 
    {
        global $wpdb;
        if(!is_wp_error( $user )) {
            $user_id = $user->ID;
            if ($user_id) {

                $email = $user->user_email;

                $api_data = array('email'=> $email);
                $user_status = $this->hhwtapi_manage_user('adminshared', $api_data);

                if ($user_status == 'notexist') {
                    
                    $user_data = get_user_meta($user_id);
                    $fname = $user_data['first_name'][0];
                    $lname = $user_data['last_name'][0];

                    $api_data = array(
                        'fname'=> $fname,
                        'lname'=> $lname,
                        'email'=> $email,
                        'type'=>'blog-admin',
                        'subscribe'=> 'yes',
                        'password'=> (!empty($password)) ? $password : '',
                        'country'=> 'SG',
                        'username'=> $user->user_login,
                        'name'=> $user->display_name
                    );

                    /* API Call */
                    $user_status = $this->hhwtapi_manage_user('blogadmin', $api_data);
                }

                if ($user_status['user']) {
                    $user_data = $user_status['user'];
                    $cookie_data =  array(
                                        'email'=> $user_data['user_email'],
                                        'id'=> $user_data['user_id'],
                                    );
                    $str_cookie = json_encode($cookie_data);
                    $enct_cookie = base64_encode($str_cookie);

                    setcookie('_hhwt_ssid_', $enct_cookie, time() + (86400 * 1), "/", ".havehalalwilltravel.com"); // 86400 = 1 day
                }
            }
        }

        return $user;
    }

} //End of Class
