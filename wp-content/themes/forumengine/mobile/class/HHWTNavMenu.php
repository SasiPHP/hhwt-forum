<?php
class HHWTNavMenu
{
    protected $site_url;

    protected $site_logo;

    protected $logout_url;

    protected $search_url;

    public  function __construct()
    {
        add_action( 'rest_api_init', array($this, 'hhwtnav_rest_menus_init') );
    }

    public static function hhwtnav_api_namespace()
    {
        return 'hhwtapi/v1';
    }

    public function hhwtnav_set_api_params($params)
    {
        $this->site_url = get_site_url();

        $this->site_logo = (isset($params['site_logo'])) ? $params['site_logo'] : TEMPLATEURL.'/images/header_logo.png';

        $this->logout_url = (isset($params['logout_url'])) ? $params['logout_url'] : '';
        $this->search_url = (isset($params['search_url'])) ? $params['search_url'] : home_url();
    }

    public function hhwtnav_api_response($url, $data)
    {
        if (is_array($post_params)) {
            $post_params = http_build_query($post_params);
        }

        $headers =  array("Content-Type: application/x-www-form-urlencoded");
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, $method);
        
        if($post_params)
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 

        $response = curl_exec($ch);

        if (curl_error($ch)) {
            return false;
        } else {
            $response_data = json_decode($response, true);
        }

        curl_close($ch);

        return $response_data;
    }

    public function hhwtnav_start_el()
    {
        return '<div class="container-fluid hhwt-navbar-fluid">
                    <div class="container hhwt-navbar-container">';
    }

    public function hhwtnav_end_el()
    {
        return '</div>
        </div>';
    }

    public function hhwtnav_menunav_start_el($device='hidden-xs hidden-sm')
    {
        return "<nav class='navbar hhwt-navbar {$device}'>";
    }

    public function hhwtnav_menunav_end_el($device='hidden-xs hidden-sm')
    {
        return '</nav>';
    }

    public function hhwtnav_search_form()
    {
        ob_start();
        ?>
        <li class="hhwt-nav-menu-li">
            <form role="search" method="get" id="hhwt-searchform" class="hhwt-searchform" action="<?php echo $this->search_url; ?>">
                <p class="search input-group">
                    <span class="input-group-addon trg-submit"><i class="fa fa-search"></i></span>
                    <input autocomplete="off" type="text" class="form-control" name="s" placeholder="Search..." value="" >
                    <span class="input-close">x</span>
                </p>
                <!-- Header serch Response Shop - Append here  -->
                <div id='header-act-city-search' class="tt-menu search-close" >
                    <div class="tt-loading">
                        <img src="https://beta.havehalalwilltravel.com/shop/images/load_more.gif" alt="loading-image">
                    </div>
                    <div class="tt-cities"></div>
                    <div class="tt-activities"></div>
                </div>
            </form>
            <div id='header-act-city-search' class="tt-menu search-close" >
                <div class="tt-loading">
                    <img src="https://beta.havehalalwilltravel.com/shop/images/load_more.gif" alt="loading-image">
                </div>
                <div class="tt-cities"></div>
                <div class="tt-activities"></div>
            </div>
        </li>
        <?php
        return ob_get_clean();
    }

    public function hhwtnav_menu_by_name($menu_name)
    {
        return wp_nav_menu( array(
            'menu'              => 'hhwt-login-menu',
            'theme_location'    => $menu_name,
            'depth'             => 0,
            'container'         => 'div',
            'container_class'   => "hidden hhwt-nav-right-container hhwt-user-menu {$menu_name}",
            'menu_class'        => "nav navbar-nav navbar-right hhwt-nav-menu-right",
            'items_wrap'        => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'walker'            => new HHWTNavWalker(),
            'echo'              => false
            )
        );
    }

    public function hhwtnav_session_menu()
    {
        ob_start();

        /* session desktop login menu */
        echo $this->hhwtnav_menu_by_name('login_menu');

        /* session desktop traveler menu */
        echo $this->hhwtnav_menu_by_name('marketplace_traveler');

        /* session desktop host menu */
        echo $this->hhwtnav_menu_by_name('marketplace_host');

        return ob_get_clean();
    }

    public function hhwtnav_global_menu($search=true, $logo=true)
    {
        $search_form = ($search) ? $this->hhwtnav_search_form() : '';

        ob_start();

        if($logo) {
        ?>
        <div class="navbar-header hhwt-navbar-header">
            <a class="navbar-brand" href="<?php echo $this->site_url;?>">
                <img src="<?php echo $this->site_logo; ?>">
            </a>
        </div>
        <?php
        }

        wp_nav_menu( array(
            'menu'              => 'hhwt-nav-menu',
            'theme_location'    => 'header_menu',
            'depth'             => 0,
            'container'         => 'div',
            'container_class'   => 'navbar-collapse collapse in',
            'menu_class'        => 'nav navbar-nav hhwt-nav-menu',
            'items_wrap'        => '<ul id="%1$s" class="%2$s">'.$search_form.'%3$s</ul>',
            'walker'            => new HHWTNavWalker())
        );

        return ob_get_clean();
    }

    public function hhwtnav_desktop_menu()
    {
        $desktop_menu = '';

        /* global menu */
        $desktop_menu.=$this->hhwtnav_global_menu();

        /* session menu */
        $desktop_menu.=$this->hhwtnav_session_menu();

        return $desktop_menu;
    }

    public function hhwtnav_mobile_logo()
    {
        ob_start();
        ?>
        <span class="hhwt-mob-search fa fa-search" data-toggle="collapse" data-target="#hhwt-mob-nav-search"></span>
        <a href="<?php echo $this->site_url;?>">
            <img class="hhwt-mob-logo" src="<?php echo $this->site_logo; ?>">
        </a>
        <?php
        return ob_get_clean();
    }

    public function hhwtnav_mobile_search()
    {
        ob_start();
        ?>
        <div class="collapse collapse-search" id="hhwt-mob-nav-search">
            <form role="search" method="get" id="hhwt-searchform-mobile" class="hhwt-searchform" action="<?php echo $this->search_url;?>">
                <p class="input-group">
                    <span class="input-group-addon trg-submit"><i class="fa fa-search"></i></span>
                    <input class="form-control" autocomplete="off" name="s" placeholder="Search..." type="text">
                </p>
                <!-- Header serch Response Shop - Append here  -->
                <div id='header-act-city-search-mobile' class="tt-menu search-close" >
                    <div class="tt-loading">
                        <img src="https://beta.havehalalwilltravel.com/shop/images/load_more.gif" alt="loading-image">
                    </div>
                    <div class="tt-cities"></div>
                    <div class="tt-activities"></div>
                </div>
            </form>
            <div id='header-act-city-search-mobile' class="tt-menu search-close" >
                <div class="tt-loading">
                    <img src="https://beta.havehalalwilltravel.com/shop/images/load_more.gif" alt="loading-image">
                </div>
                <div class="tt-cities"></div>
                <div class="tt-activities"></div>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }

    public function hhwtnav_mobile_menu()
    {
        $mobile_menu = '';

        /* mobile header */
        $mobile_menu.= '<div class="hhwt-mob-menu">';

            /* logo */
            $mobile_menu.=$this->hhwtnav_mobile_logo();

            $mobile_menu.='<button type="button" class="navbar-toggle collapse-menu" data-toggle="collapse" data-target="#hhwt-mobile-mmenu">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span> 
                        </button>';

        $mobile_menu.= '</div>';

        /* mobile search */
        $mobile_menu.= $this->hhwtnav_mobile_search();

        
        /* global menu */
        $mobile_menu.=$this->hhwtnav_global_menu(false, false);

        /* session menu */
        $mobile_menu.= '<div id="hhwt-mobile-mmenu" class="collapse navbar-collapse">';
        $mobile_menu.=$this->hhwtnav_session_menu();
        $mobile_menu.= '</div>';

        return $mobile_menu;
    }

    public function hhwtnav_html_header_menu()
    {
        $html_menu = '';

        /* container start */
        $html_menu.= $this->hhwtnav_start_el();

            /* desktop nav menu*/
            $html_menu.= $this->hhwtnav_menunav_start_el();

               $html_menu.= $this->hhwtnav_desktop_menu();

            $html_menu.= $this->hhwtnav_menunav_end_el();



            /* mobile nav menu */
            $html_menu.= $this->hhwtnav_menunav_start_el('visible-xs visible-sm');

                $html_menu.= $this->hhwtnav_mobile_menu();

            $html_menu.= $this->hhwtnav_menunav_end_el('visible-xs visible-sm');


        /* container end */
        $html_menu.= $this->hhwtnav_end_el();

        return $html_menu;
    }

    public function hhwtnav_html_footer_menu()
    {
        ob_start();
        ?>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 hhwt-footer-categories">
            <div class="col-sm-4 col-xs-12 hhwt-footer-grid clearfix">
                <h4 class="titles">Destinations</h4>
                <?php
                wp_nav_menu( array(
                    'menu'              => 'hhwt-login-menu',
                    'theme_location'    => 'footer_grid_1',
                    'depth'             => 0,
                    'container'         => 'div',
                    'container_class'   => "menu-footer-grid-1-container",
                    'items_wrap'        => '<ul id="%1$s" class="%2$s">%3$s</ul>'
                ));
                ?>
            </div>
            <div class="col-sm-4 col-xs-12 hhwt-footer-grid clearfix">
                <h4 class="titles">Helpful Links</h4>
                <?php
                wp_nav_menu( array(
                    'menu'              => 'hhwt-login-menu',
                    'theme_location'    => 'footer_grid_2',
                    'depth'             => 0,
                    'container'         => 'div',
                    'container_class'   => "menu-footer-grid-1-container",
                    'items_wrap'        => '<ul id="%1$s" class="%2$s">%3$s</ul>'
                ));
                ?>
            </div>
            <div class="col-sm-4 col-xs-12 hhwt-footer-grid clearfix">
                <h4 class="titles">Work With Us</h4>
                <?php
                wp_nav_menu( array(
                    'menu'              => 'hhwt-login-menu',
                    'theme_location'    => 'footer_grid_3',
                    'depth'             => 0,
                    'container'         => 'div',
                    'container_class'   => "menu-footer-grid-1-container",
                    'items_wrap'        => '<ul id="%1$s" class="%2$s">%3$s</ul>'
                ));
                ?>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }

    public function hhwtnav_get_html_menu()
    {
        $html_menu = array();
        
        $html_menu['header'] = $this->hhwtnav_html_header_menu();
        $html_menu['footer'] = $this->hhwtnav_html_footer_menu();

        return $html_menu;
    }

    public function hhwtnav_json_header_menu()
    {
        return $this->get_menu_location('header_menu');
    }

    public function hhwtnav_json_user_menu()
    {
        $json_user = array();

        $json_user['login'] = $this->get_menu_location('login_menu');
        $json_user['traveler'] = $this->get_menu_location('marketplace_traveler');
        $json_user['host'] = $this->get_menu_location('marketplace_host');

        return $json_user;
    }

    public function hhwtnav_json_footer_menu()
    {
        $json_footer = array();
        $json_footer['footer_grid_1'] = $this->get_menu_location('footer_grid_1');
        $json_footer['footer_grid_2'] = $this->get_menu_location('footer_grid_2');
        $json_footer['footer_grid_3'] = $this->get_menu_location('footer_grid_3');

        return $json_footer;
    }

    public function hhwtnav_get_json_menu()
    {
        $json_menu = array();

        /*header menus*/
        $json_menu['header'] = $this->hhwtnav_json_header_menu();

        /*user menus*/
        $json_menu['user'] = $this->hhwtnav_json_user_menu();

        /*footer menus*/
        $json_menu['footer'] = $this->hhwtnav_json_footer_menu();

        return $json_menu;
    }

    public function hhwtnav_get_menu($params)
    {
        $http_host = $_SERVER['HTTP_HOST'];
        if ( strpos( $http_host, '13.229.104.139') !== false || strpos( $http_host, 'havehalalwilltravel') !== false || strpos( $http_host, 'localhost') !== false ) {

            $response = array('status'=>1);

            /*set plugin constants */
            $this->hhwtnav_set_api_params($params);

            if ($params['type']=='json') {
                $menu =  $this->hhwtnav_get_json_menu();
            } else {
                $menu =  $this->hhwtnav_get_html_menu();
            }

            $response['menu'] = $menu;

        } else {
            $response = array('status'=>0, 'message'=> 'unauthorized access');
        }

        return $response;
    }

    /**
    * Get menu for location.
    *
    * @since  1.0.0
    * @param  string $location The theme location menu name
    * @return array The menu for the corresponding location
    */
    public function get_menu_location( $location ) {

        $locations = get_nav_menu_locations();

        if ( ! isset( $locations[ $location ] ) ) {

            return array();
        }

        $wp_menu = wp_get_nav_menu_object( $locations[ $location ] );
        $menu_items = wp_get_nav_menu_items( $wp_menu->term_id );

        $sorted_menu_items = $top_level_menu_items = $menu_items_with_children = array();

        foreach ( (array) $menu_items as $menu_item ) {

            $sorted_menu_items[ $menu_item->menu_order ] = $menu_item;
        }

        foreach ( $sorted_menu_items as $menu_item ) {

            if ( (int) $menu_item->menu_item_parent !== 0 ) {
                $menu_items_with_children[ $menu_item->menu_item_parent ] = true;
            } else {
                $top_level_menu_items[] = $menu_item;
            }
        }

        $menu = array();

        while ( $sorted_menu_items ) :

            $i = 0;

            foreach ( $top_level_menu_items as $top_item ) :

                $menu[ $i ] = $this->format_menu_item( $top_item, false );

                if ( isset( $menu_items_with_children[ $top_item->ID ] ) ) {
                    $menu[ $i ]['children'] = $this->get_nav_menu_item_children( $top_item->ID, $menu_items, false );
                } else {
                    $menu[ $i ]['children'] = array();
                }

                $i++;

            endforeach;

            break;

        endwhile;

        return $menu;
    }


    /**
    * Returns all child nav_menu_items under a specific parent.
    *
    * @since  1.1.0
    * @param  int     $parent_id      the parent nav_menu_item ID
    * @param  array   $nav_menu_items navigation menu items
    * @param  bool    $depth          gives all children or direct children only
    * @return array   returns filtered array of nav_menu_items
    */
    public function get_nav_menu_item_children( $parent_id, $nav_menu_items, $depth = true ) {

        $nav_menu_item_list = array();

        foreach ( (array) $nav_menu_items as $nav_menu_item ) :

            if ( $nav_menu_item->menu_item_parent == $parent_id ) :

                $nav_menu_item_list[] = $this->format_menu_item( $nav_menu_item, true, $nav_menu_items );

                if ( $depth ) {

                    if ( $children = $this->get_nav_menu_item_children( $nav_menu_item->ID, $nav_menu_items ) ) {

                        $nav_menu_item_list = array_merge( $nav_menu_item_list, $children );
                    }
                }

            endif;

        endforeach;

        return $nav_menu_item_list;
    }


    /**
    * Format a menu item for JSON API consumption.
    *
    * @since   1.1.0
    * @param   object|array    $menu_item  the menu item
    * @param   bool            $children   get menu item children (default false)
    * @param   array           $menu       the menu the item belongs to (used when $children is set to true)
    * @return  array   a formatted menu item for JSON
    */
    public function format_menu_item( $menu_item, $children = false, $menu = array() ) {

        $item = (array) $menu_item;

        $menu_item = array(
            'ID'          => abs( $item['ID'] ),
            'order'       => (int) $item['menu_order'],
            'parent'      => abs( $item['menu_item_parent'] ),
            'title'       => $item['title'],
            'url'         => $item['url'],
            'attr'        => $item['attr_title'],
            'target'      => $item['target'],
            'classes'     => implode( ' ', $item['classes'] ),
            'xfn'         => $item['xfn'],
            'description' => $item['description'],
            'object_id'   => abs( $item['object_id'] ),
            'object'      => $item['object'],
            'object_slug' => get_post($item['object_id'])->post_name,
            'type'        => $item['type'],
            'type_label'  => $item['type_label'],
        );

        if ( $children === true && ! empty( $menu ) ) {

            $menu_item['children'] = $this->get_nav_menu_item_children( $item['ID'], $menu );
        }

        return $menu_item;
    }

    public function hhwtnav_rest_menus_init() { 

        register_rest_route( self::hhwtnav_api_namespace(), '/menus', array(
            'methods' => 'GET',
            'callback' => array( $this, 'hhwtnav_get_menu' )
        ));
    }

} //End of class
