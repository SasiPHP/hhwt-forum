<?php

/**
Halal wordpress admin functionalities
*/

Class HHWTFrontend{

    public $article_category;

    public function __construct() {
    	// ajax handler
        add_action('wp_ajax_hhwtuser_ajax_method', array($this, 'hhwtuser_ajax_handler'));
        add_action('wp_ajax_nopriv_hhwtuser_ajax_method', array($this, 'hhwtuser_ajax_handler'));

        //enqueqe theme scripts and styles
        add_action('wp_enqueue_scripts', array($this, 'hhwtfe_theme_scripts'));

        //add image sizes
        add_action('after_setup_theme', array($this, 'hhwtfe_add_theme_images'));

        //deals shortcode
        add_shortcode('CITYWIDGET', array($this, 'hhwtfe_city_shortcode_content'));

        //offers shortcode
        add_shortcode('OFFERWIDGET', array($this, 'hhwtfe_offer_shortcode_content'));

        //set content type
        add_filter('wp_mail_content_type', array($this, 'hhwt_report_mail_content_type' ) );

        //floater widget
        add_shortcode('modalwidget', array($this,'hhwtfe_modal_shortcode_content'));

        //create bucket list api
        add_action( 'wp_head', array( $this, 'hhwtfe_custom_scripts') );
        add_action( 'wp_head', array( $this, 'hhwt_category_meta_tags' ), 2 );

        //include custom post type category
        add_action( 'pre_get_posts', array($this, 'hhwtfe_include_cpt_category') );

        //register custom widget
        add_action( 'widgets_init', array($this, 'hhwtfe_register_custom_widget') );
	}

	public function hhwtuser_ajax_handler() {
		$gotomethod = trim($_POST['gotomethod']); 
		if (!empty($gotomethod) && method_exists($this, $gotomethod)) {
			$rtnval = call_user_func(array($this,$gotomethod),$_POST); 
			die($rtnval);
		} else {
			die('no-method found');
		}
	}

	public function hhwt_author_avatar( $author_id, $size) {

		$abh_avatar = '';
		$abhoption = json_decode(get_option('abh_options'),true);

		if ($abhoption && isset($abhoption['abh_author'.$author_id])) {
			$abh_avatar.= $abhoption['abh_author'.$author_id]['abh_gravatar'];
		}

		$cupp_upload = get_user_meta($author_id, 'cupp_upload_meta', true);


		return $abh_avatar;
	}

	public function hhwtfe_theme_scripts() {

		echo '<script>var hhwt_ajaxurl = "'.admin_url("admin-ajax.php").'";</script>';
		
		wp_enqueue_style("bootstrap-css", TEMPLATEURL ."/assets/css/bootstrap.min.css", false);
		wp_enqueue_style("fontawesome-css", TEMPLATEURL ."/assets/css/font-awesome.min.css", false);
		wp_enqueue_style("hhwt-css", TEMPLATEURL ."/assets/css/hhwt.css", false);
		wp_enqueue_style('style', get_stylesheet_uri() );

		/*wp_enqueue_script("bootstrap-js", TEMPLATEURL ."/assets/js/bootstrap.min.js", array('jquery'), null, true);*/
		wp_enqueue_script("jq-validate-js", TEMPLATEURL ."/assets/js/jquery.validate.min.js", array('jquery'), null, true);
		wp_enqueue_script("jquery-cookie", TEMPLATEURL ."/assets/js/jquery.cookie.js", array('jquery'), null, true);
		wp_enqueue_script("plugin-js", TEMPLATEURL ."/assets/js/plugins.js", array('jquery'), null, true);
		wp_enqueue_script("hhwt-js", TEMPLATEURL ."/assets/js/hhwt.js", array('jquery'), null, true);
	}

	public function hhwtfe_print_category($id)
	{
		$cat_link = '';

		if ($id) {
			$category_link = get_category_link( $id );
			$category_name = get_cat_name( $id );

			if(strtolower($category_name) == 'travel guide')
				$category_name = "Destinations";

			$cat_link.= sprintf("<a href='%s' title='%s'>%s</a>", esc_url( $category_link ), $category_name, $category_name);
		}

		return $cat_link;
	}

	public function hhwtfe_get_category_name($id)
	{
		$category_name = '';
		if ($id) {			
			$category_name = get_cat_name( $id );
		}
		return $category_name;
	}

	public function hhwtfe_carousel_navigation($carousel_id)
	{
		if(empty($carousel_id))
			return;

		ob_start();
		?>
		<a class="left carousel-control" href="#<?php echo $carousel_id; ?>" role="button" data-slide="prev">
			<span class="fa fa-angle-left" aria-hidden="true"></span>
		</a>
		<a class="right carousel-control" href="#<?php echo $carousel_id; ?>" role="button" data-slide="next">
			<span class="fa fa-angle-right" aria-hidden="true"></span>
		</a>
		<?php
		return ob_get_clean();
	}

	public function hhwtfe_carousel_title($content=array(), $category=false)
	{
		$title = (isset($content['title'])) ? $content['title'] : '';
		$btn_title = (isset($content['btn-title'])) ? $content['btn-title'] : '';
		$btn_link = (isset($content['btn-link'])) ? $content['btn-link'] : '';

		if ($category) {
			$id = $title;
			$title = get_cat_name( $id );
			$btn_link = get_category_link( $id );

			if(strtolower($title) == 'travel guide')
				$title = "Destinations";
		}
		ob_start();
		?>
		<div class="container content-title">
			<h4 class="col-md-10 col-sm-9 col-xs-6"><?php echo $title; ?></h4>
			<a href="<?php echo $btn_link; ?>" class="btn col-sm-2 btn btn-more"><?php echo $btn_title; ?><span class="fa fa-angle-right"></span></a>
		</div>
		<?php
		return ob_get_clean();
	}

	public function hhwtfe_get_grid_posts($post_array, $post_count)
	{
		if(count($post_array)<=0)
			return;

		$return_array = array_chunk($post_array, $post_count);
		return $return_array;
	}

	public function hhwtfe_set_article_category($cat_id)
	{
		if(!empty($cat_id))
			$this->article_category = $cat_id;
	}

	public function hhwtfe_get_article_category($html=true)
	{
		if(!$html)
			return $this->article_category;

		return $this->hhwtfe_print_category($this->article_category);
	}

	public function hhwtfe_slider_posts($categoryid='', $post_count=6)
	{
		$recent_posts = array();
		$args = array(
					'numberposts' => $post_count,
					'orderby' => 'post_date',
					'order' => 'DESC',
					'post_type' => 'post',
					'post_status' => 'publish',
					'suppress_filters' => true,
					'meta_query' => array(
							array(
								'key' => META_FEATURED,
								'value' => '1',
							)
						)
					);

		if ($categoryid) {
			$category = get_category($categoryid);
			$ParentPostCount = $category->category_count;
			if($ParentPostCount==0){
				$children = get_categories(array('child_of' =>$categoryid,'hide_empty' => 0));
				if (count($children) > 1){
					$args['category'] = $children;
				}
			}else{
				$args['category'] = $categoryid;
			}
		} elseif(is_category()) {
			$args['tax_query'] = array(
				array(
					'taxonomy' => 'category',
					'field'    => 'term_id',
					'terms'    => $categoryid,
				),
			);
		} 

		$recentpostsobj = get_posts( $args, OBJECT );

		if ($recentpostsobj) {
			foreach ($recentpostsobj as $single_post) {
				$temp_array = array();
				$temp_array['id'] = $single_post->ID;
				$temp_array['title'] = $single_post->post_title;
				$recent_posts[] = $temp_array;
			}
		}

		return $recent_posts;
	}

	public function hhwtfe_add_theme_images()
	{
		add_theme_support('post-thumbnails');
		
		add_image_size( 'simple-small', 270, 263, array( 'center', 'center' ) );
		add_image_size( 'simple-medium', 252, 252, array( 'center', 'center' ) );
		
		add_image_size( 'modern-small', 270, 412, array( 'center', 'center' ) );
		add_image_size( 'modern-big', 570, 385, array( 'center', 'center' ) );
		add_image_size( 'modern-medium', 450, 337, array( 'center', 'center' ) );

		add_image_size( 'profile-thumb', 39, 39, array( 'center', 'center' ) );
		add_image_size( 'left-profile-thumb', 94, 94, array( 'center', 'center' ) );
		add_image_size( 'notification-thumb', 76, 76, array( 'center', 'center' ) );

		add_theme_support( 'html5', array( 'gallery', 'caption' ) );
		// Post formats
		add_theme_support( 'post-formats', array( 'gallery', 'video', 'audio' ) );
		// Feed Links
		add_theme_support( 'automatic-feed-links' );

		register_nav_menu('header_menu', 'Header Menu');

		register_nav_menu('login_menu', 'Login Menu');
		
		register_nav_menu('marketplace_host', 'Marketplace Host');
		register_nav_menu('marketplace_traveler', 'Marketplace Traveler');

		register_nav_menu('footer_grid_1', 'Footer Grid 1');
		register_nav_menu('footer_grid_2', 'Footer Grid 2');
		register_nav_menu('footer_grid_3', 'Footer Grid 3');
	}

	public function hhwtfe_print_ads($pos='top')
	{
		global $custom_settings;

		if(isset($custom_settings['gads-'.$pos]))
			return $custom_settings['gads-'.$pos];
		else
			return '';
	}

	public function hhwtfe_print_block_title($title='')
	{
		if(empty($title))
			return;

		ob_start();
		?>
		<div class="container block-title">
			<h4 class="col-sm-10 col-xs-6"><?php echo $title; ?></h4>
		</div>
		<?php
		return ob_get_clean();
	}

	public function hhwtfe_get_domain($url)
	{
		$pieces = parse_url($url);
		$domain = isset($pieces['host']) ? $pieces['host'] : '';

		if($domain=="res.cloudinary.com"){
			$returnurl = str_replace("/upload/", "/upload/w_57,h_57,c_fill/", $url); 
		}
		else{
			$returnurl = $url;
		}
		return $returnurl;
	}

	public function hhwtfe_cat_has_parent($catid)
	{
		$category = get_category($catid);

		if ($category->category_parent > 0) {
			return $category->category_parent;
		}
		return false;
	}

	public function hhwtfe_print_category_featured_image($cat_id, $thumb=false)
	{
		if (empty($cat_id))
			return MODERN_MEDIUM_THUMB;

		$category_image = $this->hhwtfe_get_category_image( $cat_id, $thumb );

		if( !$category_image ) {
			$category_image =  ($thumb) ? MODERN_SMALL_THUMB : MODERN_MEDIUM_THUMB;
                }

		return $category_image;
	}

	public function hhwtfe_slice_post_categories($post_id, $length)
	{
		if (empty($post_id))
			return;

		$categories = wp_get_post_categories($post_id);

		if ($categories) {
			if($length) 
				$categories = array_slice($categories, $length-1);
		}

		return $categories;
	}

	public function hhwtfe_print_post_categories($post_id, $length = 0)
	{
		$cat_text = "";
		$post_cats = $this->hhwtfe_slice_post_categories($post_id, $length);

		if($post_cats) {
			foreach ($post_cats as $key => $cat_id) {
				$cat_text.= $this->hhwtfe_print_category($cat_id);
			}
		}

		return $cat_text;
	}

	public function hhwtfe_get_category_newsletter_image($term_id, $thumb='')
	{
		$image_arr = get_term_meta( $term_id, META_CAT_IMAGE, true );

		if($image_arr) {
			return $image_arr[$thumb];
		}
	}

	public function hhwtfe_get_category_image($term_id, $thumb='')
	{
		$image_arr = get_term_meta( $term_id, META_CAT_IMAGE, true );

		if($image_arr) {
			if($thumb)
				return $image_arr['cat-image-thumb'];

			return $image_arr['cat-image'];
		}
	}

	public function hhwtfe_current_link()
	{
		return $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	}

	public function hhwtfe_add_scheme($site_link='')
    {
        if  ( $ret = parse_url($site_link) ) {
            if ( !isset($ret["scheme"]) ) {
                $site_link = "https://{$site_link}";
            }
        }
        return $site_link;
    }

    public function hhwtfe_modal_shortcode_content()
    {
        $custom_settings  = get_option(HHWT_CUSTOM_STS);

        $floater_btn_text = $custom_settings['floater-btn'];

        $url              = $custom_settings['url'];
        $url              = $this->hhwtfe_add_scheme($url);

        $floater_desc     = $custom_settings['floater-desc'];

        $floater_logo     = $custom_settings['floater-logo'];
        $floater_logo     = $this->hhwtfe_add_scheme($floater_logo);

        $bg_image_path = TEMPLATEURL.'/images/floater-bg.png';
        
        $content.='<style>
                        #hhwt-modal-widget .hhwt-modal-widget {
                            background-image: url('.$bg_image_path.');
                        }
                    </style>
                    <div id="hhwt-modal-widget" class="modal">
                        <div class="modal-content hhwt-modal-widget">
                            <div data-dismiss="modal">
                                <span class="modal-close">Close</span>
                                <span class="modal-close-mbl">X</span>
                            </div>
                            <div class="hhwt-modal-image modal-body">
                                <img src="'.$floater_logo.'">
                            </div>
                            <div class="hhwt-modal-detail">
                                <p>'.$floater_desc.'</p>
                                <a target="_blank" href="'.$url.'" class="btn">'.$floater_btn_text.'</a>
                            </div>
                        </div>
                    </div>';
        return $content;
    }

    public function hhwt_report_mail_content_type()
    {
        return 'text/html';
    }

    public function hhwt_create_report_details() {
        global $wpdb, $hhwt_admin;

        parse_str($_POST["report_data"],$post_data);

        $username = (isset($post_data['username'])) ? $post_data['username'] : '';
        $useremail= (isset($post_data['useremail'])) ? $post_data['useremail'] : '';
        $jobtitle = (isset($post_data['jobtitle'])) ? $post_data['jobtitle'] : '';
        $usercompany= (isset($post_data['usercompany'])) ? $post_data['usercompany'] : '';

        $pdf_file_url = content_url('/uploads/docs/FINAL-REPORT.pdf');

        $subject = "HHWT PRESENTS: THE MODERN MUSLIM TRAVELLER";
        $from = get_option('admin_email');
        $message = "<p>Hi <b>{$username}</b></p><p>Thanks for downloading HHWT Presents the Modern Muslim Traveller. Hope you've found it helpful!</p><p>If you want to keep it as a resource, you can access it <a href='{$pdf_file_url}' download>here</a> anytime.</p><p>For further enquiries and collaborations, you can reach us at founders@havehalalwilltravel.com.</p><br><p>Regards,</p><p><b>Mikhail</b></p>";
        $headers = 'From: HHWT Admin <founders@havehalalwilltravel.com>' . "\r\n";

        $tblname = $hhwt_admin->hhwt_report_table();
        $tbldata = array(
                        'user_name'     => $username,
                        'job_title'     => $jobtitle,
                        'user_email'    => $useremail,
                        'user_company'  => $usercompany,
                        'created_date'  => current_time('mysql'),
                        'modified_date' => current_time('mysql')
                    );

        $insert_status = $wpdb->insert( $tblname , $tbldata );

        if ($insert_status) {
            $attachments = array( WP_CONTENT_DIR . '/uploads/docs/FINAL-REPORT.pdf' );
            $mail_status = wp_mail($useremail, $subject, $message, $headers, $attachments);

            if (!class_exists('Mailchimp')) {
                require_once("Mailchimp.php");
                $Mailchimp = new MailChimp(HHWT_MAILCHIMP_KEY);
            }

            try {
                $mailchimpdata = array(
                                    'id'                => HHWT_MAILCHIMP_LIST,
                                    'email'             => array('email' => $useremail),
                                    'merge_vars'        => array(
                                                            'EMAIL'     => (!empty($useremail)) ? $useremail : 'Unidentified',
                                                            'FNAME'     => (!empty($username)) ? $username : 'Unidentified',
                                                            'POSITION'  => (!empty($jobtitle)) ? $jobtitle : 'Unidentified',
                                                            'COMPANY'   => (!empty($usercompany)) ? $usercompany : 'Unidentified',
                                                        ),
                                    'double_optin'      => false,
                                    'update_existing'   => true,
                                    'replace_interests' => false,
                                    'send_welcome'      => false
                                );

                $result = $Mailchimp->call('lists/subscribe', $mailchimpdata);
            } catch (Exception $e) {
                die($e->getMessage());
            }

            if( !empty($result['euid']) && !empty($result['leid']) )
                die('success');
            else
                die('failure');

        } else {
            die('failure');
        }
    }

	public function hhwtfe_custom_scripts()
	{
		global $custom_settings, $hhwt_api;

		$post_permalink = '';

		$img_src = TEMPLATEURL . '/images/loading.gif';
		$loading_img = "<img src='{$img_src}' class='modal-loading'>";
		$user_id = $hhwt_api->hhwtapi_userid();

		$ad_target_url = 'https://www.havehalalwilltravel.com/blog/top-8-experiences-in-korea-under-sgd50-you-cant-leave-without-having/';
		$md5_target_url = md5($ad_target_url);

		/*bigtapp variables*/
		$previous_url = (isset($_SERVER["HTTP_REFERER"])) ? $_SERVER["HTTP_REFERER"] : '';
		$current_url = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

		/* User Device */
		$detect = new Mobile_Detect;
		$device ="Desktop";

		if ($detect->isMobile()) {
			$device ="Mobile";
		} else if($detect->isTablet()) {
			$device ="Tablet";
		}

		$datalayer = 'hhwtPage';

		if(is_front_page())
			$datalayer = 'HomePage';

		if(is_category())
			$datalayer = 'CategoryPage';

		if(is_author())
			$datalayer = 'AuthorPage';

		if(is_search())
			$datalayer = 'SearchPage';

		if(is_tag())
			$datalayer = 'TagPage';

		$category_name = array();
		if (is_single()) {
			$datalayer = 'ArticlePage';

			$post_id = get_the_ID();
			$post_permalink = get_the_permalink();

			if ($post_id) {

				$category_list = get_the_category($post_id);
				if ($category_list) {
					foreach($category_list as $category) {
						if(!in_array($category->name, $category_name)) {
							$term_name = strtolower($category->name);
							$term_name = str_replace(" ", "", $term_name);
							$category_name[] = $term_name;
						}
					}
				}
			}
		}
		?>
		<script type="text/javascript">
			/* google analytic scripts */
			googletag.cmd.push(function() {
				var mapping_article = googletag.sizeMapping().
				addSize([992, 0], [[728, 90]]).
				addSize([768, 0], [[728, 90]]).
				addSize([320, 0], [[320, 50]]).
				build();

				googletag.defineSlot('/92492279/article_leader', [[728, 90]], 'div-gpt-ad-1504189767270-0').
				defineSizeMapping(mapping_article).
				setCollapseEmptyDiv(true).
				addService(googletag.pubads());
				<?php if($post_permalink == $ad_target_url) { ?>
					googletag.pubads().setTargeting("TargetURL", "<?php echo $md5_target_url; ?>");
				<?php } elseif(is_array($category_name) && count($category_name)>0) { ?>
					googletag.pubads().setTargeting("destination",<?php echo json_encode($category_name); ?>);
				<?php } ?>
				googletag.enableServices();
			});

			/* global variables */
			var loading_img = "<?php echo $loading_img; ?>";
			var fb_id = "<?php echo $custom_settings['fb-id'] ?>";
			var pageCategory = "<?php echo $datalayer; ?>";
			var apiUserId = "<?php echo $user_id; ?>";

			var previous_url = "<?php echo $previous_url; ?>";
			var current_url = "<?php echo $current_url; ?>";
			var user_device = "<?php echo $device; ?>";

		</script>
		<?php
	}

	public function hhwtfe_bucket_class($post_id)
	{
		global $hhwt_session;
		$bucket_article = $hhwt_session->set_bucket_article();

        $is_in_bucket = false;
        $bucket_data = array();

		if ($bucket_article) {

			$user_buckets = get_user_meta(get_current_user_id(), META_USER_BUCKET, true);
			if(in_array($post_id, $user_buckets))
				$is_in_bucket = true;
		}

		$bucket_data['class'] = ($is_in_bucket) ? 'fa fa-heart' : 'fa fa-heart-o';
		$bucket_data['text'] = ($is_in_bucket) ? 'Saved' : 'Save';
		$bucket_data['in_bucket'] = ($is_in_bucket) ? 'true' : 'false';

		return $bucket_data;
	}

	public function hhwt_category_meta_tags() {

		global $post,$hhwt_frontend;

		if ( is_category() ) {
			$catid = get_queried_object_id();
			$MetaImage = $hhwt_frontend->hhwtfe_print_category_featured_image($catid);
			$MetaTitle =  get_cat_name($catid);
			$MetaUrl = get_category_link( $catid);
			$keywords = get_the_category( $post->ID );
			$metakeywords = '';

			foreach ( $keywords as $keyword ) {
			    $metakeywords .= $keyword->cat_name . ", ";
			}

			echo '<meta name="keywords" content="' . $metakeywords . '" />' . "\n";
			echo '<meta property="og:title" content="' . $MetaTitle . '" />' . "\n";
			echo '<meta property="og:type" content="article" />' . "\n";
			echo '<meta property="og:url" content="' . $MetaUrl . '" />' . "\n";
			echo '<meta property="og:image" content="' . $MetaImage . '" />' . "\n";
			echo '<meta property="og:site_name" content="Travel Guides For Muslim Travellers | Have Halal, Will Travel" />' . "\n";
			echo '<meta property="fb:app_id" content="1469517816711711" />'. "\n";
			echo '<meta property="og:description" content="' . $MetaDesc . '" />' . "\n";
			echo '<meta name="twitter:card" content="summary" />'. "\n";
			echo '<meta name="twitter:title" content="' . $MetaTitle . '" />' . "\n";
			echo '<meta name="twitter:description" content="' . $MetaDesc . '" />' . "\n";
			echo '<meta name="twitter:image" content="' . $MetaImage . '" />' . "\n";
		}
	}

	public function hhwtfe_post_pagination_links($total_pages)
    {

        $big = 999999999; // need an unlikely integer
        $pages = paginate_links( array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '?paged=%#%',
            'current' => max( 1, $paged ),
            'total' => $total_pages,
            'prev_next' => false,
            'end_size'  => 0,
            'mid_size'  => 0,
            'type'  => 'array',
            'prev_next'   => TRUE,
            'prev_text'=>__('« Newer Posts'),
            'next_text'=>__('Older Posts »'),
        ) );
    ?>
        <nav class="navigation hhwt-post-pagination clearfix">
            <div class="nav-links">
            <?php
                if( is_array( $pages ) ) {
	                    foreach ( $pages as $page ) {
	                        if (strpos($page,'next') !== false){
	                          $page = str_replace("next", "", $page);
	                            echo "$page";
	                    }
                    }
                    ?>
					<div class="hhwt-page-load-status">
						<div class="infinite-scroll-request">
							<img class="page-load-more" src="<?php echo TEMPLATEURL . '/images/loading.gif'; ?>">
						</div>
						<p class="infinite-scroll-last"><em>No more articles to load</em></p>
					</div>
			<?php
                }
            ?>
            </div>
        </nav>
	<?php
    }

	public function hhwtfe_gtm_id()
	{
		global $custom_settings;
		$gtm_id = (isset($custom_settings['gtm-id'])) ? $custom_settings['gtm-id'] : 'GTM-PK4N7MT';
		
		return $gtm_id;
	}

	public function hhwtfe_mailchimp_get_groups($members_lists)
	{
		$user_groups = array();

		if ($members_lists['success_count']==1) {
	        $user_data = $members_lists['data'][0];
	        $groupings = $user_data['merges']['GROUPINGS'][0];
	        $group = $groupings['groups'];

	        if (count($group)>0) {
	            $user_groups = array_map(function ($value) {
	                return  ($value['interested']==1) ? $value['name'] : '';
	            }, $group);
	        }
	        $user_groups = array_filter($user_groups);
	    }

	    return $user_groups;
	}

	// list id 36fcd8ea96
	// group id 3135
	public function hhwtfe_create_newsletter_subscribe() 
	{
		global $hhwt_api;
        
        if(!isset($_POST["user_data"]))
        	die('failure');

        $decrpted = $hhwt_api->hhwtapi_decrypt_data($_POST['user_data']);

        parse_str($decrpted,$post_data);

        $useremail= (isset($post_data['useremail'])) ? $post_data['useremail'] : 'Unidentified';
        $nl_country = (isset($post_data['guide-country'])) ? $post_data['guide-country'] : '';

        if (!class_exists('Mailchimp')) {
            require_once("Mailchimp.php");
            $Mailchimp = new MailChimp(HHWT_MAILCHIMP_KEY);
        }

        switch ($nl_country) {
			case 'japan':
				$pdf_name = 'Tokyo.pdf';
				$mailchimp_group = 'klook tokyo';
				$city = 'Tokyo';
				$pdf_file_url = 'https://drive.google.com/file/d/1VWf_2dz0bCjq7m95i6naU-OUPAQL5eW6/view?usp=sharing';
				$klook_link = 'http://bit.ly/HHWTKlookTokyoEmail';
				break;

			case 'singapore':
				$pdf_name = 'Singapore.pdf';
				$mailchimp_group = 'klook singapore';
				$city = 'Singapore';
				$pdf_file_url =  'https://drive.google.com/file/d/1kcq6blqzX9N5-jXuas-awTrKZO2OzVaV/view?usp=sharing';
				$klook_link = 'http://bit.ly/HHWTKlookSGPrintable';
				break;

			case 'hong kong':
				$pdf_name = 'Hong-Kong.pdf';
				$mailchimp_group = 'klook hong kong';
				$city = 'Hong Kong';
				$pdf_file_url =  'https://drive.google.com/a/havehalalwilltravel.com/file/d/1U56tFQFAoXufQKgLr1fQ2srULv_O6Wq8/view?usp=sharing';
				$klook_link = 'http://bit.ly/2yDzXoq';
				break;

			default:
				$pdf_name = 'Tokyo.pdf';
				$mailchimp_group = 'klook tokyo';
				$city = 'Tokyo';
				$pdf_file_url = 'https://drive.google.com/file/d/1VWf_2dz0bCjq7m95i6naU-OUPAQL5eW6/view?usp=sharing';
				$klook_link = 'http://bit.ly/HHWTKlookTokyoEmail';
				break;
        }
        //check user already in group
        $list_id = '05e89ee5ee';
        $mailchimp_groups = $Mailchimp->lists->memberInfo($list_id,array(array('email'=>$useremail)));

        $user_groups = $this->hhwtfe_mailchimp_get_groups($mailchimp_groups);
        $user_groups[] = $mailchimp_group;

        //$pdf_file_url = content_url('/uploads/docs/'.$pdf_name);
        $subject = "Here’s your FREE copy of HHWT’s {$city} Travel Guide!";
        $from = 'founders@havehalalwilltravel.com';
        $message = "<p>Hello!</p><p>Thanks for subscribing to our newsletter! DOWNLOAD your travel guide for {$city} <a href='{$pdf_file_url}' download>here</a>.</p><p>Don’t forget to use our exclusive code - <b>TRAVELHHWT</b> to get $5 off your first purchase on <a href='{$klook_link}'>Klook!</a></p><br><p>Cheers,</p><p>HHWT Team</p>";
        $headers = 'From: HHWT Team <founders@havehalalwilltravel.com>' . "\r\n";

        try {
            $mailchimpdata = array(
                                'id'                => $list_id,
                                'email'             => array('email' => $useremail),
                                'merge_vars'        => array('EMAIL' => $useremail),
                                'double_optin'      => false,
                                'update_existing'   => true,
                                'replace_interests' => false,
                                'send_welcome'      => false
                            );

            $result = $Mailchimp->call('lists/subscribe', $mailchimpdata);

            if (!empty($result['euid']) && !empty($result['leid'])) {
            	$merge_vars = 	array(
						        	'GROUPINGS'=> 	array(
										            	array(
										                	'id' => 1,
										                	'groups' => $user_groups
										            	)
							        				)
			    				);
			    $set_group = $Mailchimp->lists->updateMember($list_id,array('email'=> $useremail), $merge_vars);
            }

            if ($set_group['euid'] && $set_group['leid']) {
            	//$attachments = array( WP_CONTENT_DIR . '/uploads/docs/'.$pdf_name );
            	$mail_status = wp_mail($useremail, $subject, $message, $headers);

        		die('success');	
            }

        } catch (Exception $e) {
            die($e->getMessage());
        }

        die('failure');
    }

    public function hhwtfe_bigtapp_cookie()
	{
		$stamp = date("Ymdhis");
		$ip = $_SERVER['REMOTE_ADDR'];
		$people_id = "".$stamp.$ip;
		$people_id =md5($people_id);
		die($people_id);
	}

	public function hhwtfe_api_bigtapp_details()
	{
		global $hhwt_session;
		$user_session = $hhwt_session->get_user_session();

		if(!$user_session)
			return false;

		$user_email = isset($user_session['user_email']) ? $user_session['user_email']: '';
		$user_name = isset($user_session['user_name']) ? $user_session['user_name'] : '';
		$user_id = isset($user_session['user_id']) ? $user_session['user_id']: '';

		$user_data = array(
						'user_email' => $user_email,
						'user_name' => $user_name,
						'user_id' => $user_id
					);

		return $user_data;
	}

	public function hhwtfe_track_bigtapp()
	{
		global $hhwt_api;

		$bigtapp_data = $_POST['bigtappdata'];
		$user_data = $this->hhwtfe_api_bigtapp_details();

		if ($user_data) {
			$bigtapp_data = array_merge($bigtapp_data, $user_data);
		}

		$bigtap_response = $hhwt_api->hhwtapi_get_response(BIGTAPP_END, $bigtapp_data);
		die ($bigtap_response);
	}

	public function hhwtfe_include_cpt_category( $query ) {
	    if ( $query->is_category() && $query->is_main_query() ) {
	        $query->set( 'post_type', array('post', 'my') );
	    }
	}

	public function hhwtfe_register_custom_widget() {
	    register_widget( 'HHWTSocialWidget' );
	    register_widget( 'HHWTFacebookWidget' );
	}


} //End of Class