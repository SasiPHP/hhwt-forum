<?php
if (WP_DEBUG && WP_DEBUG_DISPLAY)
{
   ini_set('error_reporting', E_ALL & ~E_STRICT & ~E_NOTICE);
}
if(is_admin()){
	/** Absolute path to the WordPress directory. */
	if ( !defined('ABSPATH') )
	    define('ABSPATH', dirname(__FILE__) . '/');

	define('CONCATENATE_SCRIPTS', false);
}
define("ET_UPDATE_PATH",    "//www.enginethemes.com/forums/?do=product-update");
define("ET_VERSION", '1.6.10');

if(!defined('ET_URL'))
	define('ET_URL', '//www.enginethemes.com/');

if(!defined('ET_CONTENT_DIR'))
	define('ET_CONTENT_DIR', WP_CONTENT_DIR.'/et-content/');

define ( 'TEMPLATEURL', get_bloginfo('template_url') );
define('THEME_NAME', 'forumengine');

define('THEME_CONTENT_DIR', WP_CONTENT_DIR . '/et-content' . '/' . THEME_NAME );
define('THEME_CONTENT_URL', content_url() . '/et-content' . '/' . THEME_NAME );

if(!defined('ET_LANGUAGE_PATH') )
	define('ET_LANGUAGE_PATH', THEME_CONTENT_DIR . '/lang');

if(!defined('ET_CSS_PATH') )
	define('ET_CSS_PATH', THEME_CONTENT_DIR . '/css');

require_once TEMPLATEPATH . '/includes/index.php';
//google captcha class
require_once TEMPLATEPATH . '/includes/google-captcha.php';

try {
	if ( is_admin() ){
		new ET_ForumAdmin();
	} else {
		new ET_ForumFront();
	}

} catch (Exception $e) {
	echo $e->getMessage();
}

add_theme_support( 'automatic-feed-links');
add_theme_support('post-thumbnails');

function et_prevent_user_access_wp_admin ()  {
	if(!current_user_can('manage_options')) {
		wp_redirect(home_url());
		exit;
	}
}

/// for test purpose
add_action( 'init', 'test_oauth' );
function test_oauth(){
	if ( isset($_GET['test']) && $_GET['test'] == 'twitter' ){
		require dirname(__FILE__) . '/auth.php';
		exit;
	}
}
function je_comment_template($comment, $args, $depth){
	$GLOBALS['comment'] = $comment;
?>
	<li class="et-comment" id="comment-<?php echo $comment->comment_ID ?>">
		<div class="et-comment-left">
			<div class="et-comment-thumbnail">
				<?php echo et_get_avatar($comment->user_id); ?>
			</div>
		</div>
		<div class="et-comment-right">
			<div class="et-comment-header">
				<a href="<?php comment_author_url() ?>"><strong class="et-comment-author"><?php comment_author() ?></strong></a>
				<span class="et-comment-time icon" data-icon="t"><?php comment_date() ?></span>
			</div>
			<div class="et-comment-content">
				<?php echo esc_attr( get_comment_text($comment->comment_ID) ) ?>
				<p class="et-comment-reply"><?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?></p>
			</div>
		</div>
		<div class="clearfix"></div>
<?php
}
/**
 *Check to load mobile version
 *
 *@return true if load mobile version / false if don't load
 *@since version 1.6.1
 */
if(!function_exists('et_load_mobile')) {
	function et_load_mobile() {
		global $isMobile;
		$detector = new ET_MobileDetect();
		$isMobile = $detector->isMobile() && ( ! $detector->isAndroidtablet() ) && ( ! $detector->isIpad() );
		$isMobile = apply_filters( 'et_is_mobile', $isMobile ? TRUE : FALSE );
		if ( $isMobile && ( ! isset( $_COOKIE[ 'mobile' ] ) || md5( 'disable' ) != $_COOKIE[ 'mobile' ] ) ) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
/**
 * Remove desktop style and script when load mobile version
 */
add_action('wp_head', 'et_wp_head');
function et_wp_head() {
	if(et_load_mobile()){
		return;
	}
}
add_action('wp_footer', 'et_wp_footer');
function et_wp_footer() {
	if(et_load_mobile()){
		return;
	}
}

/**
 * Add closed status to search query.
 *
 * @param $query
 *
 * @return void
 *
 * @author nguyenvanduocit
 */
function add_status_to_search_query( $query ) {
	if ( $query->is_search() && $query->is_main_query() ) {
		$query->query_vars['post_status'] = apply_filters('fe_thread_status', array('publish', 'closed'));
	}
}
add_action( 'pre_get_posts', 'add_status_to_search_query' );


/* ======== Custom PHP Classes ======== */
global $hhwt_admin, $hhwt_frontend, $custom_settings, $hhwt_api, $hhwt_session, $newdb;

$newdb = new wpdb( 'HalalWpUser' , 'v6tYkcWG9746p3g' , 'HalalWordpressDb' , 'halaldb-options-recover.cp1u9dl2s50g.ap-southeast-1.rds.amazonaws.com' );

include_once 'class/HHWTAdmin.php';
include_once 'class/HHWTFrontend.php';
include_once 'class/HHWTSession.php';
include_once 'class/HHWTAPI.php';
include_once 'class/HHWTNavWalker.php';
include_once 'class/HHWTSocialWidget.php';
include_once 'class/HHWTFacebookWidget.php';
include_once 'class/HHWTTermMeta.php';
include_once 'class/HHWTNavMenu.php';
include_once 'class/Mobile_Detect.php';

$hhwt_admin = new HHWTAdmin();
$hhwt_frontend = new HHWTFrontend();
$hhwt_session = new HHWTSession();
$hhwt_api = new HHWTAPI();
$hhwt_nav = new HHWTNavMenu();

/* Common functions */
foreach ( array( 'pre_term_description' ) as $filter ) {
    remove_filter( $filter, 'wp_filter_kses' );
}
 
foreach ( array( 'term_description' ) as $filter ) {
    remove_filter( $filter, 'wp_kses_data' );
}

function hhwt_is_mobile($device='mobile')
{
    $tablet_browser = 0;
    $mobile_browser = 0;

    if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
        $tablet_browser++;
    }

    if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
        $mobile_browser++;
    }

    if (
        ( isset($_SERVER['HTTP_ACCEPT']) && (strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0)) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))
    ) {
        $mobile_browser++;
    }

    $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
    $mobile_agents = array(
      'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
      'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
      'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
      'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
      'newt','noki','palm','pana','pant','phil','play','port','prox',
      'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
      'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
      'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
      'wapr','webc','winw','winw','xda ','xda-');

    if (in_array($mobile_ua,$mobile_agents)) {
        $mobile_browser++;
    }

    if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
        $mobile_browser++;
        //Check for tablets on opera mini alternative headers
        $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
        if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
            $tablet_browser++;
        }
    }

    switch ($device) {

        case 'mobile':
        if ((isset($_SERVER['HTTP_CLOUDFRONT_IS_MOBILE_VIEWER']) && $_SERVER['HTTP_CLOUDFRONT_IS_MOBILE_VIEWER'] == 'true') || $mobile_browser > 0 )
          return true;
        else
         return false;
        break;

        case 'tab':
        if ((isset($_SERVER['HTTP_CLOUDFRONT_IS_TABLET_VIEWER']) && $_SERVER['HTTP_CLOUDFRONT_IS_TABLET_VIEWER'] == 'true') || $tablet_browser > 0 )
          return true;
        else
          return false;
        break;

        default:
            return false;
        break;
    }
}
/* ======== Custom PHP Classes ======== */
