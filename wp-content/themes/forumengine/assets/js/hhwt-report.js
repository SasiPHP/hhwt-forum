jQuery(function($) {
	"use strict";

	$(".rp-banner-download").click(function() {
		$('html,body').animate({scrollTop: $("#rp-download-form").offset().top},'slow');
	});

	$(function() {
		$("#hhwtformdownload").validate({
			rules: {
				username: "required",
				jobtitle: "required",
				useremail: {
					required: true,
					email: true
				},
				usercompany: "required"
			},
			messages: {
				username: "Please enter your name",
				jobtitle: "Please enter your Position",
				useremail: "Please enter a valid email address",
				usercompany: "Please enter your Company"
			},
			submitHandler: function(form) {
				var report_data 	= 	jQuery(form).serialize();
				jQuery.ajax({
					url:hhwt_ajaxurl,
					type: "POST",
					data: {
						"action": 'hhwtuser_ajax_method',
						"gotomethod":'hhwt_create_report_details',
						"report_data":report_data
					},
					beforeSend: function(){
					  	$('.hhwt-report-loading').fadeIn();
					},
					success: function(response) {
				 		if(response == "success")
				 			$('#rp-thanks-modal').modal('show');
				 		
				 		jQuery(form)[0].reset();
					},
					complete: function() {
						$('.hhwt-report-loading').fadeOut();
					}
				});
			}
		});
	});
});