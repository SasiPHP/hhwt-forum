(function($){

	function preview_image(imgurl, uploadID) {
		if (imgurl && $('.hhwt-preview-upload').length) {
			uploadID.prev('.hhwt-preview-upload').find('img').attr("src", imgurl).show();
		}
	}

	function set_send(uploadID) {
		window.send_to_editor = function(html) {
			if(typeof $(html).attr('src') != 'undefined' )
				imgurl = $(html).attr('src');
			else
				imgurl = $(html).attr('href');

			uploadID.val(imgurl);
			preview_image(imgurl, uploadID);
			tb_remove();
		};
	}


	$(document).ready(function() {
		var uploadID = '';

		$('.hhwt-btn-add-media').click(function() {
			uploadID = $(this).prev('input');
			tb_show('', 'media-upload.php?type=image&TB_iframe=true');
			set_send(uploadID);
			return false;
		});
	});

	$(document).on("click", ".hhwt-btn-remove-media", function(){
		$(this).siblings('.hhwt-preview-upload').find("img").hide();
		$(this).siblings('input[type="hidden"]').val("");	
	})

	$(document).on("click", "#is-newsletter-popup", function(){
		if($(this).is(':checked'))
		{
		  $(".newsletter-detail").show();
		  
		}else
		{
		 $(".newsletter-detail").hide();
		}
	});

	$(document).ready(function() {
		if($("#is-newsletter-popup").is(':checked'))
		{
		  $(".newsletter-detail").show();
		}else
		{
		 $(".newsletter-detail").hide();
		}
	});

}(jQuery));