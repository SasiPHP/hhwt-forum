var userIPData = [];
var currentuser;
var user_id;
function validate_value(input_value)
{
	if(input_value!='' && typeof input_value != 'undefined' && input_value!=0 && input_value != 'failure' )
		return true;
	else
		return false;
}

function share_on_fb( title, description)
{
	var url = window.location.href;
	window.open('http://www.facebook.com/sharer/sharer.php?s=100&p[title]='+ decodeURIComponent(title) + '&u=' + encodeURIComponent(url) + '&p[summary]=' + description , 'sharer', 'toolbar=0,status=0,width=600,height=360');
}

function share_on_twitter( title)
{
	var url = window.location.href;
	window.open('https://twitter.com/intent/tweet?text='+ decodeURIComponent(title) + ' ' + encodeURIComponent(url), '', 'width=600,height=360');
}

function hhwtRedirect(redirectlink)
{
	if (redirectlink) {
		window.location.href = redirectlink;
	}
}

function hhwt_validate_data(data)
{
	return btoa(data);
}

function hhwt_decrypt_data(data)
{
	return atob(data);
}

function hhwt_serialize(form)
{
	var form_data = jQuery(form).serialize();

	if(validate_value(form_data)) {
		return hhwt_validate_data(form_data);
	}
}

function hhwt_destory_session()
{
	jQuery.ajax({
	 	url:hhwt_ajaxurl,
	 	type:"POST",
	 	async: true,
	 	data:{
	 		'action': 'hhwtapi_ajax_method',
	 		'gotomethod': 'hhwtapi_destory_session',
	 	},
	 	success: function(is_destroyed) {
 			if (is_destroyed != 'failure') {
				currentuser = undefined;
				jQuery.removeCookie('_hhwt_ssid_', { domain: '.havehalalwilltravel.com', path: '/' });
				jQuery(".hhwt-user-menu").html(is_destroyed);
				jQuery('.save-bucket').
				attr('data-in-bucket', 'false').
				find('i').removeClass('fa-heart').addClass('fa-heart-o');
				hhwt_set_default_href();
				if (jQuery('.save-bucket').hasClass("hhwt-save-button")) {
					jQuery('.save-bucket.hhwt-save-button').html('<i class="fa fa fa-heart-o" aria-hidden="true"></i> Save');
				}
 			}
		},
	});
}

function trg_loggin()
{
	jQuery(".modal").modal('hide');
	jQuery("#hhwt-modal-signup").modal('show');
	setTimeout(function(){
		jQuery("body").addClass('modal-open');
	}, 500);
	return false;
}

function hhwt_bucket_content(post_id)
{
	var dfd = jQuery.Deferred();

	jQuery.ajax({
	 	url:hhwt_ajaxurl,
	 	type:"POST",
	 	data: {
	 		"action": 'hhwtapi_ajax_method',
	 		"gotomethod":'hhwtapi_bucket_content',
	 		"post_id": post_id
	 	},
	 	success: function(bucket_data) {
	 		if(bucket_data){
	 			dfd.resolve( bucket_data );
	 		}
		},
	});

	return dfd.promise();
}

function hhwt_article_content(post_id)
{
	var dfd = jQuery.Deferred();

	jQuery.ajax({
	 	url:hhwt_ajaxurl,
	 	type:"POST",
	 	data: {
	 		"action": 'hhwtapi_ajax_method',
	 		"gotomethod":'hhwtapi_article_content',
	 		'post_id':post_id,
	 	},
	 	success: function(article_data) {
	 		if(article_data){
	 			dfd.resolve( article_data );
	 		}
		},
	});

	return dfd.promise();
}

function hhwt_set_default_href()
{
	jQuery('a[href="#"]').each(function() {
		jQuery(this).attr('href', 'javascript:;');
	});
}

function hhwt_set_bigtapp_cookie()
{
	jQuery.ajax({
		type: "POST",
		url: hhwt_ajaxurl,
		async: false,
		data: {
			'action': 'hhwtuser_ajax_method',
            'gotomethod': 'hhwtfe_bigtapp_cookie'
		},		
		success: function (bigtapp_cookie) {
			if (bigtapp_cookie) {
				jQuery.cookie('people_id', bigtapp_cookie, { expires: 365 });
			}
		}
	});
}

function hhwt_success_login_action(response)
{
	var response = JSON.parse(response);
	currentuser = response.user;
	if (currentuser && !jQuery.cookie('_hhwt_ssid_')) {
		var user_cookie = {};
		user_cookie.email = currentuser.user_email;
		user_cookie.id = currentuser.user_id;
		jQuery("#logged_user_name").val(currentuser.user_name);
		jQuery("#logged_user_email").val(currentuser.user_email);
		jQuery("#logged_user_id").val(currentuser.user_id);
		var cookie_data = btoa(JSON.stringify(user_cookie));
		jQuery.cookie('_hhwt_ssid_', cookie_data, { expires: 1, domain: '.havehalalwilltravel.com', path: '/'});
	}

	/* set bucket article in local storage */
	if ( response.bucket_article && typeof response.bucket_article != 'undefined') {

		var bucket_articles = response.bucket_article;

		localStorage.setItem("bucketArticle", JSON.stringify(bucket_articles));

		jQuery.each(bucket_articles, function( index, article_id ) {
			jQuery('.save-bucket[data-post-id="'+article_id+'"]')
			.attr('data-in-bucket', true)
			.find('i').removeClass('fa-heart-o').addClass('fa-heart');
			if (jQuery('.save-bucket[data-post-id="'+article_id+'"]').hasClass("hhwt-save-button")) {
				jQuery('.save-bucket.hhwt-save-button[data-post-id="'+article_id+'"]').html('<i class="fa fa fa-heart" aria-hidden="true"></i> Saved');
			}
		});
	}

	/* append user menu */
	if( response.menu && typeof response.menu != 'undefined') {
		jQuery(".hhwt-user-menu").html(response.menu);
		hhwt_set_default_href();
	}

	/* check has cookie value */
	if (jQuery.cookie('trg-bucket-article')) {
		var trg_article = jQuery.cookie('trg-bucket-article');
		if (validate_value(trg_article)) {
			jQuery('.save-bucket[data-post-id="'+trg_article+'"]').trigger('click');
			jQuery.removeCookie('trg-bucket-article');
		}
	}
}

function hhwt_trigger_user_login()
{
	var cookie_user_id;
	if (jQuery.cookie('_hhwt_ssid_')) {
		var cookie_user = jQuery.cookie('_hhwt_ssid_');
		var current_user = atob(cookie_user);
		if (validate_value(current_user)) {
			current_user = JSON.parse(current_user);
			user_id = current_user.id;
			cookie_user_id = current_user.id;

		}
	} else {
		hhwt_destory_session();
	}

	if(cookie_user_id) {
		jQuery.ajax({
	        url: hhwt_ajaxurl,
	        type: "POST",
	        data: {
	            "action": 'hhwtapi_ajax_method',
	            "gotomethod": 'hhwtfe_trigger_user_login',
	            "user_id": cookie_user_id
	        },
	        success: function(response) {
	        	if (response != 'undefined' && response!='failure' && response!='session-exist') {
	            	hhwt_success_login_action(response);
	            }
	        }
	    });
	}
}

function hhwt_push_datalayer(ga_data)
{
	if(validate_value(ga_data)) {
		window.dataLayer.push(ga_data);
	}
}

function hhwt_addslashes( str ) {
    return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
}

function hhwt_capitalize(str)
{
	str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
	    return letter.toUpperCase();
	});

	return str;
}


function msToTime(ms) {
	var seconds = (ms/1000);
	var minutes = parseInt(seconds/60, 10);
	seconds = seconds%60;
	var hours = parseInt(minutes/60, 10);
	minutes = minutes%60;
	return hours + ':' + minutes + ':' + seconds;
}

function format_data_layer(eventLabel, status, eventType)
{
    var data_string = '';
    if (currentuser) {
       eventLabel.userid = user_id;
    }
    if (eventLabel != '') {
	    jQuery.each(eventLabel, function(index, value) {
	        if(validate_value(value))
	            data_string+= ' | ' + value ;
	        eventLabel = data_string.substring(3, data_string.length);
	    });
	} else {
		eventLabel = 'Guest User';
	}

	switch(eventType) {
		case 'login':
			dl_data = { 'event' : 'PublicationLogin','eventCategory' :  'Login'};
			break;
		case 'save article':
			dl_data = { 'event' : 'SaveArticleToBucket','eventCategory' :  'Add To Bucket'};
			break;
		case 'remove article':
			dl_data = { 'event' : 'RemoveFromBucket','eventCategory' :  'Removed from Bucket'};
			break;
		case 'newsletter':
			dl_data = { 'event' : 'NewsLetterSubscribe','eventCategory' :  'Newsletter Subscribe'};
			break;
		default:
			dl_data = '';
	}
	dl_data.eventAction = 'Publication | ' + status;
	dl_data.eventLabel  = eventLabel;
    return dl_data;
}

(function($){

	$(document).ready(function() {
		$(".hhwt-nav-menu-li .search input").focus(function(){
			$(".hhwt-nav-menu-li .search").addClass("search-bg");
		});

		$(".hhwt-nav-menu-li .search input").blur(function(){
			$(".hhwt-nav-menu-li .search").removeClass("search-bg");
		});

		$('.carousel').carousel({
		    interval: false
		});

		$("#subscribedmsg").hide();

        /* Registration form validation */
		$("#hhwt-form-registration").validate({
	        rules:{
	            'register[user_email]':{
	                required: true,
	                email: true
	            },
	            'register[user_password]':{
	                required:true,
	                minlength:6
	            },
	            'register[user_login]':{
	                required:true
	            },
	            'register[first_name]':{
	                required:true
	            },
	            'register[last_name]':{
	                required:true
	            },
	            'register[user_country]':{
	            	required:true
	            }
	        },
	        messages:{
	            'register[user_email]':{
	                required: "Enter email",
	                email: "Enter valid email address"
	            },
	            'register[user_password]':{
	                required:"Enter password",
	                minlength:"password minlength 6"
	            },
	            'register[user_login]':{
	                required:"Enter username"
	            },
	            'register[firs_tname]':{
	                required:"Enter firstname"
	            },
	            'register[last_name]':{
	                required:"Enter lastname"
	            },
	            'register[user_country]':{
	            	required:"Enter country"
	            }
	        },
	        submitHandler: function(form) {

		        var register_data = hhwt_serialize(form);

		        $.ajax({
		            url: hhwt_ajaxurl,
		            type: "POST",
		            data: {
		                'action': 'hhwtapi_ajax_method',
                        'gotomethod': 'hhwtapi_register_user',
                        'user_data': register_data
		            },
		            beforeSend: function(){
		            	$(form).find(".form-loading").addClass('show');
		            	$(form).find("[type='submit']").prop('disabled', true);
		            },
		            success: function(response) {
		            	eventLabel = {
		            		'type' : 'Email'
		            	};
		            	if (response == 'exist') {
		            		$("[name='register[user_email]']")
		            		.css({"border": "1px solid #f00"})
		            		$(".hhwt-signup-form .alert").addClass('alert-danger');
		            		$(".hhwt-signup-form .alert").html('<strong>Error!</strong> User already exist.');
		            		setTimeout(function(){
								$(".hhwt-signup-form #Step2").css("display" , "none");
								$(".hhwt-signup-form #Step1").css("display" , "block");
								$(".hhwt-signup-form .alert").removeClass('alert-danger');
							}, 1000);
		            	} else if (response != 'undefined' && response!='failure') {
		            		$(".hhwt-signup-form .alert").removeClass('alert-danger').addClass('alert-success');
		            		$(".hhwt-signup-form .alert").html('<strong>Success!</strong> Registered Successfully.');
		            		
		            		var user_response = JSON.parse(response);
							currentuser = user_response.user;
						    woopra.track("registerviaemail", {
						    	email: currentuser.user_email,
						    	user_id: currentuser.user_id,
						    	name: currentuser.user_name,
						    	user_type: "Wordpress"
							});
							// Identify customer
							woopra.identify({
							    email: currentuser.user_email,
							    name: currentuser.user_name,
							});							 
							// track
							woopra.track();

		            		hhwt_success_login_action(response);
		            		$(form)[0].reset();
		            		setTimeout(function(){
								$('#hhwt-modal-signup').modal('hide');
								$(".hhwt-signup-form .alert").removeClass('alert-success');
							}, 1000);
							user_status = 'Success';
		            	} else {
							$('.modal').modal('hide');
		            	}
		            },
		            error: function() {
		            	$('.modal').modal('hide');
		            	$('#hhwt-modal-error').modal('show');
		            	user_status = 'Failure';
		            },
		            complete: function(){
		            	$(form).find(".form-loading").removeClass('show');
		            	$(form).find("[type='submit']").prop('disabled', false);
		            	dl_data = format_data_layer(eventLabel, user_status, 'login');
						hhwt_push_datalayer(dl_data);
		            }
		        });
			    
		    }
	    });

	    /* Login form validation */
	    $("#hhwt-form-login").validate({
	        rules:{
	            user_email:{
	                required:true,
	                email:true
	            },
	            user_password:{
	                required:true
	            }
	        },
	        messages:{
	            user_email:{
	                required:"Email is required",
	                email: "Enter valid email address"
	            },
	            user_password:{
	                required:"Password is required"
	            }
	        },
	        submitHandler: function(form) {

		        var login_data = hhwt_serialize(form);

		        eventLabel = {
            		'type' : 'Email'
            	};

		        $.ajax({
		            url: hhwt_ajaxurl,
		            type: "POST",
		            data: {
		                'action': 'hhwtapi_ajax_method',
                        'gotomethod': 'hhwtapi_user_login',
                        'user_data': login_data
		            },
		            beforeSend: function(){
		            	$(form).find(".form-loading").addClass('show');
		            	// $(form).find("[type='submit']").prop('disabled', true);
		            },
		            success: function(response) {
		            	if (response == 'try-alternate' || response == 'failure') {
		            		$(".hhwt-form-login .alert").addClass('alert-danger');
		            		$(".hhwt-form-login .alert").html('<strong>Error!</strong> Invalid username/password.');
		            		user_status = 'Failure';
							setTimeout(function(){
								$(".hhwt-form-login .alert").removeClass('alert-danger');
							}, 1500);
		            	} else if (response != 'undefined' && response!='failure') {
		            		$(".hhwt-form-login .alert").html('<strong>Success!</strong> Logged in Successfully.');
		            		$(".hhwt-form-login .alert").removeClass('alert-danger').addClass('alert-success');
							setTimeout(function(){
								$('#hhwt-modal-signup').modal('hide');
								$(".hhwt-form-login .alert").removeClass('alert-success');
							}, 1000);

							var user_response = JSON.parse(response);
							currentuser = user_response.user;
						    woopra.track("loginviaemail", {
						    	email: currentuser.user_email,
						    	user_id: currentuser.user_id,
						    	name: currentuser.user_name,
						    	user_type: "Wordpress"
							});
							// Identify customer
							woopra.identify({
							    email: currentuser.user_email,
							    name: currentuser.user_name,
							});							 
							// track
							woopra.track();

							hhwt_success_login_action(response);
							user_status = 'Success';
		            	}
		            },
		            error: function() {
		            	$('#hhwt-modal-signup').modal('hide');
		            	$('#hhwt-modal-error').modal('show');
						user_status = 'Failure';
		            },
		            complete: function(){
		            	$(form)[0].reset();
		            	$(form).find(".form-loading").removeClass('show');
		            	$(form).find("[type='submit']").prop('disabled', false);
		            	dl_data = format_data_layer(eventLabel, user_status, 'login');
						hhwt_push_datalayer(dl_data);
		            }
		        });
		    }
	    });

	    /* Forgot password validation */
		$("#hhwt-forget-password").validate({
			rules:{
				forgetemail:{
					required:true,
					email:true
				}
			},
			messages:{
				forgetemail:{
					required:"Email is required",
					email:"Please enter valid email"
				}
			},
			submitHandler: function(form) {

		        var fpassword_data = hhwt_serialize(form);
		        $.ajax({
		            url: hhwt_ajaxurl,
		            type: "POST",
		            data: {
		                'action': 'hhwtapi_ajax_method',
	                    'gotomethod': 'hhwtapi_forgot_password',
	                    'user_data': fpassword_data
		            },
		            beforeSend: function(){
		            	$(form).find(".form-loading").addClass('show');
		            	$(form).find("[type='submit']").prop('disabled', true);
		            },
		            success: function(response) {
		            	if (response == 'failure') {
		            		$('.modal').modal('hide');
		            		$('#hhwt-modal-error').modal('show');
		            	} else {
		            		$('.hhwt-forget-form .modal-body > .alert').html(response);
		            	}
		            },
		            error: function() {
		            	$('.modal').modal('hide');
			            $('#hhwt-modal-error').modal('show');
		            },
		            complete: function(){
		            	$(form).find(".form-loading").removeClass('show');
		            	$(form).find("[type='submit']").prop('disabled', false);
		            }
		        });
		    }
		});

		

		/* Newsletter subscribe form 1 validation */
		$("#hhwt-newsletter-subscribe").validate({
			rules:{
				useremail:{
					required:true,
					email:true
				}
			},
			messages:{
				useremail:{
					required:"Please enter email",
					email:"Please enter valid email"
				}
			},
			submitHandler: function(form) {
		        var user_data = hhwt_serialize(form);
		        $.ajax({
		            url: hhwt_ajaxurl,
		            type: "POST",
		            data: {
		                'action': 'hhwtuser_ajax_method',
	                    'gotomethod': 'hhwtfe_create_newsletter_subscribe',
	                    'user_data': user_data
		            },
		            beforeSend: function(){
		            	$(form).find(".form-loading").addClass('show');
		            	$(form).find("[type='submit']").prop('disabled', true);
		            },
		            success: function(response) {
		            	if (response == 'failure') {
		            		$('#hhwt-modal-error').modal('show');
		            		$(".subscribe-popup-cont").hide();
		            	} else {
		            		$("#hhwt-newsletter-subscribe").hide();
		            		$("#subscribedmsg").show();
		            	}
		            },
		            error: function() {
		            	$('.modal').modal('hide');
			            $('#hhwt-modal-error').modal('show');
		            },
		            complete: function(){
		            	$(form).find(".form-loading").removeClass('show');
		            	$(form).find("[type='submit']").prop('disabled', false);
		            }
		        });
		    }
		});
		
		/* Subscribe form 2 validation */
		$("#hhwt-modal-city-guide-dark-form").validate({
			rules:{
				useremail:{
					required:true,
					email:true
				}
			},
			messages:{
				useremail:{
					required:"Please enter email",
					email:"Please enter valid email"
				}
			},
			submitHandler: function(form) {
		        var subscribe_data = hhwt_serialize(form);
		        var useremail = $(form).find('[name="useremail"]').val();
		        var country = $(form).find('[name="guide-country"]').val();
		        $.ajax({
		            url: hhwt_ajaxurl,
		            type: "POST",
		            data: {
		                'action': 'hhwtuser_ajax_method',
	                    'gotomethod': 'hhwtfe_create_newsletter_subscribe',
	                    'user_data': subscribe_data
		            },
		            beforeSend: function(){
		            	$(form).find(".form-loading").addClass('show');
		            	$(form).find("[type='submit']").prop('disabled', true);
		            },
		            success: function(response) {
		            	eventLabel = {
            				'email' : useremail,
            				'country' : country,
            			};
		            	if (response == 'success') {
		            		var nl_msg = '<i class="fa fa-check" aria-hidden="true"></i> You have subscribed!';
	            			user_status = 'Success';
		            	} else {
		            		var nl_msg = '<i class="fa fa-exclamation" aria-hidden="true"></i> Please try again later';
	            			user_status = 'Failure';
		            	}
            			dl_data = format_data_layer(eventLabel, user_status, 'newsletter');
						hhwt_push_datalayer(dl_data);
		            	$(".hhwt-alert-subscribe > button").html(nl_msg);
		            	$("#hhwt-modal-city-guide-dark-form").hide();
		            	$(".hhwt-alert-subscribe").show();
		            },
		            error: function() {
		            	$('.modal').modal('hide');
			            $('#hhwt-modal-error').modal('show');
		            },
		            complete: function(){
		            	$(form)[0].reset();
		            	$(form).find(".form-loading").removeClass('show');
		            	$(form).find("[type='submit']").prop('disabled', false);
		            }
		        });
		    }
		});

		/* Subscribe form 3 validation */
		$("#hhwt-modal-city-guide-light-form").validate({
			rules:{
				useremail:{
					required:true,
					email:true
				}
			},
			messages:{
				useremail:{
					required:"Please enter email ",
					email:"Please enter valid email"
				}
			},
			submitHandler: function(form) {
		        var subscribe_data = hhwt_serialize(form);
		        var useremail = $(form).find('[name="useremail"]').val();
		        var country = $(form).find('[name="guide-country"]').val();
		        $.ajax({
		            url: hhwt_ajaxurl,
		            type: "POST",
		            data: {
		                'action': 'hhwtuser_ajax_method',
	                    'gotomethod': 'hhwtfe_create_newsletter_subscribe',
	                    'user_data': subscribe_data
		            },
		            beforeSend: function(){
		            	$(form).find(".form-loading").addClass('show');
		            	$(form).find("[type='submit']").prop('disabled', true);
		            },
		            success: function(response) {
		            	eventLabel = {
            				'email' : useremail,
            				'country' : country,
            			};
		            	if (response == 'success') {
		            		var nl_msg = '<i class="fa fa-check" aria-hidden="true"></i> You have subscribed!';
	            			user_status = 'Success';
		            	} else {
		            		var nl_msg = '<i class="fa fa-exclamation" aria-hidden="true"></i> Please try again later';
	            			user_status = 'Failure';
		            	}
            			dl_data = format_data_layer(eventLabel, user_status, 'newsletter');
						hhwt_push_datalayer(dl_data);
		            	$(".hhwt-alert-subscribe > button").html(nl_msg);
		            	$("#hhwt-modal-city-guide-light-form").hide();
		            	$(".hhwt-alert-subscribe").show();
		            },
		            error: function() {
		            	$('.modal').modal('hide');
			            $('#hhwt-modal-error').modal('show');
		            },
		            complete: function(){
		            	$(form)[0].reset();
		            	$(form).find(".form-loading").removeClass('show');
		            	$(form).find("[type='submit']").prop('disabled', false);
		            }
		        });
		    }
		});
		/* Remove from bucket */
		$("#hhwt-form-remove-article").validate({
			submitHandler: function(form) {
		        var article_data = hhwt_serialize(form);
		        var $_this = $(form);
		        article_title = $('#hhwt-form-remove-article').find('[name="hdn-post-id"]').attr( 'data-article');
            	eventLabel = {
            		'article' : article_title,
            	};
		        $.ajax({
		            url: hhwt_ajaxurl,
		            type: "POST",
		            data: {
		                'action': 'hhwtapi_ajax_method',
	                    'gotomethod': 'hhwtapi_remove_article_from_bucket',
	                    'article_data': article_data
		            },
		            beforeSend: function(){
		            	$(form).find(".form-loading").addClass('show');
		            	$(form).find("[type='submit']").prop('disabled', true);
		            },
		            success: function(response) {
		            	if (response != 'failure') {
		            		var response_data = JSON.parse(response);
		            		$(".hhwt-modal-delete .alert").removeClass('alert-danger').addClass('alert-success');
		            		$(".hhwt-modal-delete .alert").html('<strong>Success!</strong> Article removed Successfully.');
		            		eventLabel.bucketname = response_data.bucket;
		            		user_status = 'Success';
		            		var article_id = $_this.find('[name="hdn-post-id"]').val();
                            $('.save-bucket[data-post-id="'+article_id+'"]')
                            .attr('data-in-bucket', false)
                            .find('i').removeClass('fa-heart').addClass('fa-heart-o');

                            if( $('.save-bucket[data-post-id="'+article_id+'"]').hasClass("hhwt-save-button") ) {
	                            $('.save-bucket.hhwt-save-button[data-post-id="'+article_id+'"]').html('<i class="fa fa fa-heart-o" aria-hidden="true"></i> Save');
	                        }
		            		setTimeout(function(){
								$('.modal').modal('hide');
								$(".hhwt-modal-delete .alert").removeClass('alert-success');
							}, 1000);
		            	} else if(response == 'trg-login') {
		            		$('.modal').modal('hide');
		            		$('#hhwt-modal-signup').modal('show');
		            	} else {
		            		$(".hhwt-modal-delete .alert").addClass('alert-danger');
		            		$(".hhwt-modal-delete .alert").html('<strong>Error!</strong> Please try again later.');
		            		setTimeout(function(){
								$('.modal').modal('hide');
								$(".hhwt-modal-delete .alert").removeClass('alert-danger');
							}, 1000);
							$('#hhwt-form-remove-article')[0].reset();
		            		user_status = 'Failure';
		            	}
		            },
		            error: function() {
	            		user_status = 'Failure';
		            	$('.modal').modal('hide');
			            $('#hhwt-modal-error').modal('show');
		            },
		            complete: function(){
		            	$(form).find(".form-loading").removeClass('show');
		            	$(form).find("[type='submit']").prop('disabled', false);
		            	dl_data = format_data_layer(eventLabel, user_status, 'remove article');
						hhwt_push_datalayer(dl_data);
		            }
		        }).done(function(response) {
                    var response_data = JSON.parse(response);
                    jQuery.ajax({
                        url: hhwt_ajaxurl,
                        type: "POST",
                        data: {
                            'action': 'hhwtapi_ajax_method',
                            'gotomethod': 'hhwtapi_save_gamify_data',
                            'gamify_data': response_data.gamify
                        },
                    });
                });
		    }
		});

		$(document).on("click", "#hhwt-modal-delete .modal-header .close", function(){
			article_title = $('#hhwt-form-remove-article').find('[name="hdn-post-id"]').attr( 'data-article');
			eventLabel = {
        		'article' : article_title,
        	};
        	user_status = 'User Closed';
        	dl_data = format_data_layer(eventLabel, user_status, 'remove article');
			hhwt_push_datalayer(dl_data);
		});

		$(document).on("click", "#hhwt-modal-save-to-bucket .modal-header .close", function(){
			// Skip plan trip
			var header_text = $(".save-bucket-modal-header h3").html();
			if (header_text == 'Planning a trip?'){
				$(".save-bucket-modal-header h3").html("Save to Bucket List");
				$(".hhwt-form-create-bucket .save-to-bucket").css("display" , "block");
				$(".hhwt-form-create-bucket .save-bucket-article").css("display" , "flex");
				$(".hhwt-form-create-bucket .planner-modal-body").css("display" , "none");
				$(this).attr("data-dismiss","modal");
			} else {
				// gtm data
				article_title = $('#hhwt-form-create-bucket').find('[name="hdn-title"]').attr( 'data-article');
				eventLabel = {
					'article' : article_title,
				};
				user_status = 'User Closed';
				dl_data = format_data_layer(eventLabel, user_status, 'save article');
				hhwt_push_datalayer(dl_data);
			}
		});

		$('#hhwt-modal-save-to-bucket.modal').on('hidden.bs.modal', function(){
			$('#hhwt-modal-save-to-bucket .modal-header .close').removeAttr("data-dismiss");
		});

		$(document).on("click", "#hhwt-modal-signup .modal-header .close", function(){
			eventLabel = '';
			user_status = 'User Closed';
			dl_data = format_data_layer(eventLabel, user_status, 'login');
			hhwt_push_datalayer(dl_data);
		});

		$(document).on("click", ".hhwt-modal-city-guide .modal-header .close", function(){
			form_status = $('.hhwt-alert-subscribe').css('display');
			if ( form_status == 'none' ) {
				eventLabel = '';
    			user_status = 'User Closed';
    			dl_data = format_data_layer(eventLabel, user_status, 'newsletter');
				hhwt_push_datalayer(dl_data);
			}
		});

		/* create new bucket */
		$(document).on("click", ".btn-create-bucket", function(){

			$('.btn-save-article-to-bucket').prop('disabled', true);

			var new_bucket = $(this).prev('input').val();

			if (validate_value(new_bucket)) {
				$(this).prev('input').css({"border":"1px solid #08CCCB"});
				$.ajax({
		            url: hhwt_ajaxurl,
		            type: "POST",
		            data: {
		                "action": 'hhwtapi_ajax_method',
	                    "gotomethod": 'hhwtapi_create_new_bucket',
	                    "bucket_name": new_bucket
		            },
		            beforeSend: function(){
		            	$("#hhwt-form-create-bucket .form-loading").show();
		            },
		            success: function(bucket_id) {
		            	if (bucket_id != 'failure') {
		            		$('.hhwt-select-bucketname').removeClass('hidden');
		            		$('.hhwt-select-list ').removeClass('without-bucket');
		            		$('.hhwt-select-bucketname').append('<option value="'+bucket_id+'" selected>'+new_bucket+'</option>');
		            		$('.hdn-bucket-name').val(new_bucket);
		            		$('.hhwt-text-input').slideUp();
		            		$('.btn-save-article-to-bucket').prop('disabled', false);
		            	} else if(bucket_id == 'trg-loggin') {
		            		return trg_loggin();
		            	}
		            },
		            error: function() {
		            	$('.modal').modal('hide');
			            $('#hhwt-modal-error').modal('show');
		            },
		            complete: function(){
		            	$("#hhwt-form-create-bucket .form-loading").hide();
		            }
		        });
			} else {
				$(this).prev('input').css({"border":"1px solid #f00"});
			}
		})
		
		/* Homepage featured slider post link */
		if( $('#featured-carousel .active').length ){
			custom_link = $('#featured-carousel .active .featured-title h1 a').attr('href');
			$('.hhwt-fetured-details-selection .hhwt-home-btn-go').attr('href',custom_link);

			$('#featured-carousel').bind('slid.bs.carousel', function (e) {
				custom_link = $('#featured-carousel .active .featured-title h1 a').attr('href');
				$('.hhwt-fetured-details-selection .hhwt-home-btn-go').attr('href',custom_link);
			});
		};

		/* Article page modal widget */
	    var is_Blogmodal = false;
		$(window).scroll(function () {
			var current_pos = $(window).height() + $(window).scrollTop();
	        var doc_height = $(document).height();
			if(is_Blogmodal === false){
				if ( current_pos >= doc_height/2 ) {
					$("#hhwt-modal-widget")
						.prop('class', 'modal')// revert to default
						.addClass( 'right' );
					$("#hhwt-modal-widget").show();
					is_Blogmodal = true;
				}
			}
		});

		/* Trigger login form */
		$(document).on("click", '.hhwt-trg-login', function(){
			$('#hhwt-modal-signup').modal('show');
		});

		$(document).on("click", '.collapse-menu', function(){
			jQuery("#hhwt-mobile-mmenu").removeClass('navbar-collapse');		
		});

		/* writer's form age field */
		$('span.other-age').hide();
		$('.cf-writers-age').change(function(){
			if($('.cf-writers-age').val() == 'Others') {
				$('span.other-age').show();
			} else {
				$('span.other-age').hide();
			}
		});

	    $("#signuptxt, #login-email, .hhwt-back, #forget-password, #hhwt-back-forget").on("click",function(){
	        var clickEleID;
	        if ($(this).hasClass("hhwt-back") === true ) {
	            clickEleID = $(this).attr("class");
	        } else {
	            clickEleID = $(this).attr("id");
	        }
	        switch(clickEleID) {
	            case "signuptxt":
	                $(".hhwt-signup-form").show();
	                $(".hhwt-signup-account").hide();
	                break;
	            case "login-email":
	                $(".hhwt-login-form").show();
	                $(".hhwt-signup-account").hide();
	                break;
	            case "hhwt-back":
	                $(".hhwt-signup-form").hide();
	                $(".hhwt-login-form").hide();
	                $(".hhwt-signup-account").show();
	                break;
	            case "forget-password":
				    $(".hhwt-login-form").hide();
				    $(".hhwt-forget-form").show();
			        break;
			    case "hhwt-back-forget":
				    $(".hhwt-forget-form").hide();
				    $(".hhwt-login-form").show();
			        break;
			    default:
	                // code block
	        }
	    });

		/* check and trigger login */
		hhwt_trigger_user_login();

		/* set default href */
		hhwt_set_default_href();

		/* init Infinte Scroll */
		scroll_args = {
			path: '.page-numbers',
			append: '.hhwt-nested-posts',
			scrollThreshold: 0,
			status: '.hhwt-page-load-status',
			hideNav: '.page-numbers',
			checkLastPage: true,
			history: false,
		};
		var $infinte_scroll = $('.hhwt-has-pagination').infiniteScroll(scroll_args);

		$infinte_scroll.on( 'last.infiniteScroll', function() {
			setTimeout(function(){
				$('.hhwt-page-load-status').slideUp(1500);
			}, 1000);
		});

	    $(".hhwt-child-cat-link .visible-xs .hhwt-nav-pills li.active").append('<span class="fa fa-chevron-down"></span>');

		$(window).scroll(function() {
			var $_scrolltop = $(this).scrollTop();
			var top_px = '45px';
			// Go to top button
			if ($_scrolltop > 100) {
				$('#go-top-button').css({
					bottom: top_px
				});
			} else {
				$('#go-top-button').css({
					bottom: '-100px'
				});
			}
		});

		$('#go-top-button').on("click", function() {
			$('html, body').animate({
				scrollTop: 0
			}, 700);
			return false;
		});

		/* check bigtapp cookie */
		if (!$.cookie('people_id')) {
			hhwt_set_bigtapp_cookie();
		}

	}); //end of ready function

	$('#hhwt-form-registration').on('keyup keypress', function(e) {
		var keyCode = e.keyCode || e.which;
		if (keyCode === 13) {
			e.preventDefault();
			return false;
		}
	});

	$('#hhwt-modal-signup.modal').on('hidden.bs.modal', function(){
		$(this).find("#hhwt-form-registration")[0].reset();
		$(this).find("#hhwt-form-login")[0].reset();
		$(this).find("#hhwt-forget-password")[0].reset();
		$(this).find("input").removeClass('error');
		$(".hhwt-signup-account").css('display','block');
		$(".hhwt-login-form, .hhwt-forget-form, .hhwt-signup-form, #hhwt-modal-signup.modal label").css('display','none');
	});

	$(".next").click(function(){
		var curStep = $(this).closest("fieldset");
		curStepBtn = curStep.attr("id");
		 if(curStepBtn=="Step1"){
			var validator = $( "#hhwt-form-registration" ).validate();
			var email     = validator.element("[name='register[user_email]']");
			var pass      = validator.element("[name='register[user_password]']");
			if(pass== false || email== false){
				return false;
			} else{
				$(".hhwt-signup-form #Step2").css("display" , "block");
				$(".hhwt-signup-form #Step1").css("display" , "none");
			}
		}
	});

	$(".prev").click(function(){
		$(".hhwt-signup-form #Step2").css("display" , "none");
		$(".hhwt-signup-form #Step1").css("display" , "block");
	});

	$("body").click(function(){
		$('#hhwt-modal-saved-list ,#hhwt-modal-notification').modal('hide');
	});

	$('#hhwt-modal-saved-list .saved-bucket-modal-content, #hhwt-modal-notification .hhwt-modal-notification-dialog').click(function(event) {
		event.stopPropagation();
	});

	$(document).on("click", ".hhwt-create-new", function() {
		$(this).css("background" , "#F5F6F8");
		$(this).css("color" , "#B5BFC0");
		$('.btn-save-article-to-bucket').prop('disabled', true);
		$(".hhwt-text-input").slideDown(300);
		text_length = $(".hhwt-text-input input").val();
		if (text_length.length == 0){
			$(".hhwt-text-input .btn-create-bucket").css('display', 'none');
			$(".hhwt-text-input .btn-cancel-bucket").css('display', 'inline-block');
		}
	});
	
	$(document).on("keyup", ".hhwt-text-input input", function() {
		text_length = $(".hhwt-text-input input").val();
		if (text_length.length > 0){
			$(".hhwt-text-input .btn-create-bucket").css('display', 'inline-block');
			$(".hhwt-text-input .btn-cancel-bucket").css('display', 'none');
		}
		else if (text_length.length == 0){
			$(".hhwt-text-input .btn-create-bucket").css('display', 'none');
			$(".hhwt-text-input .btn-cancel-bucket").css('display', 'inline-block');
		}
	});

	$(document).on("click", ".btn-create-bucket", function() {
		$(".hhwt-create-new").css("background" , "#08CCCB");
		$(".hhwt-create-new").css("color" , "#fff");
	});

	$(document).on("click", ".btn-cancel-bucket", function() {
		$(".hhwt-create-new").css("background" , "#08CCCB");
		$(".hhwt-create-new").css("color" , "#fff");
		$(".hhwt-text-input").slideUp(300);
		$("#hhwt-form-create-bucket")[0].reset();
		$("#hhwt-form-create-bucket").find("[type='submit']").prop('disabled', false);
	});

	/* Bucket list functions */
	$(document).on("click", ".save-bucket", function(){
		var post_id = $(this).attr('data-post-id');
		var in_bucket = $(this).attr('data-in-bucket');
		var article_title = $(this).next('h3').find('a').text();
		if (article_title) {
			article_title = hhwt_addslashes(article_title);
		}

		if (validate_value(post_id) && in_bucket == 'false') {

			$('#hhwt-modal-save-to-bucket').modal('show');

			$.when( hhwt_article_content(post_id), hhwt_bucket_content(post_id) ).done(function(  article_data, bucket_data) {

				if (article_data == 'failure' || bucket_data == 'failure') {
					$.cookie('trg-bucket-article', post_id, { expires: 1 });
					return trg_loggin();
				}

				if(validate_value(article_data) && validate_value(bucket_data)) {
					var modal_content = article_data + bucket_data;
					$('#hhwt-modal-save-to-bucket .modal-body').html(modal_content);
					planner_content = $(".hhwt-form-create-bucket .planner-modal-body").length;
					if (planner_content > 0){
						$(".save-bucket-modal-header h3").html("Planning a trip?");
					}
					else{
						$('#hhwt-modal-save-to-bucket .modal-header .close').attr("data-dismiss","modal");
						$(".hhwt-form-create-bucket .save-to-bucket").css("display" , "block");
						$(".hhwt-form-create-bucket .save-bucket-article").css("display" , "flex");
					}
					setTimeout(function(){
						$("body").addClass('modal-open');
					}, 500);
				} else {
					$(".modal").modal('hide');
					$("#hhwt-modal-error").modal('show');
				}
			});
			$('#hhwt-form-create-bucket').find('[name="hdn-title"]').attr( 'data-article', article_title);
		} else {
			$('#hhwt-form-remove-article').find('[name="hdn-post-id"]').val(post_id);
			$('#hhwt-form-remove-article').find('[name="hdn-post-id"]').attr( 'data-article', article_title);
			$('#hhwt-modal-delete').modal('show');
		}
	});

	$(document).on("click", ".article-banner .hhwt-save-button", function(){
		var article_title = $('.article-banner .banner-content').find('h3').text();
		$('#hhwt-form-create-bucket').find('[name="hdn-title"]').attr( 'data-article', article_title);
		$('#hhwt-form-remove-article').find('[name="hdn-post-id"]').attr( 'data-article', article_title);
	});

	/* Entire destination clickable */
	$('#destination-carousel .modern-medium').on('click', function(){
		window.location=($(this).find("h3 a").attr("href"));
	});

	/* Remove cookie on login modal close */
	$(document).on("click", "#hhwt-modal-signup .close", function(){
		if ($.cookie('trg-bucket-article')) {
			$.removeCookie('trg-bucket-article');
		}
	});

	$(document).on("click", ".trip-plan-travel > p", function(){
		var user_trip = $(this).attr('data-value');
		if (validate_value(user_trip)) {
			$("#hhwt-form-create-bucket [name='hdn-trip-plan']").val(user_trip);
		}
		$(this).addClass("selected");
		$(".planner-modal-body").find("p").not(this).removeClass("selected");
	});

	$(document).on("click", ".hhwt-btn-trip-choose", function(){
		var $_modal = $(this).parents('.modal-body');
		var city_slug = $_modal.find('[name="hdn-city-slug"]').val();
		var hdn_id = $_modal.find('[name="hdn-id"]').val();
		var user_trip = $('.trip-plan-travel p.selected').text();

		if (validate_value(city_slug)) {
			city_name = city_slug.replace("-", " ");
			city_name = hhwt_capitalize(city_name);
		}

		if ($(".planner-modal-body p").hasClass("selected")) {
			$(".hhwt-form-create-bucket .save-to-bucket").css("display" , "block");
			$(".hhwt-form-create-bucket .save-bucket-article").css("display" , "flex");
			$(".hhwt-form-create-bucket .planner-modal-body").css("display" , "none");
			$(".save-bucket-modal-header .save-prev").css("display" , "block");
			$(".save-bucket-modal-header h3").html("Save to Bucket List");
		} else{
			$(".hhwt-form-create-bucket .alert").addClass('alert-danger');
			$(".hhwt-form-create-bucket .alert").html('Please plan your trip!');
			setTimeout(function(){
				$(".hhwt-form-create-bucket .alert").removeClass('alert-danger');
			}, 3000);
			return false;
		}
	});

	$(document).on("click", ".save-prev", function(){
		$(".hhwt-form-create-bucket .save-to-bucket, .hhwt-form-create-bucket .save-bucket-article").css("display" , "none");
		$(".hhwt-form-create-bucket .planner-modal-body").css("display" , "block");
		$(".save-bucket-modal-header .save-prev").css("display" , "none");
		$(".save-bucket-modal-header h3").html("Planning a trip?");
	});	

	$('#hhwt-modal-save-to-bucket').on('hidden.bs.modal', function(){
		$(".save-bucket-modal-content .save-bucket-modal-header .save-prev").css("display" , "none");
		$(".save-bucket-modal-header h3").html("Save to Bucket List");
		if(loading_img){
			$('#hhwt-modal-save-to-bucket .modal-body').html(loading_img);
		}
	});

	$(document).on('click', '.hhwtmainmenu-notification', function(){
        $("#hhwt-modal-notification").modal('show');
		$.ajax({
		 	url:hhwt_ajaxurl,
		 	type:"POST",
		 	data: {
		 		"action": 'hhwtapi_ajax_method',
		 		"gotomethod":'hhwtapi_user_notifications'
		 	},
		 	success: function(response) {
                $("#hhwt-modal-notification .modal-body.hhwt-notification-modal-body").html(response);
        		$("#hhwt-modal-notification .modal-body.hhwt-notification-modal-body").css('display', 'block');
				$("#hhwt-modal-notification .hhwt-notification-modal-body.notification-load").css('display', 'none');
			},
		});
	});

	$('#hhwt-modal-notification').on('hidden.bs.modal', function() {
        $("#hhwt-modal-notification .modal-body.hhwt-notification-modal-body").css('display', 'none');
		$("#hhwt-modal-notification .hhwt-notification-modal-body.notification-load").css('display', 'block');
	});

	/* Newsletter popup style 1*/
	$(document).on("click",".closebtncont", function(){
	    $(this).closest(".subscribe-popup-cont").hide();
	    $(this).closest("body").removeClass("bgshadow");
	});

	$(document).on("click",".expandbtncont", function(){
	    $(this).closest("body").addClass("bgshadow");
	    $(this).closest(".subscribe-popup-cont").addClass("active");
	});

	$('.hhwt-modal-city-guide').on('hidden.bs.modal', function() {
		$('.hhwt-travel-guide').show();
		$('.hhwt-form-city-subscribe').show();
		$('.hhwt-alert-subscribe').hide();
	});

	$(document).on("click",".hhwt-user-menu ul .hhwt-session-end a", function(){
		hhwt_destory_session();
	});

	$(document).on("change", ".save-to-bucket select", function() {
        var bucket_name = $(".save-to-bucket select option:selected").text();
        $('.hdn-bucket-name').val(bucket_name);
    });

}(jQuery));

/* nav menu hover */
jQuery('li.hhwtmainhassubmenu:not(.hhwtsecondsubmenu)>a').on('click', function() {
	jQuery(this).closest(".menucontainer").find("li.hhwtmainhassubmenu.active").removeClass("active");
	jQuery(this).closest("li").toggleClass("active");
	jQuery(this).closest(".menucontainer").find('ul.submenucontainer').not(this).slideUp(300);
	jQuery(this).closest("li").find('ul').stop(true).slideToggle(300);
	return false;
});

/* navbar menu click */
jQuery('.hhwt-mob-col-button').on('click', function() {
	jQuery(".hhwt-navbar").find("#hhwt-mob-nav-search.collapse.in").removeClass("in");
});

jQuery('.hhwt-mob-search').on('click', function() {
	jQuery(".hhwt-navbar").find("#hhwt-mob-nav-menu.collapse.in").removeClass("in");
});

jQuery(document).on("mouseenter", ".hhwtmainmenu", function(){
    jQuery(this).addClass("open");
});

jQuery(document).on("mouseleave", ".hhwtmainmenu", function(){
    jQuery(this).removeClass("open");
});

jQuery('.hhwt-child-cat-link .visible-xs .hhwt-nav-pills li.active').on('click', function() {
	jQuery(this).toggleClass('remove-border');
	jQuery(".hhwt-child-cat-link .visible-xs .hhwt-nav-pills").toggleClass('category-slide');
	jQuery(".hhwt-child-cat-link .hhwt-nav-pills li").toggle();
	jQuery(this).css('display', 'inline-flex');
	if (jQuery(".hhwt-child-cat-link .visible-xs .hhwt-nav-pills li.active span").hasClass('fa-chevron-down')){
		jQuery(".hhwt-child-cat-link .visible-xs .hhwt-nav-pills li.active span").removeClass('fa-chevron-down').addClass('fa-chevron-up');
	} else {
		jQuery(".hhwt-child-cat-link .visible-xs .hhwt-nav-pills li.active span").removeClass('fa-chevron-up').addClass('fa-chevron-down');
	}
});

/*set cookie*/
jQuery.ajax({
	url: hhwt_ajaxurl,
	success: function (location) {
		userIPData = JSON.stringify(location);
	}
}, "json");

/*bigtapp get ip*/
jQuery.ajax({
	url: 'https://freegeoip.net/json/',
	async: false,
	dataType: 'json',
	success: function (location) {
		userIPData = JSON.stringify(location);
	}
}, "json");

/* bigtapp analytics*/
var userIP_array = JSON.parse(userIPData);
start = new Date().getTime();

var doAjaxBeforeUnloadEnabled = true;
var doAjaxBeforeUnload = function (evt) {

	end = new Date().getTime();

	var bigtappdata = {
		'userip':userIP_array, 
		'people_id': jQuery.cookie('people_id'),
		'dwell_time': msToTime(end - start), 
		'current_url' : current_url,
		'previous_url' : previous_url,
		'user_agent' : navigator.userAgent,
		'device' : user_device
	};

	jQuery.ajax({
		type: "POST",
		url: hhwt_ajaxurl,
		async: false,
		data: {
			'action': 'hhwtuser_ajax_method',
            'gotomethod': 'hhwtfe_track_bigtapp',
            'bigtappdata': bigtappdata
		},		
		success: function (response) {
			id_1 = response.d;
		},
		failure: function (response) {
			id_1 = 0;
		},
		error: function (response) {
			id_1 = 0;
		}
	});

	if (id_1 == 0) {
		var message = 'Save it before you close.';
		if (typeof evt == 'undefined') {
			evt = window.event;
		} 
		if (evt) {
			evt.returnValue = message;
		}
		return message;
	}
}
window.addEventListener("pagehide", doAjaxBeforeUnload, false);

jQuery('#newletterpopup2 .hhwt-widget-guide, #newletterpopup2 .hhwt-widget-image').on('click', function() {
	jQuery('#newletterpopup2').hide();
	jQuery('#hhwt-modal-city-guide-light').modal('show');
});

jQuery('#newletterpopup3 .hhwt-widget-guide, #newletterpopup3 .hhwt-widget-image').on('click', function() {
	jQuery('#newletterpopup3').hide();
	jQuery('#hhwt-modal-city-guide-dark').modal('show');
});

jQuery('.hhwt-widget-guide, .hhwt-widget-image').on('click', function() {
	var guide_country = jQuery('.hhwt-travel-guide').attr('data-country');
	if (validate_value(guide_country)) {
		jQuery(".hhwt-form-city-subscribe [name='guide-country']").val(guide_country);
	}
});