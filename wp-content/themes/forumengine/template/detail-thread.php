<?php
global $thread, $post, $thread_author;
?>
<div class="post-display">
	<ul class="control-thread">
		<?php
			if (user_can_edit($thread)) {
		?>
			<li><a href="#" class="edit-topic-thread control-edit" data-toggle="tooltip" title="<?php _e('Edit', ET_DOMAIN) ?>"><span class="icon" data-icon="p"></span></a></li>
		<?php } ?>
			<li><a href="#" class="control-quote" data-toggle="tooltip" title="<?php _e('Quote', ET_DOMAIN) ?>"><span class="icon" data-icon='"'></span></a></li>
		<?php if ( !$thread->reported ){?>
			<li><a href="#" class="control-report" data-toggle="tooltip" title="<?php _e('Report', ET_DOMAIN) ?>"><span class="icon" data-icon='!'></span></a></li>
		<?php } ?>
	</ul>
	<div class="name">
		<a  class="post-author" href="<?php echo get_author_posts_url( $post->post_author ) ?>"><span itemprop="author"><?php echo $thread_author; //the_author() ?></span></a>
		<span class="comment">
			<span class="<?php if ( $thread->replied ) echo 'active' ?>">
				<span data-icon="w" class="icon"></span>
				<span itemprop="interactionCount" class="count"><?php echo $thread->et_replies_count ?></span>
			</span>
		</span>
		<span class="like">
			<a href="#" class="like-post <?php if ($thread->liked) echo 'active' ?>" data-id="<?php echo $thread->ID ?>">
				<span data-icon="k" class="icon"></span>
				<span  itemprop="interactionCount" class="count"><?php echo $thread->et_likes_count ?></span>
			</a>
		</span>
		<span class="date"><?php echo et_the_time( strtotime( $thread->post_date ) ); ?></span>
	</div>
	<div class="content">
		<?php the_content(); ?>
	</div>
	<!-- custom fields data go here -->
	<div class="custom-fields-data">
		<?php do_action( 'custom_fields_data', $thread->ID );?>
	</div>
</div>