<?php

class HHWTSession{

	private $user_session;

	private $bucket_article;

	private $user_bucket;

	private $user_avatar;

	private $api_cities;

	private $session_data;

	public function __construct()
	{
		$this->user_session = 'current_user';

		$this->bucket_article = 'bucket_article';

		$this->user_bucket = 'user_bucket';

		$this->user_avatar = 'user_avatar';

		$this->api_cities = 'api_cities';

		$this->session_data = 'session_data';
	}


	public function set_user_session($data='')
	{
		if ($data) {
			$_SESSION[$this->user_session] = $data;
		}
	}

	public function get_user_session()
	{
		if(isset($_SESSION[$this->user_session]))
			return $_SESSION[$this->user_session];

		return false;
	}

	public function set_bucket_article($data='')
	{
		if ($data) {
			$_SESSION[$this->bucket_article] = $data;
		}
	}

	public function get_bucket_article()
	{
		if(isset($_SESSION[$this->bucket_article]))
			return $_SESSION[$this->bucket_article];

		return false;
	}

	public function set_user_bucket($data='')
	{
		if ($data) {
			$_SESSION[$this->user_bucket] = $data;
		}
	}

	public function get_user_bucket()
	{
		if(isset($_SESSION[$this->user_bucket]))
			return $_SESSION[$this->user_bucket];

		return false;
	}

	public function set_user_avatar($data='')
	{
		if ($data) {
			$_SESSION[$this->user_avatar] = $data;
		}
	}

	public function get_user_avatar()
	{
		if(isset($_SESSION[$this->user_avatar]))
			return $_SESSION[$this->user_avatar];

		return false;
	}

	public function push_bucket_article($key='', $value)
	{
		if($key)
			$_SESSION[$this->bucket_article][$key] = $value;
		else
			$_SESSION[$this->bucket_article] = $value;
	}

	public function pop_bucket_article($key='')
	{
		if($key && isset($_SESSION[$this->bucket_article][$key]))
			unset($_SESSION[$this->bucket_article][$key]);
	}

	public function push_user_bucket($key='', $value)
	{
		if($key)
			$_SESSION[$this->user_bucket][$key] = $value;
		else
			$_SESSION[$this->user_bucket] = $value;
	}

	public function set_user_cities($data='')
	{
		if($data) {
			$_SESSION[$this->api_cities] = $data;
		}
	}

	public function get_user_cities()
	{
		if(isset($_SESSION[$this->user_avatar]))
			return $_SESSION[$this->user_avatar];

		return false;	
	}

	public function set_session_data($data='')
	{
		if($data) {
			$_SESSION[$this->session_data] = $data;
		}
	}

	public function get_session_data()
	{
		if(isset($_SESSION[$this->session_data]))
			return $_SESSION[$this->session_data];

		return false;	
	}
}

