<?php


class HHWTNavWalker extends Walker_Nav_Menu {

	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat( "\t", $depth );
		if( $depth===1){
			$output .= "\n$indent<ul role=\"menu\" class=\" submenucontainer dropdown-menu\">\n";
		}else if( $depth===2){
			$output .= "\n$indent<ul role=\"menu\" class=\" innermenucontainer dropdown-menu\" style=\"display:none !important;\">\n";
		}else{
			$output .= "\n$indent<ul role=\"menu\" class=\" menucontainer dropdown-menu\">\n";
		}
	}

	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';	

		if($depth<3){
			if ( strcasecmp( $item->attr_title, 'divider' ) == 0 && $depth === 1 ) {
				$output .= $indent . '<li role="presentation" class="divider">';
			} else if ( strcasecmp( $item->title, 'divider') == 0 && $depth === 1 ) {
				$output .= $indent . '<li role="presentation" class="divider">';
			} else if ( strcasecmp( $item->attr_title, 'dropdown-header') == 0 && $depth === 1 ) {
				$output .= $indent . '<li role="presentation" class="dropdown-header">' . esc_attr( $item->title );
			} else if ( strcasecmp($item->attr_title, 'disabled' ) == 0 ) {
				$output .= $indent . '<li role="presentation" class="disabled"><a href="#">' . esc_attr( $item->title ) . '</a>';
			} else {
				$class_names = $value = '';
				

				$classes = empty( $item->classes ) ? array() : (array) $item->classes;
				$classes[] = 'menu-item-' . $item->ID;
				$iconmenu_name ='';
				if ( in_array('hhwt-menu-icons', $classes)) {
					$key = array_search('hhwt-menu-icons', $classes);
					$iconmenu_name .= $classes[$key];
					$iconmenu_name .= " ".$classes[$key - 1];
					$classes[$key] = '';
					$classes[$key-1] = '';
				}
				
				$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
				/*
				if ( $args->has_children )
					$class_names .= ' dropdown';
				*/
				if($args->has_children && $depth === 0) { $class_names .= ' dropdown'; } elseif($args->has_children && $depth > 0) { $class_names .= ' dropdown-submenu'; }
				if ( in_array( 'current-menu-item', $classes ) )
					$class_names .= ' active';

				if($depth === 0){
					$class_names .= ' hhwtmainmenu';
				}else if($depth === 1){
					$class_names .=($args->has_children)?' hhwtmainsubmenu hhwtmainhassubmenu':' hhwtmainsubmenu';
					
				}else if($depth === 2){
					$class_names .= ' hhwtsecondsubmenu';
				}

				
				
				$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
				$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
				$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

				if($item->title == 'searchbox'){
					$output .= $indent . '<li class="hhwt-dropdown-li searchmenu"><p class="dropdown-search input-group">
					<span class="input-group-addon"><i class="fa fa-search"></i></span>
					<input class="form-control" placeholder="Search within Guide..." type="text">
					<span class="search-span btn">Search</span>
					</p>';
				}else{
					$output .= $indent . '<li' . $id . $value . $class_names .'>';
				}

				$atts = array();
				$atts['title']  = ! empty( $item->title )	? $item->title	: '';
				$atts['title']  = ! empty( $item->attr_title )	? $item->attr_title	: '';
				$atts['target'] = ! empty( $item->target )	? $item->target	: '';
				$atts['rel']    = ! empty( $item->xfn )		? $item->xfn	: '';
				// If item has_children add atts to a.
				// if ( $args->has_children && $depth === 0 ) {
				if ( $args->has_children ) {
					if($depth !== 2){
						$atts['href']			= '#';
						$atts['data-toggle']	= 'dropdown';
						$atts['class']			= ' dropdown-toggle';
					}else if($depth === 2){
						$atts['href'] = ! empty( $item->url ) ? $item->url : '';
					}
					
				} else {
					$atts['href'] = ! empty( $item->url ) ? $item->url : '';
				}
				$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );
				$attributes = '';

				foreach ( $atts as $attr => $value ) {
					
					if ( ! empty( $value ) ) {
						$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
						$attributes .= ' ' . $attr . '="' . $value . '"';
					}
				}
				
				$item_output = $args->before;
				// Font Awesome icons
				if ( ! empty( $iconmenu_name ) )
					$item_output .= '<a'. $attributes .'><span class="' . esc_attr( $iconmenu_name ) . '">';
				else{
					if($item->title != 'searchbox'){
						$item_output .= '<a'. $attributes .'>';
					}
				}

				if ( ! empty( $iconmenu_name ) ){
					$item_output .= '</span>';
				}
				if($item->title != 'searchbox'){
					$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
					$item_output .= ( $args->has_children && 0 === $depth ) ? ' <span class="caret"></span></a>' : '</a>';
					$item_output .= $args->after;
				}
				$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
			}
		}
	}

	public function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {
		if ( ! $element )
		return;
			$id_field = $this->db_fields['id'];
		// Display this element.
		if ( is_object( $args[0] ) )
			$args[0]->has_children = ! empty( $children_elements[ $element->$id_field ] );
			parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
	}

	public static function fallback( $args ) {
		if ( current_user_can( 'manage_options' ) ) {
			extract( $args );
			$fb_output = null;
			if ( $container ) {
				$fb_output = '<' . $container;
				if ( $container_id )
					$fb_output .= ' id="' . $container_id . '"';
				if ( $container_class )
					$fb_output .= ' class="' . $container_class . '"';
				$fb_output .= '>';
			}
			$fb_output .= '<ul';
			if ( $menu_id )
				$fb_output .= ' id="' . $menu_id . '"';
			if ( $menu_class )
				$fb_output .= ' class="' . $menu_class . '"';
			$fb_output .= '>';
			$fb_output .= '<li><a href="' . admin_url( 'nav-menus.php' ) . '">Add a menu</a></li>';
			$fb_output .= '</ul>';
			if ( $container )
				$fb_output .= '</' . $container . '>';
			echo $fb_output;
		}
	}

}