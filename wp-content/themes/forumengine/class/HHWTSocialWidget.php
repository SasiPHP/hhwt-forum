<?php

class HHWTSocialWidget extends WP_Widget {

    function __construct() {
        parent::__construct(
            'hhwt_social_widget', // Base ID
            esc_html__( 'HHWT:Follow Us', 'hhwt' ), // Name
            array( 'description' => esc_html__( 'Follow us link for social media', 'hhwt' ) ) // Args
        );
    }

    public function widget( $args, $instance ) 
    {
    	echo $args['before_widget'];

        $widget_title = $instance['widget_title'];
		$facebook = $instance['facebook'];
        $instagram = $instance['instagram'];
        $twitter = $instance['twitter'];
        ?>
        <h4><?php echo $widget_title; ?></h4>
        <div class="col-lg-offset-0 col-lg-4 col-md-offset-0 col-md-4 col-sm-offset-0 col-sm-4 col-xs-offset-3 col-xs-2">
            <a href="<?php echo $facebook;?>"><i class="fa fa-facebook social_fb"></i></a>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-2">
            <a href="<?php echo $instagram;?>"><i class="fa fa-instagram social"></i></a>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-2">
            <a href="<?php echo $twitter;?>"><i class="fa fa-twitter social"></i></a>
        </div>
        <?php
        echo $args['after_widget'];
    }

    public function form( $instance ) 
    {
        $widget_title = ! empty( $instance['widget_title'] ) ? $instance['widget_title'] : '';
		$facebook = ! empty( $instance['facebook'] ) ? $instance['facebook'] : '';
        $instagram = ! empty( $instance['instagram'] ) ? $instance['instagram'] : '';
        $twitter = ! empty( $instance['twitter'] ) ? $instance['twitter'] : '';
		?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'widget_title' ) ); ?>"><?php esc_attr_e( 'Widget Title:', 'hhwt' ); ?></label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'widget_title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'widget_title' ) ); ?>" type="text" value="<?php echo esc_attr( $widget_title ); ?>">
        </p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'facebook' ) ); ?>"><?php esc_attr_e( 'Facebook Url:', 'hhwt' ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'facebook' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'facebook' ) ); ?>" type="text" value="<?php echo esc_attr( $facebook ); ?>">
		</p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'instagram' ) ); ?>"><?php esc_attr_e( 'Instagram Url:', 'hhwt' ); ?></label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'instagram' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'instagram' ) ); ?>" type="text" value="<?php echo esc_attr( $instagram ); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'twitter' ) ); ?>"><?php esc_attr_e( 'Twitter Url:', 'hhwt' ); ?></label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'twitter' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'twitter' ) ); ?>" type="text" value="<?php echo esc_attr( $twitter ); ?>">
        </p>
		<?php 
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['widget_title'] = ( ! empty( $new_instance['widget_title'] ) ) ? strip_tags( $new_instance['widget_title'] ) : '';
        $instance['facebook'] = ( ! empty( $new_instance['facebook'] ) ) ? strip_tags( $new_instance['facebook'] ) : '';
        $instance['instagram'] = ( ! empty( $new_instance['instagram'] ) ) ? strip_tags( $new_instance['instagram'] ) : '';
        $instance['twitter'] = ( ! empty( $new_instance['twitter'] ) ) ? strip_tags( $new_instance['twitter'] ) : '';
		return $instance;
	}

} 