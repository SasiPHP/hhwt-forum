<?php

if ( ! class_exists( 'CT_TAX_META' ) ) {

class HHWTTermMeta {

	public function __construct() {
		//
	}
 
	 /*
	  * Initialize the class and start calling our hooks and filters
	  * @since 1.0.0
	 */
	public function init() {
		add_action( 'category_add_form_fields', array ( $this, 'add_category_image' ), 10, 2 );
		add_action( 'created_category', array ( $this, 'save_category_image' ), 10, 2 );
		add_action( 'category_edit_form_fields', array ( $this, 'update_category_image' ), 10, 2 );
		add_action( 'edited_category', array ( $this, 'save_category_image' ), 10, 2 );
		add_action( 'admin_enqueue_scripts', array( $this, 'load_media' ) );
	}

	public function load_media() {
        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_enqueue_style('thickbox');
	}
 
	/*
	* Add a form field in the new category page
	* @since 1.0.0
	*/
	public function add_category_image ( $taxonomy ) {
		?>
		<div class="form-field term-group">
			<label for="is-dest-category"><?php _e('Destination Category'); ?></label>
			<p><input type="checkbox" name="is-dest-category" value="1"/><?php _e('Is Destination Category'); ?></p>
		</div>

		<div class="form-field term-group">
			<label for="dest-category-order"><?php _e('Destination Order'); ?></label>
			<p><input type="number" class="regular-text" name="dest-category-order" value="1" min="1" max="20"/></p>
		</div>

		<div class="form-field term-group">
			<label for="catmeta[cat-image]"><?php _e('Image'); ?></label>
			<p>
				<div class="hhwt-preview-upload">
					<img src="" width="200" height="100" style="display: none;">
				</div>
				<input type="hidden" class="regular-text" name="catmeta[cat-image]" class="widefat"/>
				<input type="button" class="button button-secondary hhwt-btn-add-media" value="<?php _e( 'Add Image', 'hero-theme' ); ?>" />
				<input type="button" class="button button-secondary hhwt-btn-remove-media" value="<?php _e( 'Remove Image', 'hero-theme' ); ?>" />
			</p>
		</div>

		<div class="form-field term-group">
			<label for="catmeta[cat-image-thumb]"><?php _e('Image Thumbnail'); ?></label>
			<p>
				<div class="hhwt-preview-upload">
					<img src="" width="200" height="100" style="display: none;">
				</div>
				<input type="hidden" class="regular-text" name="catmeta[cat-image-thumb]" class="widefat"/>
				<input type="button" class="button button-secondary hhwt-btn-add-media" value="<?php _e( 'Add Image', 'hero-theme'); ?>" />
				<input type="button" class="button button-secondary hhwt-btn-remove-media" value="<?php _e( 'Remove Image', 'hero-theme' ); ?>" />
			</p>
		</div>
		<?php
	}
 
	/*
	* Save the form field
	* @since 1.0.0
	*/
	public function save_category_image ( $term_id, $tt_id ) {
		if ( isset( $_POST['catmeta'] ) ) 
			update_term_meta( $term_id, META_CAT_IMAGE, $_POST['catmeta']);

		if ( isset( $_POST['is-dest-category'] ) ) 
			update_term_meta( $term_id, META_DEST_CAT, $_POST['is-dest-category']);
				else
			delete_term_meta( $term_id, META_DEST_CAT );

		if ( isset( $_POST['dest-category-order'] ) ) 
			update_term_meta( $term_id, META_DEST_ORDER, (int) $_POST['dest-category-order']);

		if ( isset( $_POST['is-newsletter-popup'] ) ) 
			update_term_meta( $term_id, META_SHOW_NEWLETTER, (int) $_POST['is-newsletter-popup']);
				else
			delete_term_meta( $term_id, META_SHOW_NEWLETTER );

		if ( isset( $_POST['newsletter-type'] ) ) 
			update_term_meta( $term_id, META_NEWLETTER_TYPE, (int) $_POST['newsletter-type']);

		if ( isset( $_POST['newsletter-title'] ) ) 
			update_term_meta( $term_id, META_NEWLETTER_TITLE, $_POST['newsletter-title']);

		if ( isset( $_POST['newsletter-description'] ) ) 
			update_term_meta( $term_id, META_NEWLETTER_DESC, $_POST['newsletter-description']);

		if ( isset( $_POST['newsletter-signup-text'] ) ) 
			update_term_meta( $term_id, META_NEWLETTER_SIGNUP, $_POST['newsletter-signup-text']);

	}
 
	/*
	* Edit the form field
	* @since 1.0.0
	*/
	public function update_category_image ( $term, $taxonomy ) { 
		$image_arr = get_term_meta( $term->term_id, META_CAT_IMAGE, true );
		$is_dest_cat = get_term_meta( $term->term_id, META_DEST_CAT, true );
		$dest_cat_ord = get_term_meta( $term->term_id, META_DEST_ORDER, true );
		$is_show_newsletter = get_term_meta( $term->term_id, META_SHOW_NEWLETTER, true );
		$newsletter_type = get_term_meta( $term->term_id, META_NEWLETTER_TYPE, true );
		$newsletter_title = get_term_meta( $term->term_id, META_NEWLETTER_TITLE, true );
		$newsletter_desc = get_term_meta( $term->term_id, META_NEWLETTER_DESC, true );
		$newsletter_signup_text = get_term_meta( $term->term_id, META_NEWLETTER_SIGNUP, true );
		
		
		?>
		<tr class="form-field term-group-wrap">
			<th scope="row"><label for="is-dest-category"><?php _e('Destination Category'); ?></label></th>
			<td>
				<p><input type="checkbox" name="is-dest-category" value="1" <?php checked($is_dest_cat, 1); ?>/><?php _e('Is Destination Category'); ?></p>
			</td>
		</tr>

		<tr class="form-field term-group-wrap">
			<th scope="row"><label for="dest-category-order"><?php _e('Destination Order'); ?></label></th>
			<td><p><input type="number" class="regular-text" name="dest-category-order" value="<?php echo $dest_cat_ord; ?>" min="0" max="20"/></p></td>
		</tr>

		<tr class="form-field term-group-wrap">
			<th scope="row">
				<label for="category-image-id"><?php _e('Image'); ?></label>
			</th>

			<td>
				<div class="hhwt-preview-upload">
					<img src="<?php echo $image_arr['cat-image']; ?>" width="200" height="100" style="display: <?php echo ($image_arr['cat-image']) ? 'block;': 'none;'; ?>">
				</div>
				<input type="hidden" class="regular-text" name="catmeta[cat-image]" class="widefat" value="<?php echo $image_arr['cat-image'];?>"/>
				<input type="button" class="button button-secondary hhwt-btn-add-media" value="<?php _e( 'Add Image', 'hero-theme' ); ?>" />
				<input type="button" class="button button-secondary hhwt-btn-remove-media" value="<?php _e( 'Remove Image', 'hero-theme' ); ?>" />
			</td>
		</tr>

		<tr class="form-field term-group-wrap">
			<th scope="row">
				<label for="category-image-id"><?php _e('Image Thumbnail'); ?></label>
			</th>

			<td>
				<div class="hhwt-preview-upload">
					<img src="<?php echo $image_arr['cat-image-thumb']; ?>" width="200" height="100" style="display: <?php echo ($image_arr['cat-image-thumb']) ? 'block;': 'none;'; ?>;">
				</div>
				<input type="hidden" class="regular-text" name="catmeta[cat-image-thumb]" class="widefat" value="<?php echo $image_arr['cat-image-thumb'];?>"/>
				<input type="button" class="button button-secondary hhwt-btn-add-media" value="<?php _e( 'Add Image', 'hero-theme' ); ?>" />
				<input type="button" class="button button-secondary hhwt-btn-remove-media" value="<?php _e( 'Remove Image', 'hero-theme' ); ?>" />
			</td>
		</tr>

		<tr class="form-field term-group-wrap">
			<th scope="row"><label for="is-newsletter-popup"><?php _e('Show Newsletter Popup'); ?></label></th>
			<td>
				<p><input type="checkbox" name="is-newsletter-popup" id="is-newsletter-popup" value="1" <?php checked($is_show_newsletter, 1); ?>/><?php _e('Show Newsletter Popup'); ?></p>
			</td>
		</tr>

		<tr class="form-field term-group-wrap newsletter-detail" id="newslettertype">
			<th scope="row"><label for="newsletter-type"><?php _e('Select Newsletter Type'); ?></label></th>
			<td>				
				<select name="newsletter-type" id="newsletter-type" class="postform">
					<option value="">None</option>
					<option value="1" <?php selected( $newsletter_type, 1 );?>>Center Popup</option>
					<option value="2" <?php selected( $newsletter_type, 2 );?>>Right - Light</option>	
					<option value="3" <?php selected( $newsletter_type, 3 );?>>Right - Dark</option>					
				</select>
			</td>
		</tr>
		<tr class="form-field term-group-wrap newsletter-detail" id="newsletterimage">
			<th scope="row">
				<label for="category-image-id"><?php _e('Newsletter Image Thumbnail'); ?></label>
			</th>

			<td>
				<div class="hhwt-preview-upload">
					<img src="<?php echo $image_arr['newsletetr-image-thumb']; ?>" width="200" height="100" style="display: <?php echo ($image_arr['newsletetr-image-thumb']) ? 'block;': 'none;'; ?>;">
				</div>
				<input type="hidden" class="regular-text" name="catmeta[newsletetr-image-thumb]" class="widefat" value="<?php echo $image_arr['newsletetr-image-thumb'];?>"/>
				<input type="button" class="button button-secondary hhwt-btn-add-media" value="<?php _e( 'Add Image', 'hero-theme' ); ?>" />
				<input type="button" class="button button-secondary hhwt-btn-remove-media" value="<?php _e( 'Remove Image', 'hero-theme' ); ?>" />
			</td>
		</tr>
		<tr class="form-field term-group-wrap newsletter-detail" id="newslettertitle">
			<th class="row">
				<label for="newsletter-title"><?php _e('Newsletter Title'); ?></label>
			</th>
			<td>
				<input type="text" class="regular-text" id="newsletter-title" name="newsletter-title" class="widefat" value="<?php echo $newsletter_title;?>"/>
			</td>
		</tr>
		<tr class="form-field term-group-wrap newsletter-detail" id="newsletterdesc">
			<th class="row">
				<label for="newsletter-description"><?php _e('Newsletter Description'); ?></label>
			</th>
			<td>
				<input type="text" class="regular-text" id="newsletter-description" name="newsletter-description" class="widefat" value="<?php echo $newsletter_desc;?>"/>
			</td>
		</tr>
		<tr class="form-field term-group-wrap newsletter-detail" id="newslettersignup">
			<th class="row">
				<label for="newsletter-signup-text"><?php _e('Newsletter SignUp Text'); ?></label>
			</th>
			<td>
				<input type="text" class="regular-text" id="newsletter-signup-text" name="newsletter-signup-text" class="widefat" value="<?php echo $newsletter_signup_text;?>"/>
			</td>
		</tr>
		<?php
	}

} //End of class

$TermMeta = new HHWTTermMeta();
$TermMeta->init();

}