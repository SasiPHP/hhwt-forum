	<footer id="footer">
			<div class="container-fluid hhwt-footer-fluid">
		         <div class="container hhwt-footer-container">

		            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 hhwt-footer-categories">
		               <?php
		                  if ( is_active_sidebar( 'footer_column_1' ) ) {
		                     dynamic_sidebar( 'footer_column_1' );
		                  }

		                  if ( is_active_sidebar( 'footer_column_2' ) ) {
		                     dynamic_sidebar( 'footer_column_2' );
		                  }

		                  if ( is_active_sidebar( 'footer_column_3' ) ) {
		                     dynamic_sidebar( 'footer_column_3' );
		                  }
		               ?>
		            </div>

		            <?php
		               if ( is_active_sidebar( 'footer_column_4' ) ) {
		                  dynamic_sidebar( 'footer_column_4' );
		               }
		            ?>
		          
		            <div class="col-lg-offset-1 col-lg-3 col-md-4 col-sm-4 hhwt-footer-download">
		               <div class="col-lg-12 col-md-12 col-sm-12 hhwt-footer-download-title">
		                  <h4>Download Our Mobile App</h4>
		               </div>
		               <div class="col-lg-6 col-md-6 col-sm-6 hhwt-footer-download-btn">
		                  <a href="https://itunes.apple.com/sg/app/hhwt-travel-planner-for-muslim-travellers-recommended/id1088691334?mt=8">   <img target="_blank" src="<?php echo TEMPLATEURL;?>/images/ios_footer_btn.png" title="IOS APP">
		                  </a>
		               </div>
		               <div class="col-lg-6 col-md-6 col-sm-6 hhwt-footer-download-btn">
		                  <a href="https://play.google.com/store/apps/details?id=com.hhwt.travelplanner&hl=en">
		                     <img target="_blank" src="<?php echo TEMPLATEURL;?>/images/android_footer_btn.png" alt="Android APP">
		                  </a>
		               </div>
		            </div>

		            <div class="hhwt-footer-logo col-xs-12">
		               <img src="<?php echo TEMPLATEURL;?>/images/footer_logo.png">
		            </div>
		            <div class="hhwt-footer-copyright col-xs-12">
		               <p>&copy; <?php echo date('Y');?> Hello Travel Pte Ltd.All Rights Reserved.</p>
		            </div>
		            <div class="hhwt-footer-agree col-xs-12">
		               <a href="http://www.havehalalwilltravel.com/end-user-license-agreement/" target="_blank">End User License Agreement</a>
		               <span>|</span>
		               <a href="http://www.havehalalwilltravel.com/privacy-policy/" target="_blank">Privacy Policy</a>
		            </div>
		         </div>
		      </div>
		      <div id="go-top-button" class="fa fa-angle-up" title="Scroll To Top"></div>
		</footer><!-- End Footer -->

		<!-- MODAL UPLOAD IMAGES -->
	    <?php
		    if(is_front_page() || is_singular( 'thread' ) || is_tax()){
		    	get_template_part( 'template/modal', 'images' );
			}
		?>
		<!-- END MODAL UPLOAD IMAGES -->

		<!-- REPLY TEMPLATE -->
	    <?php
		    if( is_singular( 'thread' ) ){
		    	get_template_part( 'template-js/reply', 'item' );
		    	get_template_part( 'template-js/child-reply', 'item' );
			}
		?>
		<!-- END REPLY TEMPLATE -->

		<!-- Modal Login -->
		<?php
			if(!is_user_logged_in()){
				get_template_part( 'template/modal', 'auth' );
			}
			else{
				get_template_part( 'template/modal', 'report' );
			}
		?>
		<!-- End Modal Login -->

		<!-- Modal Contact Form -->
		<?php
			if(is_author() || is_page_template('page-member.php' )){
				get_template_part( 'template/modal', 'contact' );
			}
		?>
		<!-- End Modal Contact Form -->
		<!-- REPLY TEMPLATE -->
		<script type="text/template" id="search_preview_template">

			<# _.each(threads, function(thread){ #>

			<div class="i-preview">
				<a href="{{= thread.permalink }}">
					<div class="i-preview-avatar">
						{{= (typeof(thread.et_avatar) === "object") ? thread.et_avatar.thumbnail : thread.et_avatar }}
					</div>
					<div class="i-preview-content">
						<span class="i-preview-title">{{= thread.post_title.replace( search_term, '<strong>' + search_term + "</strong>" ) }}</span>
						<span class="comment active">
							<span class="icon" data-icon="w"></span>{{= thread.et_replies_count }}
						</span>
						<span class="like active">
							<span class="icon" data-icon="k"></span>{{= thread.et_likes_count }}
						</span>
					</div>
				</a>
			</div>

			<# }); #>

			<div class="i-preview i-preview-showall">

				<# if ( total > 0 && pages > 1 ) { #>

				<a href="{{= search_link }}"><?php printf( __('View all %s results', ET_DOMAIN), '{{= total }}' ); ?></a>

				<# } else if ( pages == 1) { #>

				<a href="{{= search_link }}"><?php _e('View all results', ET_DOMAIN) ?></a>

				<# } else { #>

				<a> <?php _e('No results found', ET_DOMAIN) ?> </a>

				<# } #>

			</div>
		</script>
		<!-- REPLY TEMPLATE -->
		<!-- Default Wordpress Editor -->
		<div class="hide">
			<?php wp_editor( '' , 'temp_content', editor_settings() ); ?>
		</div>
		<!-- Default Wordpress Editor -->

		</div>
		<div class="mobile-menu">
			<ul class="mo-cat-list">
				<?php et_the_mobile_cat_list(); ?>
			</ul>
		</div>
	</div>
	<!-- <div class="chatBottCnt">
		<div class="chatBottIframe">
			<iframe src='https://webchat.botframework.com/embed/hhwt?s=XYR6JtMrT10.cwA.K48.rIqSJbMaBsU47dObjNEDYT7w99_Jb5RQ4eZdoYxlAc4' id="chatBot" width="100%" height="300px"></iframe>
		</div>

		<a href="javascript:;" class="viewChatBox"><span class="chatIcon"><img src="<?php echo TEMPLATEURL;?>/images/chat_icon.png" alt="" title=""></span> HHWT Chat Bot</a>
	</div> -->

	<?php wp_footer(); ?>
	<!-- CHANGE DEFAULT SETTINGs UNDERSCORE  -->
	<!-- END CHANGE DEFAULT SETTINGs UNDERSCORE  -->
	<?php
		global $fe_confirm;
		if($fe_confirm == 1)
			echo '<script type="text/javascript">
	        jQuery(document).ready(function() {
	            pubsub.trigger("fe:showNotice", "'.__("Your account has been confirmed successfully!",ET_DOMAIN).'" , "success");
	        });
	    </script>';
	    //Show notification if user can't view this thread
	     if(isset($_REQUEST['error']) && $_REQUEST['error'] == 404){
	    	echo '<script type="text/javascript">
	        jQuery(document).ready(function() {
	             pubsub.trigger("fe:showNotice", "'.__("Please log into your  account to view this thread",ET_DOMAIN).'" , "warning");
	        });
	    </script>';
	    }
	?>
	<?php
		if(et_get_option('gplus_login', false)){
	?>
	<style type="text/css">
		iframe[src^="https://apis.google.com"] {
		  display: none;
		}
	</style>
	<?php } ?>
	<!-- Fix Padding Right In Thread Title -->
	<?php if(!is_user_logged_in() || !current_user_can( 'administrator' )){ ?>
	<style type="text/css">
		.f-floatright .title a {
			padding-right: 0;
		}
	</style>
	<?php } ?>
	<!-- Fix Padding Right In Thread Title -->

	<?php 
		$current_user = wp_get_current_user();
	?>
	<script type="text/javascript">
		/*window.dataLayer.push({
		 'event': 'Forum-PageView',
		 'eventCategory': 'Forum Total Views',
		 'eventAction': 'Forum | Total Views',
		 'eventLabel': 'Forum Total Views',
		});*/


		jQuery(document).ready(function(){

			jQuery('iframe#chatBot').load(function(){
			    console.log(jQuery('iframe#chatBot').contents().find("#BotChatElement").html());
			});

			//alert(jQuery("iframe#chatBot").contents().find('#BotChatElement').html());
			//jQuery("iframe#chatBot").contents().find(".wc-shellinput").css( "background-color", "#000");

			jQuery(".hhwtmainsubmenu .dropdown-toggle").click(function(){
				jQuery(this).next(".submenucontainer").slideToggle('slow');
			});

			jQuery(".viewChatBox").click(function(){
				jQuery(".chatBottIframe").slideToggle('slow');
			});


/*			jQuery(".new-thread #btn-create").click(function(){
				var logged_user_name = "<?php echo $current_user->user_login; ?>";
				var logged_user_email = "<?php echo $current_user->user_email; ?>";
				var category = jQuery('.text-select').text();
				var topic_title = jQuery('#thread_title').val();
				var content = tinyMCE.activeEditor.getContent();
				if(category != "" && topic_title != "" && content != ""){
					woopra.track("forum_create_topic", {
			          name: logged_user_name,
			          email: logged_user_email,
			          category: category,
			          topic_title: topic_title,
			      	});

				    // Identify customer
			        woopra.identify({
			            email: logged_user_email,
			            name: logged_user_name,
			        });                          
			        // track
			        woopra.track();
		   		}
			});

			jQuery(".tog-follow.follow").click(function(){
				var logged_user_name = "<?php echo $current_user->user_login; ?>";
				var logged_user_email = "<?php echo $current_user->user_email; ?>";
				var topic_title = "<?php the_title(); ?>";
				var permalink = "<?php echo get_the_permalink(); ?>";

				woopra.track("forum_following_topic", {
		          name: logged_user_name,
		          email: logged_user_email,
		          permalink: permalink,
		          topic_title: topic_title,
		      	});

			    // Identify customer
		        woopra.identify({
		            email: logged_user_email,
		            name: logged_user_name,
		        });                          
		        // track
		        woopra.track();
			});


			jQuery(".ajax-reply .button-event .btn").click(function(){
				var logged_user_name = "<?php echo $current_user->user_login; ?>";
				var logged_user_email = "<?php echo $current_user->user_email; ?>";
				var topic_title = "<?php the_title(); ?>";
				var permalink = "<?php echo get_the_permalink(); ?>";

				woopra.track("forum_comment_topic", {
		          name: logged_user_name,
		          email: logged_user_email,
		          permalink: permalink,
		          topic_title: topic_title,
		      	});

			    // Identify customer
		        woopra.identify({
		            email: logged_user_email,
		            name: logged_user_name,
		        });                          
		        // track
		        woopra.track();
			});*/

		});
	</script>

	<link href="<?php echo TEMPLATEURL ?>/css/custom-2.css" type='text/css' rel="stylesheet" >

	<script src="<?php echo TEMPLATEURL . '/js/jquery.cookie.js' ?>" type='text/javascript'></script>
	<script src="<?php echo TEMPLATEURL . '/js/plugins.js' ?>" type='text/javascript'></script>
	<script src="<?php echo TEMPLATEURL . '/js/hhwt.js' ?>" type='text/javascript'></script>

  	</body>
</html>